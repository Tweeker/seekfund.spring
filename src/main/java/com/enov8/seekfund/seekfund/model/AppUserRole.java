package com.enov8.seekfund.seekfund.model;


import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "app_user_role")
public class AppUserRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}




	public AppUserRoleConstant getName() {
		return name;
	}

	public void setName(AppUserRoleConstant name) {
		this.name = name;
	}



	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true)
	private Long id;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private AppUserRoleConstant name;


}
