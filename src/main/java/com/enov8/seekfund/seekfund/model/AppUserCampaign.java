package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "app_user_campaign")
public class AppUserCampaign  implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @NotNull
    @OneToOne
    private AppUser appUser;

    private String name;


    private Timestamp startDate;

    private Timestamp endDate;

    private Timestamp dateApproved;

    public Timestamp getDateApproved() {
        return dateApproved;
    }

    public void setDateApproved(Timestamp dateApproved) {
        this.dateApproved = dateApproved;
    }

    @Enumerated(EnumType.STRING)
    @NotNull
    private CampaignStatus status = CampaignStatus.PENDING;

    @NotNull
    @Column(columnDefinition = "numeric(19, 2) default 0")
    private BigDecimal amountRaised = new BigDecimal(0);

    @NotNull
    @Column(columnDefinition = "numeric(19, 0) default 0")
    private Long searchCount = 0L;


    @NotNull
    @Column(columnDefinition="TEXT")
    private String description;


    private String campaignImage;


    @NotNull
    @OneToOne
    private CampaignCategory campaignCategory;

    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());

    @Column(columnDefinition = "numeric(19, 2) default 0")
    private BigDecimal amount = new BigDecimal(0);


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @BatchSize(size = 4)
    private Set<CampaignImage> images = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CampaignCategory getCampaignCategory() {
        return campaignCategory;
    }

    public void setCampaignCategory(CampaignCategory campaignCategory) {
        this.campaignCategory = campaignCategory;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountRaised() {
        return amountRaised;
    }

    public void setAmountRaised(BigDecimal amountRaised) {
        this.amountRaised = amountRaised;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Long searchCount) {
        this.searchCount = searchCount;
    }

    public Set<CampaignImage> getImages() {
        return images;
    }

    public void setImages(Set<CampaignImage> images) {
        this.images = images;
    }

    public String getCampaignImage() {
        return campaignImage;
    }

    public void setCampaignImage(String campaignImage) {
        this.campaignImage = campaignImage;
    }
}
