package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "campaign_comment")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignComment implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("deprecation")
    @Id

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @OneToOne
    private AppUser commentedBy;

    @NotNull
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());


    @Column(columnDefinition="TEXT")
    @NotNull
    private String comment;

    @Enumerated(EnumType.STRING)
    @NotNull
    private GenericStatusConstant status = GenericStatusConstant.ACTIVE;

    @OneToOne
    private AppUserCampaign campaign;

    private Timestamp dateModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUser getCommentedBy() {
        return commentedBy;
    }

    public void setCommentedBy(AppUser commentedBy) {
        this.commentedBy = commentedBy;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public AppUserCampaign getCampaign() {
        return campaign;
    }

    public void setCampaign(AppUserCampaign campaign) {
        this.campaign = campaign;
    }

    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }
}
