package com.enov8.seekfund.seekfund.model.attribute;

import java.io.Serializable;

public class SocialRegister implements Serializable {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
