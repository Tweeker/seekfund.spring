package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "campaign_image")
public class CampaignImage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @ManyToOne
    private AppUserCampaign appUserCampaign;


    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @NotNull
    @Column( columnDefinition = "varchar(1000)")
    private String baseUrl ;
    @NotNull
    @Column(columnDefinition = "varchar(1000)")
    private String fullUrl;
    @Enumerated(EnumType.STRING)
    @NotNull
    private GenericStatusConstant  status = GenericStatusConstant.ACTIVE;

    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public AppUserCampaign getAppUserCampaign() {
        return appUserCampaign;
    }

    public void setAppUserCampaign(AppUserCampaign appUserCampaign) {
        this.appUserCampaign = appUserCampaign;
    }
}
