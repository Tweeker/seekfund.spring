package com.enov8.seekfund.seekfund.model;
import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.AppUserType;
import com.enov8.seekfund.seekfund.enumumeration.SocialAuthType;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "app_user")
public class AppUser implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("deprecation")
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true)
	private Long id;

	private Timestamp dateEmailConfirmed;

	private boolean emailConfirmed;

	@NotNull
	private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());

	private Timestamp dateModified;

	private Long modifiedBy;

	@NotNull
	@Column(length = 15, unique = true)
	private String username;
	@NotNull
	private String hashedPassword;
	@NotNull
	@Column(unique = true)
	private String email;
	@Column(columnDefinition="TEXT")
	private String hashEntry;

	private String phoneNumber;

	private String altPhoneNumber;

	private String lastLoginIP;
	private Timestamp lastLoginDate;
	private Integer noOfTimesPaid = 0;

	@Enumerated(EnumType.STRING)
	@NotNull
	private AppUserStatus status = AppUserStatus.AWAITING_EMAIL_CONF;


	@Enumerated(EnumType.STRING)
	@NotNull
	private AppUserType type = AppUserType.USER;

	private Timestamp logoutDate;


	
	private Timestamp passwordResetExpireDate;
	@Column(length = 500)
	private String  resetPassswordToken;
	@Column(length = 500)
	private String resetPassswordTokenAlt;

	private Integer loginCounter = 0;

	@Column(nullable = false)
	private Integer loginFailCounter = 0;


	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@BatchSize(size = 5)
	private Set<AppUserRole> roles;


	private transient Object socialPrincipal;
	private transient SocialAuthType socialAuthType;

	public SocialAuthType getSocialAuthType() {
		return socialAuthType;
	}

	public void setSocialAuthType(SocialAuthType socialAuthType) {
		this.socialAuthType = socialAuthType;
	}

	public Object getSocialPrincipal() {
		return socialPrincipal;
	}

	public void setSocialPrincipal(Object socialPrincipal) {
		this.socialPrincipal = socialPrincipal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateEmailConfirmed() {
		return dateEmailConfirmed;
	}

	public void setDateEmailConfirmed(Timestamp dateEmailConfirmed) {
		this.dateEmailConfirmed = dateEmailConfirmed;
	}

	public boolean isEmailConfirmed() {
		return emailConfirmed;
	}

	public void setEmailConfirmed(boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAltPhoneNumber() {
		return altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	public String getLastLoginIP() {
		return lastLoginIP;
	}

	public void setLastLoginIP(String lastLoginIP) {
		this.lastLoginIP = lastLoginIP;
	}

	public Timestamp getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Timestamp lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Integer getNoOfTimesPaid() {
		return noOfTimesPaid;
	}

	public void setNoOfTimesPaid(Integer noOfTimesPaid) {
		this.noOfTimesPaid = noOfTimesPaid;
	}

	public AppUserStatus getStatus() {
		return status;
	}

	public void setStatus(AppUserStatus status) {
		this.status = status;
	}


	public AppUserType getType() {
		return type;
	}

	public void setType(AppUserType type) {
		this.type = type;
	}

	public Timestamp getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Timestamp logoutDate) {
		this.logoutDate = logoutDate;
	}


	public Integer getLoginCounter() {
		return loginCounter;
	}

	public void setLoginCounter(Integer loginCounter) {
		this.loginCounter = loginCounter;
	}

	public Integer getLoginFailCounter() {
		return loginFailCounter;
	}

	public void setLoginFailCounter(Integer loginFailCounter) {
		this.loginFailCounter = loginFailCounter;
	}


	public Set<AppUserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<AppUserRole> roles) {
		this.roles = roles;
	}

	public Timestamp getPasswordResetExpireDate() {
		return passwordResetExpireDate;
	}

	public void setPasswordResetExpireDate(Timestamp passwordResetExpireDate) {
		this.passwordResetExpireDate = passwordResetExpireDate;
	}

	public String getResetPassswordToken() {
		return resetPassswordToken;
	}

	public void setResetPassswordToken(String resetPassswordToken) {
		this.resetPassswordToken = resetPassswordToken;
	}

	public String getResetPassswordTokenAlt() {
		return resetPassswordTokenAlt;
	}

	public void setResetPassswordTokenAlt(String resetPassswordTokenAlt) {
		this.resetPassswordTokenAlt = resetPassswordTokenAlt;
	}

	public String getHashEntry() {
		return hashEntry;
	}

	public void setHashEntry(String hashEntry) {
		this.hashEntry = hashEntry;
	}
}
