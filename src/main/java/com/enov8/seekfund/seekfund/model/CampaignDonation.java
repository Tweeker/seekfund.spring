package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "campaign_donation")
public class CampaignDonation {
    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
    @Column(columnDefinition = "numeric(19, 0) default 0")
    private BigDecimal amount;

    private BigDecimal amountRaised = new BigDecimal("0");

    @Enumerated(EnumType.STRING)
    @NotNull
    private PaymentStatus paymentStatus = PaymentStatus.PENDING;

    public BigDecimal getAmountRaised() {
        return amountRaised;
    }

    public void setAmountRaised(BigDecimal amountRaised) {
        this.amountRaised = amountRaised;
    }

    @OneToOne
    private AppUser donatedBy;

    private String donatedByName;
    private String donatedByEmail;
    private String donatedByPhone;

    public String getDonatedByName() {
        return donatedByName;
    }

    public void setDonatedByName(String donatedByName) {
        this.donatedByName = donatedByName;
    }

    public String getDonatedByEmail() {
        return donatedByEmail;
    }

    public void setDonatedByEmail(String donatedByEmail) {
        this.donatedByEmail = donatedByEmail;
    }

    public String getDonatedByPhone() {
        return donatedByPhone;
    }

    public void setDonatedByPhone(String donatedByPhone) {
        this.donatedByPhone = donatedByPhone;
    }

    @Column(columnDefinition="TEXT")
    private String comment;

    @NotNull
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());

    @NotNull
    @OneToOne
    private AppUserCampaign campaign;


    private String paymentRef;


    private Timestamp dateCompleted;

    private String sessionId;

    private String returnMsg;

    @Column(columnDefinition="TEXT")
    private String returnText;

    public String getReturnText() {
        return returnText;
    }

    public void setReturnText(String returnText) {
        this.returnText = returnText;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public AppUserCampaign getCampaign() {
        return campaign;
    }

    public void setCampaign(AppUserCampaign campaign) {
        this.campaign = campaign;
    }

    public String getPaymentRef() {
        return paymentRef;
    }

    public void setPaymentRef(String paymentRef) {
        this.paymentRef = paymentRef;
    }

    public Timestamp getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Timestamp dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public AppUser getDonatedBy() {
        return donatedBy;
    }

    public void setDonatedBy(AppUser donatedBy) {
        this.donatedBy = donatedBy;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
