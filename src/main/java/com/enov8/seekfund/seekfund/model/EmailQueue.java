package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.EmailQueueType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "email_queue")
public class EmailQueue implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @Column(columnDefinition="TEXT")
    private String emailBody;
    @Column(columnDefinition="TEXT")
    private String data;
    @NotNull
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());
    private Timestamp dateToSend = new Timestamp(System.currentTimeMillis()) ;

    private Boolean sent = false;
    @Enumerated(EnumType.STRING)
    private EmailQueueType type;
    @NotNull
    private String title;

    private String description;
    @NotNull
    private String toEmail ;
    @NotNull
    private String fromEmail;

    private String copy;

    private Timestamp dateSent ;

    private String emailTemplate;

    public EmailQueueType getType() {
        return type;
    }

    public void setType(EmailQueueType type) {
        this.type = type;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateToSend() {
        return dateToSend;
    }

    public void setDateToSend(Timestamp c) {
        this.dateToSend = dateToSend;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDateSent() {
        return dateSent;
    }

    public void setDateSent(Timestamp dateSent) {
        this.dateSent = dateSent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }
}
