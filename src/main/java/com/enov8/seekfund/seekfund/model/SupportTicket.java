package com.enov8.seekfund.seekfund.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
@Table(name="support_ticket")
public class SupportTicket implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id	
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true)
	private Long id;

	@NotNull
	@Column(columnDefinition="TEXT")
	private String text;
	private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());
	@OneToOne
	private AppUser createdBy;
	@OneToOne
	private AppUser replyBy;
	@Lob
	@Column(columnDefinition="TEXT")
	private String replyText;
	
	private Timestamp dateReplied;
	
	private Boolean closed = Boolean.FALSE;
	
	private String topic;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public AppUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AppUser createdBy) {
		this.createdBy = createdBy;
	}

	public AppUser getReplyBy() {
		return replyBy;
	}

	public void setReplyBy(AppUser replyBy) {
		this.replyBy = replyBy;
	}

	public String getReplyText() {
		return replyText;
	}

	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}

	public Timestamp getDateReplied() {
		return dateReplied;
	}

	public void setDateReplied(Timestamp dateReplied) {
		this.dateReplied = dateReplied;
	}

	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
