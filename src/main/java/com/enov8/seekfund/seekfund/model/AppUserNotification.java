package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.AppUserNotificationType;
import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.enumumeration.NotificationType;
import org.hibernate.annotations.IndexColumn;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "app_user_notification")
public class AppUserNotification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @IndexColumn(name="id_index")
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @OneToOne
    private AppUser appUser;

    @Enumerated(EnumType.STRING)
    @NotNull
    private GenericStatusConstant status = GenericStatusConstant.ACTIVE;


    @Enumerated(EnumType.STRING)
    @NotNull
    private AppUserNotificationType notificationType;


    public AppUserNotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(AppUserNotificationType notificationType) {
        this.notificationType = notificationType;
    }


    @NotNull
    @Column(columnDefinition="TEXT")
    private String description;


    private Timestamp dateCreated;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }
}
