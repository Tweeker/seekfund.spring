package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.enumumeration.SocialAuthType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "social_auth_user")
public class SocialAuthUser implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
    @NotNull
    private String auth_id;

    @Enumerated(EnumType.STRING)
    @NotNull
    private SocialAuthType authType;
    @NotNull
    private String fullName;
    private String email;
    @NotNull
    private String token;

    @Column(columnDefinition="TEXT")
    private String credentials;
    @Column(columnDefinition="TEXT")
    private String profileImage;

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Enumerated(EnumType.STRING)
    @NotNull
    private GenericStatusConstant status = GenericStatusConstant.PENDING;
    @OneToOne
    private AppUser appUser;

    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuth_id() {
        return auth_id;
    }

    public void setAuth_id(String auth_id) {
        this.auth_id = auth_id;
    }

    public SocialAuthType getAuthType() {
        return authType;
    }

    public void setAuthType(SocialAuthType authType) {
        this.authType = authType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
}
