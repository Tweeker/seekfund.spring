package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import org.hibernate.annotations.IndexColumn;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "audit_trail")
public class AuditTrail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("deprecation")
	@Id	
    @IndexColumn(name="id_index")
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true)
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	public String getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(String actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public Timestamp getDatePerformed() {
		return datePerformed;
	}

	public void setDatePerformed(Timestamp datePerformed) {
		this.datePerformed = datePerformed;
	}

	public String getEntityAffected() {
		return entityAffected;
	}

	public void setEntityAffected(String entityAffected) {
		this.entityAffected = entityAffected;
	}
	@IndexColumn(name="id_index")
	@Enumerated(EnumType.STRING)
	private ActivityType activityType;
	
	@NotNull
	private String actionPerformed;
	
	private Timestamp datePerformed = new Timestamp(System.currentTimeMillis());
	
	private String entityAffected;
	
	private Long recordId;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	private Long userId;

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
