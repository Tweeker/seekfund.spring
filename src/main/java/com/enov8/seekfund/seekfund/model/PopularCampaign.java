package com.enov8.seekfund.seekfund.model;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
@Entity
@Table(name = "popular_campaign")
public class PopularCampaign {
    @SuppressWarnings("deprecation")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
    @OneToOne
    @NotNull
    private AppUserCampaign appUserCampaign;
    @Enumerated(EnumType.STRING)
    @NotNull
    private GenericStatusConstant status = GenericStatusConstant.ACTIVE;

    @Column(columnDefinition="TEXT")
    private String campaignImage;

    @NotNull
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());

    @Column(columnDefinition = "numeric(19, 0) default 0")
    private Long pOrder = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUserCampaign getAppUserCampaign() {
        return appUserCampaign;
    }

    public void setAppUserCampaign(AppUserCampaign appUserCampaign) {
        this.appUserCampaign = appUserCampaign;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public String getCampaignImage() {
        return campaignImage;
    }

    public void setCampaignImage(String campaignImage) {
        this.campaignImage = campaignImage;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getpOrder() {
        return pOrder;
    }

    public void setpOrder(Long pOrder) {
        this.pOrder = pOrder;
    }
}
