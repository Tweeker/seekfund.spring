package com.enov8.seekfund.seekfund.filter;

import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class CsrfTokenFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(CsrfTokenFilter.class);
    private String CSRF_TOKEN;
    private Environment env;

    public CsrfTokenFilter(Environment env ) {
        this.env = env;
        this.CSRF_TOKEN = env.getProperty("api.csrf_token.name");

    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        //TODO check body of request for security token
        logger.info(" ====================HIT API FILTER  =======================");
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        Enumeration<String> headers = httpRequest.getHeaderNames();
        String securityHeader = null;

        while(headers.hasMoreElements()){

            String nextElement  = headers.nextElement();


            if(nextElement.equalsIgnoreCase(CSRF_TOKEN)){
                securityHeader = nextElement;
            }


        }

        if(securityHeader != null && httpRequest.getHeader(securityHeader).equals(CSRF_TOKEN)){
            chain.doFilter(request, response);
        }else{
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ObjectMapper mapper = new ObjectMapper();
            RestResponsePojo authRespPojo = new RestResponsePojo();
            authRespPojo.setSuccess(false);
            authRespPojo.setReason("UnAuthorised!.");
            httpResponse.setStatus(HttpStatus.FORBIDDEN.value());
            httpResponse.getWriter().write(mapper.writeValueAsString(authRespPojo));
        }





    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub


    }

}
