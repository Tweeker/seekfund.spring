package com.enov8.seekfund.seekfund.api;

import com.amazonaws.services.opsworks.model.App;
import com.amazonaws.services.s3.AmazonS3;
import com.enov8.seekfund.seekfund.enumumeration.*;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.*;
import com.enov8.seekfund.seekfund.pojo.CampaignImageRespPojo;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignImagePojo;
import com.enov8.seekfund.seekfund.pojo.request.AddCampaignCommentRq;
import com.enov8.seekfund.seekfund.pojo.request.AddCampaignRequestPojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.pojo.request.UpdateCampaignRequestPojo;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.File;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/u/api/campaign")
@Transactional
public class CampaignApi {
    @Autowired
    DatabaseService databaseService;
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CampaignService campaignService;
    @Autowired
    AmazonS3 amazonS3;
    @Autowired
    CommonMethod commonMethod;



    private static final Logger logger = LoggerFactory.getLogger(CampaignApi.class);

    private static final SimpleDateFormat sdfServerFormat = new SimpleDateFormat(Constant.SERVER_DATETIME_FORMAT);
    @PostMapping("/uploadfile")
    public RestResponsePojo handleFileUpload(@RequestParam("file") MultipartFile mFile,
                                   RedirectAttributes redirectAttributes, Authentication authentication) throws Exception {


        RestResponsePojo restResponsePojo = new RestResponsePojo();

        AppUser appUser = (AppUser) authentication.getPrincipal();


        logger.info("About to upload file for  " + appUser.getUsername());


        File file = commonMethod.convert(mFile);

        campaignService.addCampaignImage(appUser, file);

        logger.info("Done uploading file for  " + appUser.getUsername());


        return restResponsePojo;
    }


    @PostMapping("/comment")
    public RestResponsePojo addCampaignComment(
        Authentication authentication,
        HttpServletRequest httpServletRequest,
        @RequestBody AddCampaignCommentRq comment) throws
            IllegalArgumentException, SeekFundException, Exception {

        RestResponsePojo restResponsePojo  = new RestResponsePojo();
        AppUser appUser = (AppUser) authentication.getPrincipal();

        Assert.notNull(comment.getCampaignId(), "Cannot find campaign id");
        Assert.notNull(comment.getComment(), "Cannot find campaign comment");


        if(comment.getComment().length() > 500){
            throw new SeekFundException("comment too long");
        }

        AppUserCampaign appUserCampaign = (AppUserCampaign)databaseService.getRecordById(AppUserCampaign.class,
                comment.getCampaignId());


        Assert.notNull(appUserCampaign, "Cannot find campaign");
        Assert.isTrue(appUserCampaign.getStatus()
                .getValue().equalsIgnoreCase(CampaignStatus.RUNNING.getValue()),
                "Campaign is currently " + appUserCampaign.getStatus());

        CampaignComment campaignComment = new CampaignComment();
        campaignComment.setCampaign(appUserCampaign);
        campaignComment.setComment(comment.getComment());
        campaignComment.setCommentedBy(appUser);

        databaseService.saveRecord(campaignComment);

        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);
        databaseService.createAuditTrail(appUser
                        .getUsername().trim() + " " +
                        "" +
                        " Added   Campaign " +
                        " comment with id: " + campaignComment.getId()+"" +
                        " from ip " + remoteIp,
                CampaignComment.class.getSimpleName(),
                ActivityType.INSERT, appUser.getId(), appUser.getId());


        return restResponsePojo;

    }

   // u/api/campaign/image
   @DeleteMapping("/image/{imageId}")
   public RestResponsePojo deleteCampaignImage(Authentication authentication,
                                           @PathVariable("imageId") Long  imageId) throws  SeekFundException, Exception {


        AppUser appUser  = (AppUser) authentication.getPrincipal();

        CampaignImage campaignImage = campaignService.getMyCampaignImageById(appUser,imageId);
        campaignImage.setStatus(GenericStatusConstant.DELETED);

        databaseService.updateRecord(campaignImage);
        AppUserCampaign appUserCampaign = campaignImage.getAppUserCampaign();

        if(appUserCampaign.getCampaignImage() != null &&
                appUserCampaign.getCampaignImage().equalsIgnoreCase(campaignImage.getFullUrl())){

            List<CampaignImagePojo>  campaignImagePojos =  campaignService.getCampaignImages(appUserCampaign);
            String newCampaignImageUrl  = null;
            if(campaignImagePojos != null &&
                    !campaignImagePojos.isEmpty() ){

                newCampaignImageUrl = campaignImagePojos.get(0).getFullUrl();
            }
            appUserCampaign.setCampaignImage(newCampaignImageUrl);




        }




        /*campaignImage.setStatus(GenericStatusConstant.DELETED);

        databaseService.updateRecord(campaignImage);
*/
        databaseService.updateRecord(appUserCampaign);



        RestResponsePojo restResponsePojo = new RestResponsePojo();



        return  restResponsePojo;


   }

    @PutMapping("/mycampaign/{campaingId}")
    public RestResponsePojo updateMyCampaign(Authentication authentication,
                                            @RequestBody AddCampaignRequestPojo campaign,
                                            @PathVariable("campaingId") String  campaiginId) throws  SeekFundException, Exception {
        AppUser appUser = (AppUser) authentication.getPrincipal();

        RestResponsePojo restResponsePojo = new RestResponsePojo();
        String sCampaignId  = campaiginId.trim().toLowerCase()
                .replace("seekfunds","")
                .replace("seekfund","")
                .replace("-","");

        Long lCampaignId  = Long.valueOf(sCampaignId);

        CurrentCampaignPojo appUserCampaign = campaignService.getMyCampaignById(appUser, lCampaignId );


        AppUserCampaign aCampaign  =  (AppUserCampaign) databaseService.getRecordById(AppUserCampaign.class,appUserCampaign.getId());
        if(campaign.getCategoryId() != null){
            //TODO find category

            CampaignCategory campaignCategory = (CampaignCategory)databaseService
                    .getRecordById(CampaignCategory.class,campaign.getCategoryId());


            aCampaign.setCampaignCategory(campaignCategory);

        }


        Assert.notNull(aCampaign, "Cannot find campaign ");
        if(campaign.getDescription() != null){
            aCampaign.setDescription(campaign.getDescription());

        }
        if(campaign.getName() != null){
            aCampaign.setName(campaign.getName());

        }

        databaseService.updateRecord(aCampaign);


        logger.info("==========updated  campaign details ================");
        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "" +
                        "Deleted  Campaign  id : " + aCampaign.getId()+" ",
                AppUserCampaign.class.getSimpleName(),
                ActivityType.UPDATE, appUser.getId(), appUser.getId());


        return restResponsePojo;

    }
    @GetMapping("/mycampaign/{campaingId}")
    public RestResponsePojo getUserCampaign(Authentication authentication,
            @PathVariable("campaingId") String  campaiginId) throws  SeekFundException, Exception {
        AppUser appUser = (AppUser) authentication.getPrincipal();


        RestResponsePojo restResponsePojo = new RestResponsePojo();
        String sCampaignId  = campaiginId.trim().toLowerCase()
                .replace("seekfunds","")
                .replace("seekfund","")
                .replace("-","");

        Long lCampaignId  = Long.valueOf(sCampaignId);

        CurrentCampaignPojo appUserCampaign = campaignService.getMyCampaignById(appUser, lCampaignId );


        Assert.notNull(appUserCampaign, "No Current campaign ");


        restResponsePojo.setData(appUserCampaign);







        return restResponsePojo;


    }

    @DeleteMapping("/current/image/{imageId}")
    public RestResponsePojo getUserCampaign(Authentication authentication, @PathVariable("imageId") Long imageId) throws  SeekFundException, Exception {
        AppUser appUser  = (AppUser) authentication.getPrincipal();
        AppUserCampaign appUserCampaign = campaignService.getCurrentCampaign(appUser);
        Assert.notNull(appUserCampaign, "No Current campaign ");
        String hql  =  "select c from CampaignImage c " +
                " where c.appUserCampaign.appUser.id = " + appUser.getId() +
                " and  c.appUserCampaign.id =  " + appUserCampaign.getId()  + " " +
                " and  c.status = '" + GenericStatusConstant.ACTIVE + "' " +
                " and  c.id = " + imageId;


        CampaignImage campaignImage = (CampaignImage) databaseService.getUniqueRecordByHql(hql);

        Assert.notNull(campaignImage,"Cannot find campaign image");



        campaignImage.setStatus(GenericStatusConstant.DELETED);

        databaseService.updateRecord(campaignImage);

        logger.info("==========Campaign image delete ================");
        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "Deleted  Campaign image  id : " + campaignImage.getId()
                        +" and  url  " + campaignImage.getBaseUrl(),
                CampaignImage.class.getSimpleName(),
                ActivityType.DELETE, appUser.getId(), appUser.getId());

        logger.info("==========Campaign image delete complate ================");

        return new RestResponsePojo();




    }
    @GetMapping("/current/image")
    public RestResponsePojo getUserCampaign(Authentication authentication) throws  SeekFundException, Exception{
        AppUser appUser  = (AppUser) authentication.getPrincipal();
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        AppUserCampaign appUserCampaign = campaignService.getCurrentCampaign(appUser);
        Assert.notNull(appUserCampaign, "No Current campaign ");
        String hql  =  "select c from CampaignImage c " +
                " where c.appUserCampaign.appUser.id = " + appUser.getId() +
                " and  c.appUserCampaign.id =  " + appUserCampaign.getId()  + " " +
                " and  c.status = '" + GenericStatusConstant.ACTIVE + "'";



        List<CampaignImage> campaignImages = (List<CampaignImage>)databaseService.getAllRecordsByHql(hql);

     /*   uid: -1,
                name: 'xxx.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
*/
     if(campaignImages == null){

         if(campaignImages == null){ campaignImages = new ArrayList<>();}
     }
     int i  = 1 ;
     List<CampaignImageRespPojo> campaignImageRespPojoList  = new ArrayList<>();
     for (CampaignImage c : campaignImages){

         CampaignImageRespPojo cc = new CampaignImageRespPojo();
         cc.setUid(c.getId());
         cc.setName("Campaign Image " + i);
         cc.setUrl(c.getFullUrl());
         campaignImageRespPojoList.add(cc);
         i++;
         ;
         //CampaignIm
     }

     restResponsePojo.setData(campaignImageRespPojoList);

        return restResponsePojo;

    }
    @GetMapping
    public RestResponsePojo getUserCampaign(Authentication authentication,
                                            @RequestParam(name ="start", defaultValue = "1") Integer start,
                                            @RequestParam(name ="length", defaultValue = "10") Integer length

                                            ) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();


        AppUser appUser = (AppUser) authentication.getPrincipal();


        if (!appUser.getStatus().equals(AppUserStatus.ACTIVE)) {
            throw new SeekFundException("User currently not active");
        }


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);

        criteria
                .add(Restrictions.eq("appUser.id",appUser.getId()));

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")

                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("dateCreated"), "dateCreated")

                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("description"), "description")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));

        criteria.setFirstResult(start-1);
        criteria.setMaxResults(length);
        List<CurrentCampaignPojo> currentCampaignPojos = criteria.list();


        restResponsePojo.setData(currentCampaignPojos);


        return restResponsePojo;

    }
    @GetMapping("/category")
    public RestResponsePojo getCampaignCategory(
            Authentication authentication) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();




        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignCategory.class);
        criteria
                .add(Restrictions.eq("status",GenericStatusConstant.ACTIVE));



        List<CampaignCategory> campaignCategories = criteria.list();


        restResponsePojo.setData(campaignCategories);



        return restResponsePojo;

    }


    @GetMapping("/current")
    public RestResponsePojo getCurrentUserCampaign(
                                            Authentication authentication) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();


        AppUser appUser = (AppUser) authentication.getPrincipal();


        if (!appUser.getStatus().equals(AppUserStatus.ACTIVE)) {
            throw new SeekFundException("User currently not active");
        }


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.PENDING,
                        CampaignStatus.RUNNING
                ))
                .add(Restrictions.eq("appUser.id",appUser.getId()));

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("description"), "description")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
        List<CurrentCampaignPojo> currentCampaignPojos = criteria.list();

        if(currentCampaignPojos == null) {

            return restResponsePojo;
        }
        if(currentCampaignPojos.isEmpty()) {

            return restResponsePojo;
        }

        restResponsePojo.setData(currentCampaignPojos.get(0));


        return restResponsePojo;

    }

    @PostMapping
    public RestResponsePojo addCampaign(@RequestBody AddCampaignRequestPojo addCampaignRequestPojo,
                                        Authentication authentication) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();


        AppUser appUser = (AppUser) authentication.getPrincipal();


        if(!appUser.getStatus().equals(AppUserStatus.ACTIVE)){ throw new SeekFundException("User currently not active");}

        //todo check if user has a campaign that is waiting approval or running

        //String hql  = "select c from AppUserCampaign c where c.status in ()"

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);

        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.PENDING,
                        CampaignStatus.RUNNING
                ))
                .add(Restrictions.eq("appUser.id",appUser.getId()));
        List<AppUserCampaign> appUserCampaigns = criteria.list();

        if(appUserCampaigns == null){ appUserCampaigns = new ArrayList<>();}


        if(!appUserCampaigns.isEmpty()){ throw new
                SeekFundException("Cannot " +
                "create campaign when a current campaign is pending or running!.");}


        Date startDate  = sdfServerFormat.parse(addCampaignRequestPojo.getStartDate());
        Date endDate  = sdfServerFormat.parse(addCampaignRequestPojo.getEndDate());


        Calendar endDateCalendar  = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        Calendar startDateCalendar  = Calendar.getInstance();
        startDateCalendar.setTime(startDate);


        Calendar todayCalender = Calendar.getInstance();
        Calendar yesterDayCalender  = Calendar.getInstance();
        yesterDayCalender.add(Calendar.DATE, -1);


        if( yesterDayCalender.after(startDateCalendar)){

            throw new SeekFundException("Campaign start date cannot be back dated beyond  " + sdfServerFormat.format(todayCalender.getTime()));
        }

        AppSetting maxDurationOfCampaign = databaseService
                .getAppSettingByName(Constant.DEFAULT_MAX_CAMPAIGN_DURATION,
                        Constant.DEFAULT_MAX_CAMPAIGN_DURATION_VAL.toString());

        Calendar maxDurationCalender = Calendar.getInstance();
        maxDurationCalender.setTime(new Date());
        maxDurationCalender.add(Calendar.DATE, Integer.parseInt(maxDurationOfCampaign.getValue()));

        //28102018  30112018
        if(maxDurationCalender.before(endDateCalendar)){

            throw new SeekFundException("Campaign end date cannot be beyond. " + sdfServerFormat.format(maxDurationCalender.getTime()));
        }

        AppUserCampaign newCampaign = new AppUserCampaign();

        //TODO check amount is within limit
        newCampaign.setAmount(addCampaignRequestPojo.getAmount());

        //Todo check for bad words in description
        newCampaign.setDescription(addCampaignRequestPojo.getDescription());

        //Todo check campaign name for bad words

        newCampaign.setName(addCampaignRequestPojo.getName());


        newCampaign.setAppUser(appUser);

        CampaignCategory campaignCategory = (CampaignCategory) databaseService.getRecordById(CampaignCategory.class,addCampaignRequestPojo.getCategoryId());

        if(campaignCategory == null){ throw new SeekFundException("Unidentified campaign category!");}
        newCampaign.setCampaignCategory(campaignCategory);

        newCampaign.setStatus(CampaignStatus.PENDING);




        newCampaign.setStartDate(new Timestamp(startDate.getTime()));
        newCampaign.setEndDate(new Timestamp(endDate.getTime()));

        databaseService.saveRecord(newCampaign);


        logger.info("==========Campaign creation complete ================");
        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "Created Campaign with id : " + newCampaign.getId(),
                AppUserCampaign.class.getSimpleName(),
                ActivityType.INSERT, appUser.getId(), newCampaign.getId());


        databaseService.createNotification(appUser,"We have created a campaign for you!." +
                " Please setup bank account details to ensure your fund goals are correctly " +
                "remitted. We will review the campaign shortly before approving!.",
                AppUserNotificationType.CAMPAIGN_CREATION);


        return restResponsePojo;

    }
}
