package com.enov8.seekfund.seekfund.api;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.model.CampaignCategory;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/d/api/campaign")
@Transactional
public class CampaignCategoryApi {
    @Autowired
    SessionFactory sessionFactory;

    @GetMapping("/category")
    public RestResponsePojo getCampaignCategory(
            Authentication authentication) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();




        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignCategory.class);
        criteria
                .add(Restrictions.eq("status",GenericStatusConstant.ACTIVE));



        List<CampaignCategory> campaignCategories = criteria.list();


        restResponsePojo.setData(campaignCategories);



        return restResponsePojo;

    }
}
