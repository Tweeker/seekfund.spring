package com.enov8.seekfund.seekfund.api;

import com.enov8.seekfund.seekfund.exception.RegisterException;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.i18n.phonenumbers.NumberParseException;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
//@RestControllerAdvice
@RestControllerAdvice(basePackages="com.enov8.seekfund.seekfund.api, com.enov8.seekfund.seekfund.admin.api", annotations = RestController.class)
public class ApiAdvice {
	private static final Logger logger = LoggerFactory.getLogger(ApiAdvice.class);
	@ExceptionHandler(RegisterException.class)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	RestResponsePojo registerException(Exception e, final Model model, HttpServletResponse response) {
		logger.error("Exception during execution of SpringSecurity application "+ e.getMessage());
		RestResponsePojo restPojo = new RestResponsePojo();
		restPojo.setReason(e.getMessage());
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(200);
		
		return restPojo;
	}
	@ExceptionHandler({SeekFundException.class,IllegalArgumentException.class})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	RestResponsePojo registerSeekfundException(Exception e, final Model model, HttpServletResponse response) {
		logger.error("Exception during execution of application "+ e.getMessage());
		RestResponsePojo restPojo = new RestResponsePojo();
		restPojo.setReason(e.getMessage());
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(200);

		return restPojo;
	}
	@ExceptionHandler(JsonParseException.class)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	RestResponsePojo jsonParseException(Exception e, final Model model, HttpServletResponse response) {
		logger.error("Exception during execution  application "+ e.getMessage());
		RestResponsePojo restPojo = new RestResponsePojo();
		restPojo.setReason("Invalid Json");
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(200);

		return restPojo;
	}
    @ExceptionHandler(HibernateException.class)
    public @ResponseBody
    RestResponsePojo hibernateException(Exception e, HttpServletResponse response) {
    	logger.error("Exception during execution of  application " +  e.getMessage());
		RestResponsePojo restPojo = new RestResponsePojo();
		e.printStackTrace();
		//restPojo.setReason(e.getMessage());
		restPojo.setReason("Unable to process request at the moment!.");
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(200);
		
		return restPojo;
    }
    @ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    RestResponsePojo exception(Exception e, HttpServletResponse response) {
    	logger.error("Exception during execution of  application " + e.getMessage());
    	e.printStackTrace();
		RestResponsePojo restPojo = new RestResponsePojo();
		restPojo.setReason("Unable to process request at the moment");
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		//response.setStatus(200);
		return restPojo;
    }

    @ExceptionHandler(NumberParseException.class)
	@ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    RestResponsePojo numberParseException(Exception e, HttpServletResponse response){
    	logger.error("Exception during execution of  application " + e.getMessage());
		RestResponsePojo restPojo = new RestResponsePojo();
		restPojo.setReason("Invalid Phone Number");
		restPojo.setSuccess(Boolean.FALSE);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(200);
		return restPojo;
    }

}
