package com.enov8.seekfund.seekfund.api;

import com.enov8.seekfund.seekfund.enumumeration.*;
import com.enov8.seekfund.seekfund.exception.RegisterException;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.*;
import com.enov8.seekfund.seekfund.pojo.*;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignDonationPojo;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignImagePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.pojo.request.GetPaymentRefPojo;
import com.enov8.seekfund.seekfund.service.*;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/d/api")
@Transactional
public class DefaultApi {
    private static final Logger logger = LoggerFactory.getLogger(DefaultApi.class);
    @Autowired
    CommonMethod commonMethod;
    @Autowired
    DatabaseService dbService;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    CriteriaProjectService criteriaProjectService;
    @Autowired
    CampaignService campaignService;
    @Autowired
    AppUserCreationService appUserCreationService;
    @Autowired
    CampaignDonationService campaignDonationService;
    @Autowired
    AesService aesService;
    @Autowired
    PaystackService paystackService;

    @PutMapping("/campaign-donation/ref/{campaignId}")
    public RestResponsePojo2<String> getPaymentReference(
            HttpServletRequest httpServletRequest,
            @PathVariable(value = "campaignId") Long campaignId, @RequestParam("reference") String reference)
            throws SeekFundException,Exception{
        String sessionId  = httpServletRequest.getSession().getId();
        RestResponsePojo2<String> restResponsePojo2 = new RestResponsePojo2<>();



        campaignService.verifyPayment(reference,campaignId);


        return restResponsePojo2;
    }

    @PostMapping("/campaign-donation/ref/{campaignId}")
    public RestResponsePojo2<String> getPaymentReference(
            Authentication authentication,
        @RequestBody GetPaymentRefPojo getPaymentRefPojo,
         HttpServletRequest httpServletRequest,
         @RequestParam(value = "anonymous" , required = false) String anonymous,
         @PathVariable(value = "campaignId") Long campaignId) throws Exception {
        //

        RestResponsePojo2<String> restResponsePojo2 = new RestResponsePojo2<>();
        String sessionId  = httpServletRequest.getSession().getId();


        Assert.isTrue(getPaymentRefPojo.getAmount() != null,"no amount for request");

        Assert.isTrue(getPaymentRefPojo.getAmount().floatValue() > 0.00,"Please enter a valid amount");
        Assert.notNull(sessionId,"Cannot find session id");

        CurrentCampaignPojo currentCampaign = (CurrentCampaignPojo) campaignService.getCampaignById(campaignId);

        Assert.notNull(currentCampaign,"cannot find campaign");

        Assert.isTrue(currentCampaign.getStatus().getValue().equalsIgnoreCase(CampaignStatus.RUNNING.getValue()),
                "Campaign is currently "  + currentCampaign.getStatus());



        CampaignDonation campaignDonation = campaignDonationService.
                getPendingCampaignDonation(sessionId,campaignId);

            CampaignDonation newDonation  = new CampaignDonation();
            AppUserCampaign appUserCampaign1 = new AppUserCampaign();

            boolean isValidEmail = commonMethod.isValidString(getPaymentRefPojo.getEmail()) &&
                    commonMethod.isValidEmail(getPaymentRefPojo.getEmail());
            boolean isValidPhone  = getPaymentRefPojo.getPhoneNumber() != null;
            if(isValidEmail){
                newDonation.setDonatedByEmail(getPaymentRefPojo.getEmail());
            }
            if(isValidPhone){ newDonation.setDonatedByPhone(getPaymentRefPojo.getPhoneNumber());}

            newDonation.setAmount(getPaymentRefPojo.getAmount());
            appUserCampaign1.setId(currentCampaign.getId());
            newDonation.setCampaign(appUserCampaign1);

            if(commonMethod.isValidString(getPaymentRefPojo.getComment())){
                newDonation.setComment(getPaymentRefPojo.getComment());
            }

            newDonation.setSessionId(sessionId);
            newDonation.setPaymentRef("");
            newDonation.setAmountRaised(appUserCampaign1.getAmountRaised());


            if(!commonMethod.isValidString(anonymous)
                    && commonMethod.userIsLoggedOn(authentication)){
                AppUser donatedBy  = (AppUser) authentication.getPrincipal();
                newDonation.setDonatedBy(donatedBy);
            }
            dbService.saveRecord(newDonation);

            String paymentRef  = campaignDonationService.createPaymentRef();

            newDonation.setPaymentRef(paymentRef);

            dbService.updateRecord(newDonation);

            restResponsePojo2.setData(paymentRef);
           // newDonation.set



        //}




        //if not create new donation

        //c

        return restResponsePojo2;

    }

    @GetMapping("/campaign/images/{campaignId}")
    public RestResponsePojo2<List<CampaignImagePojo>> getCampaignImage(
            Authentication authentication, @PathVariable("campaignId") Long campaiginId) throws Exception {

        RestResponsePojo2<List<CampaignImagePojo>> restResponsePojo = new RestResponsePojo2<>();


        AppUserCampaign appUserCampaign = (AppUserCampaign) dbService.getRecordById(AppUserCampaign.class,campaiginId);

        Assert.notNull(appUserCampaign,"Cannot find campaign");

        List<CampaignImagePojo> images = campaignService.getCampaignImages(appUserCampaign);

        restResponsePojo.setData(images);

        return  restResponsePojo;
    }
    @GetMapping("/comment/_q/{campaignId}")
    public RestResponsePojo2<DataTableResponsePojo2<CampaignCommentPojo>> getCampaignComment(
            HttpServletRequest request,
            @PathVariable("campaignId") Long campaignId,
            @RequestParam(name = "start" ,
                    defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Integer start,
            @RequestParam(name = "length",
                    defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Integer length
    ){

        RestResponsePojo2<DataTableResponsePojo2<CampaignCommentPojo>> restResponsePojo  =
                new RestResponsePojo2<>();



         DataTableResponsePojo2<CampaignCommentPojo>  comments =
                 campaignService.getCampaignComment(GenericStatusConstant.ACTIVE,
                    campaignId,
                    start,length);



         restResponsePojo.setData(comments);

        return  restResponsePojo;

    }
    @PostMapping("/register")
    public RestResponsePojo index(HttpServletRequest httpServletRequest,
                                  @RequestBody RegisterUserPojo registerUserPojo) throws Exception {

       // httpServletRequest.getHeaderNames();
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        boolean isValidPassword = commonMethod.isValidString(registerUserPojo.getPassword())
                && commonMethod.checkPasswordStrength(registerUserPojo.getPassword());
        // && CommonMethod.checkPasswodStrength(password);
        boolean isValidEmail = commonMethod.isValidString(registerUserPojo.getEmail()) &&
                commonMethod.isValidEmail(registerUserPojo.getEmail());
        boolean isValidUsername = commonMethod.isValidUsername(registerUserPojo.getUsername());

        if (!isValidUsername) {
            throw new RegisterException("Invalid Username!.");
        }

        if (!isValidEmail) {
            throw new RegisterException("Invalid Email!.");
        }

        if (!isValidPassword) {
            throw new RegisterException("Check Password Strength!.");
        }
        logger.info("==========Checking user creation ================");

        logger.info("===============================");

        // AppUser testUserUsername =
        // appUserDao.getAppUserByUsername(username.toLowerCase().trim());

        List<AppUser> testAppUser = null;
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        criteria.add(Restrictions.eq("username", registerUserPojo.getUsername().trim()).ignoreCase());

        testAppUser = criteria.list();

        if(testAppUser != null && !testAppUser.isEmpty()){
            throw new RegisterException("Username Already Exist");
        }
        testAppUser = null;
        criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        criteria.add(Restrictions.eq("email", registerUserPojo.getEmail().trim()).ignoreCase());

        testAppUser = criteria.list();

        if(testAppUser != null && !testAppUser.isEmpty()){
            throw new RegisterException("Email Already Exist");
        }
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);

        AppUser appUser = new AppUser();

        appUser.setEmail(registerUserPojo.getEmail().trim().toLowerCase());

        appUser.setHashedPassword(commonMethod.hashPassword(registerUserPojo.getPassword().trim()));

        appUser.setEmailConfirmed(Boolean.FALSE);

        appUser.setUsername(registerUserPojo.getUsername().trim().toLowerCase());

        appUser.setType(AppUserType.USER);

        appUser.setStatus(AppUserStatus.AWAITING_EMAIL_CONF);
        //TODO send verification email


        AppUserRole role = new AppUserRole();
        role.setName(AppUserRoleConstant.USER);
        HashSet<AppUserRole> roles = new HashSet<AppUserRole>();
        roles.add(role);
        appUser.setRoles(roles);

        dbService.saveRecord(appUser);
        try{        String encryppt  = appUserCreationService.createActivationEmailQueue(appUser);
            appUser.setHashEntry(encryppt);
        } catch (Exception e){e.printStackTrace();}


        logger.info("==========user creation complete ================");
        dbService.createAuditTrail(registerUserPojo.getUsername().trim() + " Registered with Ip : " + remoteIp, AppUser.class.getSimpleName(),
                ActivityType.INSERT, appUser.getId(), appUser.getId());

        return  restResponsePojo;


    }
    @GetMapping("/campaign/_q/{id}")
    public RestResponsePojo getCampaign(HttpServletRequest httpServletRequest,
                                  @PathVariable("id") Long id) throws Exception {

        // httpServletRequest.getHeaderNames();
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.COMPLETED,
                        CampaignStatus.RUNNING
                ))
                .add(Restrictions.eq("id",id));

        criteria.createAlias("appUser","a");

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("a.id"), "postedById")
                .add(Projections.property("a.username"), "postedByName")
                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("description"), "description")
                .add(Projections.property("dateCreated"), "dateCreated")
                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("amountRaised"), "amountRaised")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
        CurrentCampaignPojo currentCampaignPojo = (CurrentCampaignPojo) criteria.uniqueResult();

        if(currentCampaignPojo == null){
            throw new SeekFundException("Campaign is currently unavailable!.");
        }

        campaignService.incrementSearchCount(currentCampaignPojo.getId());

        restResponsePojo.setData(currentCampaignPojo);

        return restResponsePojo;

    }



    @GetMapping("/campaign/_q")
    public RestResponsePojo searchCampaign(HttpServletRequest httpServletRequest,
           @RequestParam(value
                   = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
                   defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
           @RequestParam(value =
                   Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
                   defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start,
               @RequestParam(name = "category", required=false) String category,
               @RequestParam(name = "query", required=false) String query) throws Exception {

        // httpServletRequest.getHeaderNames();
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        criteria.createAlias("c.campaignCategory", "campaignCategory");
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        countCriteria.createAlias("c.campaignCategory", "campaignCategory");
        criteria.addOrder(Order.desc("c.dateCreated"));
        criteria.addOrder(Order.desc("c.name"));

        criteria
                .add(Restrictions.eq("c.status",
                        CampaignStatus.RUNNING
                ));

        if(commonMethod.isValidString(query)){
            criteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));
            countCriteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));

        }
        if(commonMethod.isValidString(category)){

            criteria.add(Restrictions.eq("campaignCategory.name",
                    category.trim()).ignoreCase());
            countCriteria.add(Restrictions.eq("campaignCategory.name",
                    category.trim()).ignoreCase());
        }

        ProjectionList projectionList = criteriaProjectService.getProjectForSearchCampaign();
        criteria.setMaxResults(limit.intValue());

        criteria.setFirstResult((start.intValue()-1)*limit.intValue());
        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));


        List<CurrentCampaignPojo> currentCampaignPojos = (List<CurrentCampaignPojo>) criteria.list();
        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<CurrentCampaignPojo> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(currentCampaignPojos);

        restResponsePojo.setData(dtRes);

        return restResponsePojo;

    }
    @GetMapping("/campaign/_c/{categoryId}")
    public RestResponsePojo searchCampaignByCategory(HttpServletRequest httpServletRequest,
   @RequestParam(value
           = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
           defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
   @RequestParam(value =
           Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
           defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start,
   @PathVariable(name = "categoryId", required = true) Long categoryId,
   @RequestParam(name = "query", required=false) String query) throws Exception {

        // httpServletRequest.getHeaderNames();
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        criteria.createAlias("c.campaignCategory", "campaignCategory");
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        countCriteria.createAlias("c.campaignCategory", "campaignCategory");
        criteria.addOrder(Order.desc("c.dateCreated"));
        criteria.addOrder(Order.desc("c.name"));

        criteria
                .add(Restrictions.eq("c.status",
                        CampaignStatus.RUNNING
                ));

        if(commonMethod.isValidString(query)){
            criteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));
            countCriteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));

        }

        criteria.add(Restrictions.eq("campaignCategory.id",
                categoryId));
        countCriteria.add(Restrictions.eq("campaignCategory.id",
                categoryId));

        ProjectionList projectionList = criteriaProjectService.getProjectForSearchCampaign();

        criteria.setMaxResults(limit.intValue());

        criteria.setFirstResult((start.intValue()-1)*limit.intValue());
        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));


        List<CurrentCampaignPojo> currentCampaignPojos = (List<CurrentCampaignPojo>) criteria.list();
        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<CurrentCampaignPojo> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(currentCampaignPojos);

        restResponsePojo.setData(dtRes);

        return restResponsePojo;

    }
    @GetMapping("/campaigndonation/campaign/_q/{id}")
    public RestResponsePojo getCampaignDonations(HttpServletRequest httpServletRequest,
                          @PathVariable("id") Long id,
                         @RequestParam(name = "start" , defaultValue = "1") Integer start,
                             @RequestParam(name = "length", defaultValue = "10") Integer length) throws Exception {

        // httpServletRequest.getHeaderNames();
        RestResponsePojo restResponsePojo = new RestResponsePojo();


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class,"cd")
        .createAlias("cd.donatedBy", "donatedBy",JoinType.LEFT_OUTER_JOIN)
        .createAlias("cd.campaign", "campaign");
            //  .createCriteria("cd.donatedBy" , "donatedBy");
        criteria
                .add(Restrictions.eq("cd.paymentStatus",
                        PaymentStatus.COMPLETE))
                .add(Restrictions.eq("campaign.id",id));

        ProjectionList projectionList = Projections


                .projectionList()
                .add(Projections.property("cd.id"), "id")
                .add(Projections.property("cd.dateCompleted"), "dateCompleted")
                .add(Projections.property("cd.dateCreated"), "dateCreated")
                .add(Projections.property("cd.comment"), "comment")

                .add(Projections.property("cd.amountRaised"), "amountRaised")
                .add(Projections.property("cd.amount"), "amount")
                .add(Projections.property("cd.paymentStatus"), "paymentStatus")
                .add(Projections.property("donatedBy.id"),"donatedById")
                .add(Projections.property("donatedBy.username"),"donatedByUsername");



        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(CampaignDonationPojo.class));
        criteria.setFirstResult(start-1);
        criteria.setMaxResults(length);

        criteria.addOrder(Order.desc("cd.dateCompleted"));
        criteria.addOrder(Order.desc("cd.amountRaised"));
        List<CampaignDonationPojo> campaignDonations = (List<CampaignDonationPojo>)  criteria.list();


        if(campaignDonations == null){
            throw new SeekFundException("Donation is currently unavailable!.");
        }
        restResponsePojo.setData(campaignDonations);

        return restResponsePojo;

    }

    @GetMapping("/campaign/trending")
    public RestResponsePojo2<List<CurrentCampaignPojo>> getTrendingCampaign(HttpServletRequest httpServletRequest,
        @RequestParam(name = "length", defaultValue = "3") Integer length
        ) throws Exception {

        RestResponsePojo2<List<CurrentCampaignPojo>> restResponsePojo = new RestResponsePojo2<>();
        List<CurrentCampaignPojo> trendingCampaigns = campaignService.getTrendingCampaign();

        if(trendingCampaigns.size() > length){

            trendingCampaigns = trendingCampaigns.subList(0,length-1);
        }
        restResponsePojo.setData(trendingCampaigns);


        return restResponsePojo;


    }
    @GetMapping("/campaign/popular")
    public  RestResponsePojo2<DataTableResponsePojo2<PopularCampaignPojo>> getPopularCampaigns(
    @RequestParam(name = "start" , defaultValue = "1") Integer start,
   @RequestParam(name = "length", defaultValue = "10") Integer length){

        RestResponsePojo2<DataTableResponsePojo2<PopularCampaignPojo>> popularCampaigns  = new RestResponsePojo2<>();


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PopularCampaign.class,"p");
        Criteria countCriteria = sessionFactory.getCurrentSession().createCriteria(PopularCampaign.class,"p");

        criteria
                .add(Restrictions.eq("p.status",
                        GenericStatusConstant.ACTIVE));
        countCriteria
                .add(Restrictions.eq("p.status",
                        GenericStatusConstant.ACTIVE));
        criteria.createAlias("p.appUserCampaign", "c");

        ProjectionList projectionList = criteriaProjectService.getProjectionForPopularCampaign();

        criteria.setProjection(projectionList);

        criteria.setResultTransformer(Transformers.aliasToBean(PopularCampaignPojo.class));
        //criteria.setResultTransformer(new AliasToBeanNestedResultTransformer(PopularCampaignPojo.class));
        criteria.setFirstResult(start-1);
        criteria.setMaxResults(length);

        criteria.addOrder(Order.desc("p.pOrder"));

        // List<PopularCampaignPojo> popularCampaignPojos =  criteria.list();

        List<Map<String,Object>> maps  = criteria.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
        DataTableResponsePojo2<PopularCampaignPojo> dtRes  = new DataTableResponsePojo2<>();
        List<PopularCampaignPojo> popularCampaignPojos = new ArrayList<>();
        for(int i=0;i<maps.size();i++){

            Map<String,Object> map=maps.get(i);

            try{

                PopularCampaignPojo popularCampaignPojo = new PopularCampaignPojo();
                popularCampaignPojo.setId(Long.valueOf(map.get("id")+""));
                popularCampaignPojo.setOrder(Long.valueOf(map.get("order")+""));
                if(map.get("campaignImage") != null){
                    popularCampaignPojo.setCampaignImage(map.get("campaignImage")+"");
                }

                if(map.get("campaign.id") != null){
                    Long cId  =  Long.valueOf(map.get("campaign.id")+"");
                    CurrentCampaignPojo currentCampaignPojo = new CurrentCampaignPojo();
                    currentCampaignPojo.setId(cId);
                    String cName  =  map.get("campaign.name")+"";
                    currentCampaignPojo.setName(cName);
                    String cDescription =  map.get("campaign.description")+"";
                    currentCampaignPojo.setDescription(cDescription);
                    CampaignCategory cCat  =  (CampaignCategory)map.get("campaign.campaignCategory");
                    currentCampaignPojo.setCampaignCategory(cCat);
                    currentCampaignPojo.setAmount(new BigDecimal(map.get("campaign.amount")+""));
                    currentCampaignPojo.setDateCreated((Timestamp)map.get("campaign.dateCreated"));
                    currentCampaignPojo.setStatus(CampaignStatus.getEnumFromString(map.get("campaign.status")+""));
                    currentCampaignPojo.setAmountRaised(new BigDecimal(map.get("campaign.amountRaised")+""));
                    popularCampaignPojo.setCampaign(currentCampaignPojo);
                }
                popularCampaignPojos.add(popularCampaignPojo);
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }



            //
        }

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        dtRes.setData(popularCampaignPojos);
        dtRes.setRecordsTotal(totalResult);

        popularCampaigns.setData(dtRes);


        return popularCampaigns;





    }

    @PostMapping("/uploadfile")
    public String handleFileUpload(
            @RequestParam("file") MultipartFile mFile,
           RedirectAttributes redirectAttributes,
            Authentication authentication) throws Exception {

        logger.info("file name" + mFile.getName());
        logger.info("original  file name" + mFile.getOriginalFilename());
        logger.info("SIZE" + mFile.getSize());
        return "hello";
    }


}
