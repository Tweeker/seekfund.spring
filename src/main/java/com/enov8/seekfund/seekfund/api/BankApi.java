package com.enov8.seekfund.seekfund.api;

import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.Bank;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/u/api/bank")
public class BankApi {
    @Autowired
    DatabaseService databaseService;
    @GetMapping
    public RestResponsePojo getUserCampaign(Authentication authentication,
                                            @RequestParam(name ="start", defaultValue = "1") Integer start,
                                            @RequestParam(name ="length", defaultValue = "10") Integer length

    ) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();





        List<Bank> banks  = (List<Bank>) databaseService
                .getAllRecordsByHql("select b from Bank b",start-1,length);

        restResponsePojo.setData(banks);



        return restResponsePojo;


    }
}
