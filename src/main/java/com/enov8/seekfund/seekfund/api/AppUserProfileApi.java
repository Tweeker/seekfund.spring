package com.enov8.seekfund.seekfund.api;

import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.BankAccountStatus;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserBankAccount;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.Bank;
import com.enov8.seekfund.seekfund.pojo.AppUserProfilePojo;
import com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber.ResolveAccountNumberResp;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.pojo.request.AppUserPasswordRequestPojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.request.PaymentDetailsRequestPojo;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.service.PaystackService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/u/api/user")
@Transactional
public class AppUserProfileApi {
    private static final Logger logger = LoggerFactory.getLogger(AppUserProfileApi.class);
    @Autowired
    CommonMethod commonMethod;
    @Autowired
    DatabaseService databaseService;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    PaystackService paystackService;

    private com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber.Data
        resolveAccountNumber(String accountNumber, String bankCode) throws SeekFundException, Exception{
        ResolveAccountNumberResp resp =paystackService.resolveAcountNumber(accountNumber,bankCode);
        if(resp.isStatus() && resp.getData() != null){


            return resp.getData();

        }else{
            throw new SeekFundException("Cannot verify Account!.");
        }

    }
    @GetMapping
    public RestResponsePojo getUserProfileDetails(
            Authentication authentication) throws Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();





        return restResponsePojo;
    }
    @PostMapping("/paymentdetail")
    public RestResponsePojo updateUserPaymentDetails(HttpServletRequest httpServletRequest,Authentication authentication, @RequestBody PaymentDetailsRequestPojo requestPojo) throws SeekFundException,Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();


        boolean isValidAccountNumber = commonMethod.isValidAccountNumber(requestPojo.getAccountNumber());

        if(!isValidAccountNumber){ throw new SeekFundException("Invalid account Number");}

        Long countOfSameAccountNumber = (Long)
                sessionFactory.getCurrentSession().
                createCriteria(AppUserBankAccount.class)
                .add(Restrictions.eq("accountNumber", requestPojo.getAccountNumber().trim()))
                .setProjection( Projections.rowCount()).uniqueResult();

        Assert.isTrue(countOfSameAccountNumber == 0 ,"Unable to update account number, " +
                "Please try a different account number");



        Bank bank = (Bank) databaseService.getRecordById(Bank.class, requestPojo.getBankId());

        if(bank == null){ throw  new SeekFundException("Unable tor retrieve bank!.");}

        AppUser appUser  = (AppUser)authentication.getPrincipal();

        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(AppUserBankAccount.class);

        criteria.add(Restrictions.eq("appUser.id", appUser.getId()));

        AppUserBankAccount appUserBankAccount = (AppUserBankAccount) criteria.uniqueResult();
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);
        if(appUserBankAccount == null){


            appUserBankAccount  = new AppUserBankAccount();

            appUserBankAccount.setAccountNumber(requestPojo.getAccountNumber().trim());
            appUserBankAccount.setAppUser(appUser);
            appUserBankAccount.setBank(bank);
           // appUserBankAccount.setStatus(BankAccountStatus.VALIDATING);
            appUserBankAccount.setStatus(BankAccountStatus.ACTIVE);
            appUserBankAccount.setAccountName(
                    resolveAccountNumber(appUserBankAccount.getAccountNumber(),
                    appUserBankAccount.getBank().getCode())
                    .getAccountName()
            );

            databaseService.saveRecord(appUserBankAccount);
            databaseService.createAuditTrail(appUser.getUsername().trim() + " Added AppUserBank Account  with Ip : "
                            + remoteIp, AppUserBankAccount.class.getSimpleName(),
                    ActivityType.INSERT, appUser.getId(), appUser.getId());



        }else{

            if(appUserBankAccount.getStatus()
                    .getValue().equalsIgnoreCase(BankAccountStatus.BLOCKED.getValue()) ||
                    appUserBankAccount.getStatus()
                    .getValue().equalsIgnoreCase(BankAccountStatus.SUSPENDED.getValue())  ){
                throw new SeekFundException("You are currently unable to make changes to your account!");

            }
           
            appUserBankAccount.setAccountNumber(requestPojo.getAccountNumber().trim());
            appUserBankAccount.setBank(bank);
            appUserBankAccount.setStatus(BankAccountStatus.ACTIVE);
            appUserBankAccount.setDateModified(new Timestamp(System.currentTimeMillis()));


            appUserBankAccount.setAccountName(
                    resolveAccountNumber(appUserBankAccount.getAccountNumber(),
                            appUserBankAccount.getBank().getCode())
                            .getAccountName()
            );
            databaseService.updateRecord(appUserBankAccount);
            databaseService.createAuditTrail(appUser.getUsername().trim() + " Updated AppUserBank Account  with Ip : "
                            + remoteIp, AppUserBankAccount.class.getSimpleName(),
                    ActivityType.UPDATE, appUser.getId(), appUser.getId());


        }

        return restResponsePojo;


    }
    @GetMapping("/paymentdetail")
    public RestResponsePojo getPaymentDetails(HttpServletRequest httpServletRequest,Authentication authentication) throws SeekFundException,Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        AppUser appUser  = (AppUser)authentication.getPrincipal();


        String hql  = " select a.id as id, a.bank as bank," +
                " a.accountNumber as accountNumber, " +
                "a.accountName as accountName , a.dateCreated as dateCreated, " +
                "a.dateModified as dateModified, a.status as status " +
                " from AppUserBankAccount a  " +
                "  where a.appUser.id  = " + appUser.getId();



         AppUserBankAccount appUserBankAccount  = (AppUserBankAccount)
            databaseService.getUniqueRecordByHql(hql,AppUserBankAccount.class);



        restResponsePojo.setData(appUserBankAccount);




        return restResponsePojo;
    }
    @GetMapping("/profile")
    public RestResponsePojo getUserProfile(HttpServletRequest httpServletRequest,Authentication authentication) throws SeekFundException,Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();
        AppUser appUser  = (AppUser)authentication.getPrincipal();

        String hql  =" select a.id as id, a.username as username , " +
                "a.email as email, " +
                " a.dateCreated as dateCreated" +
                " " +
                " from AppUser a where a.id = " + appUser.getId();


        AppUserProfilePojo appUserProfilePojo  = (AppUserProfilePojo) databaseService.getUniqueRecordByHql(hql, AppUserProfilePojo.class);



        //TODO get OAUTH details
        //
        restResponsePojo.setData(appUserProfilePojo);


        return restResponsePojo;
    }
    @PostMapping("/password")
    public RestResponsePojo updateUserPassword(HttpServletRequest httpServletRequest,Authentication authentication, @RequestBody AppUserPasswordRequestPojo requestPojo) throws SeekFundException,Exception {
        RestResponsePojo restResponsePojo = new RestResponsePojo();

        boolean isValidPassword  = requestPojo.getPassword() != null && commonMethod.checkPasswordStrength(requestPojo.getPassword());

        if(!isValidPassword){ throw new SeekFundException("Password too weak or invalid!.");}



        AppUser appUser  = (AppUser)authentication.getPrincipal();

        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class,"appUser");

        criteria.add(Restrictions.eq("appUser.id", appUser.getId()));

        appUser = (AppUser) criteria.uniqueResult();

        if(appUser == null){ throw new SeekFundException("Cannot find User Bank Details");}






        if(appUser.getHashedPassword() != null){

            //TODO ensure new password is not same as old

            if(commonMethod.isValidPassword(requestPojo.getPassword().trim(),
                    appUser.getHashedPassword())){
                throw new SeekFundException("Password cannot be the same as previous");
            }
            if(!commonMethod.isValidPassword(requestPojo.getCurrentPassword().trim(),
                    appUser.getHashedPassword())){

                throw new SeekFundException("Password Does not match!.");
            }

        }

        appUser.setHashedPassword(commonMethod.hashPassword(requestPojo.getPassword()));

        databaseService.updateRecord(appUser);

        logger.info("Updated User Details  ------ for  " + appUser.getUsername());

        //TODO send email
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);
        databaseService.createAuditTrail(
                appUser.getUsername().trim() + " Updated Password  with Ip : " + remoteIp, AppUser.class.getSimpleName(),
                ActivityType.UPDATE, appUser.getId(), appUser.getId());


        return restResponsePojo;
    }
}
