package com.enov8.seekfund.seekfund.dao;

import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class AppUserDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	CommonMethod commonMethod;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public AppUser getAppUserByUsername(String username) throws HibernateException, Exception {
		Session session = getSession();

		Criteria criteria = session.createCriteria(AppUser.class);

		criteria.add(Restrictions.eq("username", username).ignoreCase());

		return (AppUser) criteria.uniqueResult();

	}
	public AppUser getAppUserByEmail(String email) throws HibernateException, Exception {
		Session session = getSession();

		Criteria criteria = session.createCriteria(AppUser.class);

		criteria.add(Restrictions.eq("email", email).ignoreCase());

		return (AppUser) criteria.uniqueResult();

	}
	public AppUser getAppUserById(Long id) throws HibernateException, Exception {
		Session session = getSession();

		Criteria criteria = session.createCriteria(AppUser.class);

		criteria.add(Restrictions.eq("id", id));

		return (AppUser) criteria.uniqueResult();

	}
	@SuppressWarnings("deprecation")
	public AppUser getAppUserByUsernameorEmail(String usernameOrEmail) throws HibernateException, Exception {
		Session session = getSession();
		boolean isValidEmail = commonMethod.isValidEmail(usernameOrEmail);
		boolean isValidUsername = commonMethod.isValidUsername(usernameOrEmail);
		

		Criteria criteria = null;
		
		if(isValidEmail){
			
			criteria = session.createCriteria(AppUser.class);
			criteria.add(Restrictions.eq("email", usernameOrEmail.toLowerCase()));
		}
		else if(isValidUsername) {

			criteria = session.createCriteria(AppUser.class);
			criteria.add(Restrictions.eq("username", usernameOrEmail.toLowerCase()));
		}

		if(criteria == null){
			
			throw new Exception("Invalid Username or Email");
			
		}

		return (AppUser) criteria.uniqueResult();
		
	}
	

}
