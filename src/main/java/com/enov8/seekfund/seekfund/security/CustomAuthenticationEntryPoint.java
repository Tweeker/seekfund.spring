package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.util.CommonMethod;

import com.enov8.seekfund.seekfund.util.Constant;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint  {
	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);

	@Autowired
	CommonMethod commonMethod;
	@Override
	public void commence(HttpServletRequest request,
                         HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {
		logger.info("Custom authentication Entry Point Kicking in ");
		//redirectStrategy.sendRedirect(request, response, "/login");
		
		
			String baseURL = CommonMethod.getBaseURL(request);
			String accept  = request.getHeader("Accept");
			if (commonMethod.isValidString(accept)
					&& (Constant.CONTENT_TYPE_JSON.equals(accept)
				|| accept.toLowerCase().contains(Constant.CONTENT_TYPE_JSON.toLowerCase()))) {

			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			response.addHeader("WWW-Authenticate", "FormBased");
			//;
			JSONObject jObject = new JSONObject();
	        jObject.put("success", Boolean.FALSE);
	        jObject.put("reason",authException.getMessage());
	        jObject.put("access-denied", Boolean.TRUE);
	        jObject.put("cause", "SC_UNAUTHORIZED");
	        response.setStatus(401);
	        PrintWriter out = response.getWriter();
	        out.println(jObject.toString());
	        out.flush();
			out.close();
			
			
		}else{
			String queryString = request.getQueryString() ;
			if(commonMethod.isValidString(queryString)){
				queryString += "?" +queryString;
			}else{queryString="";}
			response.sendRedirect(baseURL.trim() + "/login" /*+ "?done=" + baseURL.trim()+
					request.getServletPath().trim() + queryString*/
					);}
		
	}
	


}
