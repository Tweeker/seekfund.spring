package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.model.AppUser;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedPrincipalExtractor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Map;

public class CustomFixedPrincipalExtractor extends FixedPrincipalExtractor {
    private static final String[] PRINCIPAL_KEYS = new String[]{"user", "username", "userid", "user_id", "login", "id", "name"};

    public Object extractPrincipal(Map<String, Object> map) {
        String[] var2 = PRINCIPAL_KEYS;
        int var3 = var2.length;
        SecurityContext context =  SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        AppUser appUser = new AppUser();
        for (int var4 = 0; var4 < var3; ++var4) {
            String key = var2[var4];
            if (map.containsKey(key)) {

                if(authentication != null
                        && authentication.getPrincipal() != null
                        && authentication.getPrincipal() instanceof AppUser){

                    appUser = (AppUser) authentication.getPrincipal();
                    appUser.setSocialPrincipal(map.get(key));

                    return appUser;
                }
                return map.get(key);

            }
        }

        return null;

    }
}
