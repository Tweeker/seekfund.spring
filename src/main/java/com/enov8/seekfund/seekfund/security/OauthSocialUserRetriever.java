package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.enumumeration.SocialAuthType;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.SocialAuthUser;
import com.enov8.seekfund.seekfund.pojo.TwitterOauthDetails;
import com.enov8.seekfund.seekfund.pojo.twitter.verifycredentials.VerifyCredentials;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.security.core.Authentication;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Base64;

public class OauthSocialUserRetriever {
    public SocialAuthUser getSocialUser(Authentication auth2Authentication, String token,SocialAuthType authType, boolean isLoggedIn){
        SocialAuthUser socialAuthUser = null;
        try{
            if(authType.getValue().equalsIgnoreCase(SocialAuthType.FACEBOOK.getValue())){
               socialAuthUser =  getFacebookSocialUser(auth2Authentication,token,isLoggedIn);
            }else if(authType.getValue().equalsIgnoreCase(SocialAuthType.GOOGLE.getValue())){
                socialAuthUser =  getGoogleUser(auth2Authentication,token,isLoggedIn);
            }else if(authType.getValue().equalsIgnoreCase(SocialAuthType.TWITTER.getValue())){
               // socialAuthUser = getTwitterUser(auth2Authentication,token,isLoggedIn);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        return socialAuthUser;

    }

    public SocialAuthUser getTwitterUser(TwitterProfile twitterProfile,
                                         String token, boolean isLoggedIn, OAuthToken authToken) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        SocialAuthUser socialAuthUser = new SocialAuthUser();
        String authId  = null;

        authId = twitterProfile.getId()+"";
        socialAuthUser.setAuth_id( authId );
        socialAuthUser.setAuthType(SocialAuthType.TWITTER);
        socialAuthUser.setToken(token);
        TwitterOauthDetails twitterOauthDetails = new TwitterOauthDetails(authToken,twitterProfile);
        String details = "";

        details  = objectMapper
                .writeValueAsString(twitterOauthDetails);

        socialAuthUser
                .setCredentials(details);


        if(twitterProfile != null){

            if(twitterProfile.getName() != null){
                socialAuthUser.setFullName(twitterProfile.getName() + " , " + twitterProfile.getScreenName());
            }else{
                socialAuthUser.setFullName("Anonymous");
            }
           if(twitterProfile.getProfileImageUrl() != null){

               try{
                   String encodedImage  =  getEncodedImageFromUrl(twitterProfile.getProfileImageUrl()) ;
                   socialAuthUser.setProfileImage(encodedImage);
               } catch (Exception e){
                   e.printStackTrace();
               }

           }

        }
        //set default email since twitter makes email not required.

        socialAuthUser.setEmail(twitterProfile.getId() +"@twitter.com");

 /*       RestTemplate restTemplate = ((TwitterTemplate) twitter).getRestTemplate();

        VerifyCredentials verifyCredentials = restTemplate
                .getForObject("https://api.twitter.com/1.1/account/" +
                                "verify_credentials.json?include_email=true",
                        VerifyCredentials.class);*/




        return  socialAuthUser;

    }

    private  SocialAuthUser getGoogleUser(Authentication auth2Authentication, String token, boolean isLoggedIn) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        SocialAuthUser socialAuthUser = new SocialAuthUser();
        String authId  = null;
        if(!isLoggedIn){  authId = auth2Authentication.getPrincipal().toString();}
        else{
            authId = ((AppUser)auth2Authentication.getPrincipal()).getSocialPrincipal().toString();

        }
        socialAuthUser.setAuth_id( authId );
        socialAuthUser.setAuthType(SocialAuthType.GOOGLE);
        socialAuthUser.setToken(token);
        String details  = objectMapper.writeValueAsString(auth2Authentication.getDetails());
        socialAuthUser
                .setCredentials(details);
        if(details != null){
            JsonNode node = objectMapper.readTree(details);
            if(node.has("name")){
                socialAuthUser.setFullName(node.get("name")
                        .toString().replace("\"", ""));
            }else{
                socialAuthUser.setFullName("Anonymous");
            }
            if(node.has("email")){
                socialAuthUser.setEmail(node.get("email").toString().replace("\"", ""));
            }

            if(node.has("picture")){
                String pictureUrl  = node.get("picture")
                        .toString().replace("\"", "");

                try{
                    String encodedImage  =  getEncodedImageFromUrl(pictureUrl) ;
                    socialAuthUser.setProfileImage(encodedImage);
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }



        return socialAuthUser;

    }


    private String getEncodedImageFromUrl(String pictureUrl) throws Exception{

            java.net.URL url = new java.net.URL(pictureUrl);
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.connect();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(conn.getInputStream(), baos);

            baos.toByteArray();
            String encodedImage  =  Base64.getEncoder().encodeToString(baos.toByteArray()) ;


            return encodedImage;

    }
    //th:text="${#authentication.principal.profileImage}"
    private  SocialAuthUser getFacebookSocialUser(Authentication auth2Authentication, String token, boolean isLoggedIn) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        SocialAuthUser socialAuthUser = new SocialAuthUser();
        String authId  = null;
        if(!isLoggedIn){  authId = auth2Authentication.getPrincipal().toString();}
        else{
            authId = ((AppUser)auth2Authentication.getPrincipal()).getSocialPrincipal().toString();

        }
        socialAuthUser.setAuth_id( authId );
        socialAuthUser.setAuthType(SocialAuthType.FACEBOOK);
        socialAuthUser.setToken(token);
        String details  = objectMapper.writeValueAsString(auth2Authentication.getDetails());
        socialAuthUser
                .setCredentials(details);
        if(details != null){
            JsonNode node = objectMapper.readTree(details);
            if(node.has("name")){
                socialAuthUser.setFullName(node.get("name")
                        .toString().replace("\"", ""));
            }else{
                socialAuthUser.setFullName("Anonymous");
            }
            if(node.has("email")){
                socialAuthUser.setEmail(node.get("email").toString().replace("\"", ""));
            }
        }
        Facebook facebook = new FacebookTemplate(token);
        User user  = facebook.userOperations().getUserProfile();

        //String email = facebook.userOperations().getUserProfile().getEmail();
        byte[] image  = facebook.userOperations().getUserProfileImage();
        String encodedImage = Base64.getEncoder().encodeToString(image);

        if(socialAuthUser.getEmail() == null){ socialAuthUser.setEmail(   user.getEmail()); }
        socialAuthUser.setProfileImage(encodedImage);


        return socialAuthUser;

    }
}
