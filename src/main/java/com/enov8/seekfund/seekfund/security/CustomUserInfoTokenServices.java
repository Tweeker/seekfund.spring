package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.model.AppUser;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.util.*;

public class CustomUserInfoTokenServices extends UserInfoTokenServices {
    private String clientId;
    public CustomUserInfoTokenServices(String userInfoEndpointUrl, String clientId) {
        super(userInfoEndpointUrl, clientId);


        super.setPrincipalExtractor(new CustomFixedPrincipalExtractor());
        super.setAuthoritiesExtractor(new CustomFixedAuthoritiesExtractor());

        this.clientId = clientId;
    }




  /*  private OAuth2Authentication extractAuthentication(Map<String, Object> map) {
        SecurityContext context =  SecurityContextHolder.getContext();

        Authentication authentication = context.getAuthentication();
        UsernamePasswordAuthenticationToken token = null;
        Object principal = this.getPrincipal(map);
        Collection<? extends GrantedAuthority> authorities = new ArrayList<>();
        AppUser appUser = new AppUser();
        appUser.setSocialPrincipal(principal);
        if(authentication != null
                && authentication.getPrincipal() != null
        && authentication.getPrincipal() instanceof AppUser){

            appUser = ((AppUser) authentication.getPrincipal());

            appUser.setSocialPrincipal(principal);
            authorities = authentication.getAuthorities();



        }

    //    List<GrantedAuthority> authorities = this.authoritiesExtractor.extractAuthorities(map);
        OAuth2Request request = new OAuth2Request((Map)null, this.clientId, (Collection)null, true, (Set)null, (Set)null, (String)null, (Set)null, (Map)null);

        token = new UsernamePasswordAuthenticationToken(appUser, "N/A", authorities);
        token.setDetails(map);
        return new OAuth2Authentication(request, token);
    }*/
}
