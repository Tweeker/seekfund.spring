package com.enov8.seekfund.seekfund.security;


import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;
import com.enov8.seekfund.seekfund.model.AppSetting;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.pojo.LoginPojo;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	@Autowired
	DatabaseService dbService;
	@Autowired
	CommonMethod commonMethod;
	RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		// String targetUrl = determineTargetUrl(authentication);
		/*
		 * if (response.isCommitted()) { logger.
		 * debug("Response has already been committed. Unable to redirect to " +
		 * targetUrl); return; }
		 */

		AppSetting SESSION_TIME_OUT = dbService.getAppSettingByName(Constant.SESSION_TIMEOUT,
				Constant.SESSION_TIMEOUT_VALUE.toString());
		request.getSession().setMaxInactiveInterval(Integer.parseInt(SESSION_TIME_OUT.getValue())*60);
		AppUser appUser = (AppUser) authentication.getPrincipal();
		appUser.setLastLoginIP(CommonMethod.getRemoteIp(request));
		appUser.setLoginFailCounter(0);
		appUser.setLoginCounter(appUser.getLoginCounter()+1);
		appUser.setLastLoginDate(new Timestamp(System.currentTimeMillis()));

		try {
			dbService.updateRecord(appUser);
		} catch (Exception e) {

			e.printStackTrace();
		}

		JSONObject res = new JSONObject();
		res.put(Constant.STRING_SUCCESS, true);
		LoginPojo pojo = determinLoginPojo(authentication);
		res.put("data", new JSONObject(pojo));
		response.setContentType(Constant.CONTENT_TYPE_JSON);
		response.getWriter().write(res.toString());

		// redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	private String determineTargetUrl(Authentication authentication) {

		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		AppUser appUser = (AppUser) authentication.getPrincipal();
		// System.out.println(appUser.getEmail());
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals("USER")) {
				return "/login";
			}
		}
		return null;
	}

	private LoginPojo determinLoginPojo(Authentication authentication) {
		LoginPojo loginPojo = new LoginPojo();



		return loginPojo;
	}

}
