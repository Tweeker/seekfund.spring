package com.enov8.seekfund.seekfund.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomSimpleUrlAuthenticationSuccessHandler  extends
SimpleUrlAuthenticationSuccessHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(CustomSimpleUrlAuthenticationSuccessHandler.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		logger.info("=============================");
		logger.info("Hitting custom success handler");
		logger.info("=============================");
	
		
	/*	Set<String> roles = AuthorityUtils.authorityListToSet(authentication
				.getAuthorities());
		getRedirectStrategy().sendRedirect(request, response,
				"/demoadminpage");*/
	
			super.onAuthenticationSuccess(request, response, authentication);
			return;
		
	}
}
