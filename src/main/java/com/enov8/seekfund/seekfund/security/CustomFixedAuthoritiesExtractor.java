package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.model.AppUser;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CustomFixedAuthoritiesExtractor implements AuthoritiesExtractor {
    private static final String AUTHORITIES = "authorities";
    private static final String[] AUTHORITY_KEYS = new String[]{"authority", "role", "value"};

    public CustomFixedAuthoritiesExtractor() {
    }

    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
        String authorities = "social_auth";
        SecurityContext context =  SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        if(authentication != null
                && authentication.getPrincipal() != null
                && authentication.getPrincipal() instanceof AppUser){

            return (List<GrantedAuthority>) authentication.getAuthorities();
        }

        if (map.containsKey("authorities")) {

            authorities = this.asAuthorities(map.get("authorities"));
        }

        return AuthorityUtils.commaSeparatedStringToAuthorityList(authorities);
    }

    private String asAuthorities(Object object) {
        List<Object> authorities = new ArrayList();
        if (object instanceof Collection) {
            Collection<?> collection = (Collection)object;
            object = collection.toArray(new Object[0]);
        }

        if (ObjectUtils.isArray(object)) {
            Object[] array = (Object[])((Object[])object);
            Object[] var4 = array;
            int var5 = array.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                Object value = var4[var6];
                if (value instanceof String) {
                    authorities.add(value);
                } else if (value instanceof Map) {
                    authorities.add(this.asAuthority((Map)value));
                } else {
                    authorities.add(value);
                }
            }

            return StringUtils.collectionToCommaDelimitedString(authorities);
        } else {
            return object.toString();
        }
    }

    private Object asAuthority(Map<?, ?> map) {
        if (map.size() == 1) {
            return map.values().iterator().next();
        } else {
            String[] var2 = AUTHORITY_KEYS;
            int var3 = var2.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                String key = var2[var4];
                if (map.containsKey(key)) {
                    return map.get(key);
                }
            }

            return map;
        }
    }
}
