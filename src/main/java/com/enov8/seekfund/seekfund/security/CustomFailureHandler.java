package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.util.Constant;

import org.json.JSONObject;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomFailureHandler  extends
SimpleUrlAuthenticationFailureHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	String urltoRedirect;
	
/*	
	public CustomFailureHandler(final String urltoredirect) {
		super(urltoredirect);
		this.urltoRedirect = urltoredirect;
	}*/

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		JSONObject res = new JSONObject();
		res.put(Constant.STRING_SUCCESS,false);
		res.put("reason", exception.getMessage());
		response.setContentType(Constant.CONTENT_TYPE_JSON);
		response.getWriter().write(res.toString());

	/*	String errordetail = exception.getMessage();
		redirectStrategy.sendRedirect(request, response, urltoRedirect + "?username="
				+ username + "&errordetail=" + errordetail);*/

	}
}
