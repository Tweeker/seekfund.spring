package com.enov8.seekfund.seekfund.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {
	private static final Logger logger = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse httpServletResponse, AccessDeniedException e)
			throws IOException, ServletException {
		
		httpServletResponse.getWriter().append("Access Denied");
		
		logger.info("Access Denied");
		httpServletResponse.setStatus(409);

		httpServletResponse.sendRedirect(request.getContextPath() + "/accessDenied");
		
	}

}
