package com.enov8.seekfund.seekfund.security;


import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.model.AppSetting;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class CustomAuthenticationProvider implements AuthenticationProvider {
	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	@Autowired
	AppUserDao appUserService;
	@Autowired
	DatabaseService dbService;
	@Autowired
	CommonMethod commonMethod;
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String usernamerEmail = authentication.getName();
		String password = (String) authentication.getCredentials();
		AppUser appUser = null;

		String errMsg = null;
		try {
			appUser = appUserService.getAppUserByUsernameorEmail(usernamerEmail);
		} catch (Exception e) {
	
			e.printStackTrace();
			
			throw new BadCredentialsException(e.getMessage());
		}
		if (appUser == null) {
			throw new BadCredentialsException("Not A Valid User");
		}
/*
		appUser.setStatus(AppUserStatus.ACTIVE);
		try {
			dbService.updateRecord(appUser);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			dbService.updateRecord(appUser);
		} catch (HibernateException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/

		if(appUser.getStatus().getValue().equalsIgnoreCase(AppUserStatus.AWAITING_EMAIL_CONF.getValue())){

			errMsg = "Login in to email and follow the link to activate account!." ;
		}
		else if(!appUser.getStatus().getValue().equalsIgnoreCase(AppUserStatus.ACTIVE.getValue())){
			
			errMsg = "Account is currently " + appUser.getStatus().getValue() +"!." ;
		}
	
		//todo check for email confirmation//
		
		else if (!commonMethod.isValidPassword(password, appUser.getHashedPassword())) {
			errMsg = "Invalid Login Credentials";
			appUser.setLoginFailCounter(appUser.getLoginFailCounter()+1);
			AppSetting maxFailedLoginSetting = dbService.getAppSettingByName(Constant.DEFAULT_MAX_FAILED_LOGIN_NAME,
					Constant.DEFAULT_MAX_FAILED_LOGIN_VAL.toString());
					
					
			if(appUser.getLoginFailCounter() > Integer.valueOf(maxFailedLoginSetting.getValue()) ){
				
				appUser.setStatus(AppUserStatus.LOCKED);
			}
			
			errMsg = "Invalid Login Credentials";
		}
		
		
		
		if(commonMethod.isValidString(errMsg)){
			try {
				dbService.updateRecord(appUser);
				dbService.createAuditTrail(errMsg , AppUser.class.getSimpleName(), 
						ActivityType.LOGIN, appUser.getId(), appUser.getId());
			}catch (Exception e) {

				e.printStackTrace();
			}
			
			throw new BadCredentialsException(errMsg);
		}
		
		
		
		
		
		

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Set<AppUserRole> roles = appUser.getRoles();
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			AppUserRole appUserRole = (AppUserRole) iterator.next();
			logger.info("===================roles=====================");
			logger.info(appUserRole.getName().getValue());
			logger.info("===================roles=====================");
			//
			authorities.add(new SimpleGrantedAuthority(appUserRole.getName().getValue()));
		}
		
		try {
		
			dbService.createAuditTrail("Succesfully logged", AppUser.class.getSimpleName(), 
					ActivityType.LOGIN, appUser.getId(), appUser.getId());
		}catch (Exception e) {

			e.printStackTrace();
		}
			
		return new UsernamePasswordAuthenticationToken(appUser,appUser.getHashedPassword(),
				authorities);


	
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication));
	}

}
