package com.enov8.seekfund.seekfund.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@SuppressWarnings("serial")
public class CustomUsernamePasswordAuthenticationToken  extends
UsernamePasswordAuthenticationToken{
	public CustomUsernamePasswordAuthenticationToken(Object principal,
			Object credentials) {
		super(principal, credentials);
	}
}
