package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserDetailsService implements UserDetailsService {
	@Autowired
	AppUserDao appUserDao;

	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		AppUser user = null;
		;
		try {
			user = appUserDao.getAppUserByUsernameorEmail(usernameOrEmail);
			if (user == null) {

				new UsernameNotFoundException(String.format("User with login=%s was not found", usernameOrEmail));
			}

		} catch (Exception e) {

			e.printStackTrace();
			throw new UsernameNotFoundException(String.format("Cannot Retrieve User"));
		}

		return new CurrentUser(user);

	}

}
