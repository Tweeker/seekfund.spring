package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private static Logger log = LoggerFactory.getLogger(CustomUsernamePasswordAuthenticationFilter.class);
	@Autowired
	CommonMethod commonMethod;
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		//log.info("Attempting authentication with filter");
		//log.info("header ======================");
		//log.info(request.getHeader("Content-Type"));
		String password = null;
		String usernameOrEmail = null;
		if (!request.getMethod().equals(Constant.REQUEST_METHOD_POST)) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}
		UsernamePasswordAuthenticationToken authRequest = null;
		if (Constant.CONTENT_TYPE_JSON.equals(request
				.getHeader(Constant.HEADER_CONTENT_TYPE))
				|| Constant.CONTENT_TYPE_JSON_CHAR_SET.
				equalsIgnoreCase(request.getHeader(Constant.HEADER_CONTENT_TYPE))
				)
		 {
			LoginRequest loginRequest = null;

			try {
				loginRequest = this.getLoginRequest(request);
			} catch (Exception e) {

				// e.printStackTrace();
				throw new AuthenticationServiceException("Invalid Auth Object ");
			}
			usernameOrEmail= loginRequest.getUsernameOrEmail();
			password = loginRequest.getPassword();
			

		} else {
			usernameOrEmail= request.getParameter(Constant.USERNAME_EMAIL_LOWER);
			password = request.getParameter(Constant.PASSOWRD_LOWER);
			
		}
		
		if(!commonMethod.isValidString(usernameOrEmail) || !commonMethod.isValidString(password)){
			throw new AuthenticationServiceException("No Auth Parameters ");
		}
		authRequest = new UsernamePasswordAuthenticationToken(usernameOrEmail, password);
			
		setDetails(request, authRequest);

		log.info("============== about to call getAuthMan ================");
		return this.getAuthenticationManager().authenticate(authRequest);

	}

	private LoginRequest getLoginRequest(HttpServletRequest request) throws Exception {
		BufferedReader reader = null;
		LoginRequest loginRequest = new LoginRequest();
		try {
			/*
			 * HttpServletRequest can be read only once
			 */
			StringBuffer sb = new StringBuffer();
			String line = null;

			reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			//log.info(sb.toString());
			JSONObject jObject = new JSONObject(sb.toString());

			loginRequest.setPassword(jObject.optString(Constant.PASSOWRD_LOWER, ""));
			loginRequest.setUsernameOrEmail(jObject.optString(Constant.USERNAME_EMAIL_LOWER, ""));

		} catch (Exception e) {

			throw e;
		}
		return loginRequest;
	}

}
