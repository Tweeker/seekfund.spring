package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.model.AppUser;
import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User {
	private AppUser appUser;

    public CurrentUser(AppUser appUser) {
        super(appUser.getEmail(), appUser.getHashedPassword(), AuthorityUtils.createAuthorityList());
        this.appUser = appUser;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public Long getId() {
        return appUser.getId();
    }

 

}
