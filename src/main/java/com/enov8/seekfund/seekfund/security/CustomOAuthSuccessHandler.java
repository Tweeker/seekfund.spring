package com.enov8.seekfund.seekfund.security;

import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;
import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.enumumeration.SocialAuthType;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.model.SocialAuthUser;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class CustomOAuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomOAuthSuccessHandler.class);
    private SocialAuthType authType;

    DatabaseService databaseService;

    public CustomOAuthSuccessHandler(SocialAuthType authType, DatabaseService databaseService){

        this.authType  = authType;
        this.databaseService = databaseService;

    }
    public CustomOAuthSuccessHandler(){
        super();
    }
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        logger.info("Auth TYpe  :: " + authType + " : value " + authType.getValue());
        String token = ((OAuth2AuthenticationDetails) ((OAuth2Authentication) authentication).getDetails()).getTokenValue();
        //Facebook facebook = new FacebookTemplate(token);

        SecurityContext context =  SecurityContextHolder.getContext();



        SocialAuthUser socialAuthUser = null;

        String hql  = "";
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        AppUser appUser = new AppUser();
        appUser.setSocialAuthType(authType);

        AppUserRole role = new AppUserRole();
        role.setName(AppUserRoleConstant.SOCIAL_AUTH);
        HashSet<AppUserRole> roles = new HashSet<AppUserRole>();
        roles.add(role);
        Authentication userAuth = null;
        boolean isLoggedIn  =  false;
        if(authentication != null
                && authentication.getPrincipal() != null
                && authentication.getPrincipal() instanceof AppUser
                && ((AppUser) authentication.getPrincipal()).getId() != null){

            isLoggedIn= true;
        }
        try{
            if(isLoggedIn){
                //user is authenticated
                hql  = "select a from SocialAuthUser a where a.auth_id " +
                        "= '" + ((AppUser) authentication.getPrincipal()).getSocialPrincipal().toString()+ "' and a.authType =  " +
                        "'" + authType + "'";



            }else{

                //user is not authenticated
                hql  = "select a from SocialAuthUser a where a.auth_id " +
                        "= '" + authentication.getPrincipal().toString()+ "' and a.authType =  " +
                        "'" + authType + "'";
                appUser.setSocialPrincipal(authentication.getPrincipal());

            }

            socialAuthUser = (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);

            if(socialAuthUser == null){

                OauthSocialUserRetriever oauthSocialUserRetriever = new OauthSocialUserRetriever();

                Authentication auth2Authentication =   ((OAuth2Authentication) authentication)
                        .getUserAuthentication();
                socialAuthUser = oauthSocialUserRetriever.
                        getSocialUser(auth2Authentication,token,authType, isLoggedIn);


                databaseService.saveRecord(socialAuthUser);

                authorities =  getAuthorites(roles);

                if(isLoggedIn){
                    this.setDefaultTargetUrl("/home");
                }else{
                    this.setDefaultTargetUrl(Constant.SOCIAL_REGISTER_ROUTE);
                }

                userAuth = new UsernamePasswordAuthenticationToken(appUser,"",
                        authorities);
                //todo set userauth here


            }else{

                if(isLoggedIn){
                    socialAuthUser.setStatus(GenericStatusConstant.ACTIVE);
                    socialAuthUser.setAppUser((AppUser)authentication.getPrincipal());
                    databaseService.updateRecord(socialAuthUser);
                    //TODO ENSURE THAT IF USER IS ALREADY TIED WITH A SOCIAL AUTH USER ,...
                    // THE LOGGED IN USER IS THE SAME AS SOCIAL AUTH USER
                    this.setDefaultTargetUrl("/home");

                }else{
                    //user is not logged in
                    if(socialAuthUser.getStatus().getValue()
                            .equalsIgnoreCase(GenericStatusConstant.ACTIVE.getValue())
                            && socialAuthUser.getAppUser() != null){

                        appUser = (AppUser) databaseService.getRecordById(AppUser.class,
                                socialAuthUser.getAppUser().getId());
                        authorities =  getAuthorites(appUser.getRoles());

                        userAuth = new UsernamePasswordAuthenticationToken(appUser,appUser.getHashedPassword(),
                                authorities);
                        //context.setAuthentication(userAuth);
                        this.setDefaultTargetUrl("/home");
                    }else{
                        authorities =  getAuthorites(roles);

                        userAuth = new UsernamePasswordAuthenticationToken(appUser,"",
                                authorities);
                        //context.setAuthentication(userAuth);
                        this.setDefaultTargetUrl(Constant.SOCIAL_REGISTER_ROUTE);

                    }

                }


            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        //TODo implement

       /* authentication.getCredentials();

        //if
        if(authType.getValue().equalsIgnoreCase(SocialAuthType.FACEBOOK.getValue())){
            Facebook facebook = new FacebookTemplate(token);

            User user  = facebook.userOperations().getUserProfile();
            String email = facebook.userOperations().getUserProfile().getEmail();
            byte[] image  = facebook.userOperations().getUserProfileImage();
            File file = new File("C:\\Users\\PErE\\dev\\java\\test.jpg");

            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(image);
            } finally {
                stream.close();
            }


        }*/


        //this.setDefaultTargetUrl("/home");


        //TODO replace user auth
        context.setAuthentication(userAuth);
        super.onAuthenticationSuccess(request, response, userAuth);
    }

  private Collection<GrantedAuthority> getAuthorites( HashSet<AppUserRole> roles){
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
            AppUserRole appUserRole = (AppUserRole) iterator.next();
            authorities.add(new
                    SimpleGrantedAuthority(appUserRole.getName().getValue()));

        }

        return authorities;
    }

    private Collection<GrantedAuthority> getAuthorites( Set<AppUserRole> roles){
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
            AppUserRole appUserRole = (AppUserRole) iterator.next();
            authorities.add(new
                    SimpleGrantedAuthority(appUserRole.getName().getValue()));

        }

        return authorities;
    }
}
