package com.enov8.seekfund.seekfund.task;

import com.enov8.seekfund.seekfund.service.CampaignService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class ScheduleTask {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleTask.class);
    private final String dateTimeFormat  = "EEE, dd MMM yyyy hh:mma";
    SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
    @Autowired
    Scheduler scheduler;
    @Autowired
    CampaignService campaignService;
    @Value("${seekfund.task.welcomemail}")
    private Boolean TASK_WELCOME;
    @Value("${seekfund.task.trendingcampaign}")
    private Boolean TASK_TRENDING_CAMPAIGN;

    @Value("${seekfund.task.verifypayments}")
    private Boolean TASK_VERIFY_PAYMENTS;
    @Value("${seekfund.task.campaignend}")
    private Boolean TASK_VERIFY_CAMPAIGN_END;

    @Scheduled(cron = "${seekfund.cron.trendingcampaign}")
    public void runTrendingCampaign() {

        if(TASK_TRENDING_CAMPAIGN){
            logger.info("============== running trending campaign===================");
            campaignService.init();
            logger.info("============== done trending campaign===================");

        }

    }


    @Scheduled(cron = "${seekfund.cron.verifypayments}")
    public void verifyPayments() {

        if(TASK_VERIFY_PAYMENTS){
            logger.info("============== running verify payments task ===================");
            scheduler.verifyPayments();
            logger.info("============== finished  verify payments task===================");

        }

    }

    @Scheduled(cron ="${seekfund.cron.welcomemail}" )
    public void sendConfirmationEmail() {

        logger.info(" TASK_WELCOME " + TASK_WELCOME);
        if(TASK_WELCOME){
            logger.info("============== running sendConfirmationEmail===================");
            try {
                scheduler.sendEmailActivation();
            }catch (Exception e){System.out.println(e.getMessage());}

            logger.info("============== done sendConfirmationEmail===================");

        }

    }
    @Scheduled(cron ="${seekfund.cron.campaignEnd}" )
    public void checkCampaignForEndDate() {

        logger.info(" TASK_END_CAMPAIGN  " +TASK_VERIFY_CAMPAIGN_END);
        if(TASK_VERIFY_CAMPAIGN_END){
            logger.info("============== running END CAMPAIGN ===================");
            try {
                scheduler.checkCampaignForEndDate();
            }catch (Exception e){System.out.println(e.getMessage());}

            logger.info("============== done RUNNING END CAMPAIGN ===================");

        }

    }

}
