package com.enov8.seekfund.seekfund.task;

public interface Scheduler {


    void sendEmailActivation();
    void verifyPayments();

    void checkCampaignForEndDate();
}
