package com.enov8.seekfund.seekfund.task;


import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.EmailQueueType;
import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.model.AppSetting;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.CampaignDonation;
import com.enov8.seekfund.seekfund.model.EmailQueue;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.service.EmailService;
import com.enov8.seekfund.seekfund.util.Constant;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Transactional
public class SchedulerImpl implements Scheduler {

    private static final Logger logger  = LoggerFactory.getLogger(SchedulerImpl.class);
    private final String dateTimeFormat = "EEE, dd MMM yyyy hh:mma";
    SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
    @Autowired
    DatabaseService databaseService;
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    EmailService emailService;
    @Autowired
    CampaignService campaignService;

    @Override
    public void verifyPayments() {


        try{

            AppSetting maxWaitForPendingPayment = databaseService
                    .getAppSettingByName(Constant.DEFAULT_MAX_PENDING_PAYMENT_DURATION,
                            Constant.DEFAULT_MAX_PENDING_PAYMENT_DURATION_VAL.toString());

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR_OF_DAY, -1 * Integer.parseInt(maxWaitForPendingPayment.getValue()));


            Date maxWaitForPendingDate  = calendar.getTime();


            Long tt  =   maxWaitForPendingDate.getTime();
            logger.info("Retrieving payments pending less than "+ sdf.format(maxWaitForPendingDate));

            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class);

            criteria
                    .add(Restrictions.eq("paymentStatus",PaymentStatus.PENDING));

            criteria
                    .add(Restrictions.lt("dateCreated",new Timestamp(tt)));

            criteria.setMaxResults(100);

            List<CampaignDonation> campaignDonations = criteria.list();

            logger.info(" Size " + campaignDonations.size());

            campaignDonations.forEach(campaignService :: verifyPayment);


        }
        catch (Exception e){
          e.printStackTrace();
        }






    }

    @Override
    public void checkCampaignForEndDate() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date tomorrow = calendar.getTime();

        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.RUNNING, CampaignStatus.PENDING));

        criteria
                .add(Restrictions.le("endDate",new Timestamp(tomorrow.getTime())));

        criteria.setMaxResults(100);
        List<AppUserCampaign> campaigns = criteria.list();


        campaigns.forEach(campaignService::endCampaignOnDueDate);


    }

    @Override
    public void sendEmailActivation() {

        //get all register email 100
        //send mails


        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        Date tomorrow = calendar.getTime();

        Long tt  =   tomorrow.getTime();
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EmailQueue.class);

        criteria
                .add(Restrictions.eqOrIsNull("sent",false));
        criteria
                .add(Restrictions.eq("type",EmailQueueType.USER_REGISTER));

       criteria
                .add(Restrictions.lt("dateToSend",new Timestamp(tt)));

        criteria
                .add(Restrictions.isNotNull("toEmail"));


        criteria
                .add(Restrictions.isNotNull("fromEmail"));


        criteria.setMaxResults(100);
        List<EmailQueue> emailQueues = criteria.list();


        Assert.notNull(emailQueues,"Email Queue is null");

        Assert.notEmpty(emailQueues,"Email Queue is empty");

        emailQueues.forEach(emailQueue -> {
            try {
                emailService.sendUserEmailConfirmation(emailQueue);
                emailQueue.setDateSent(new Timestamp(System.currentTimeMillis()));
                emailQueue.setSent(true);
                databaseService.updateRecord(emailQueue);

            }catch (Exception e){e.printStackTrace();}

        });





    }
}
