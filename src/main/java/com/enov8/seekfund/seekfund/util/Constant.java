package com.enov8.seekfund.seekfund.util;

public class Constant {

	public final static String usernamePattern = "^[a-zA-Z0-9_]{3,15}$";
	public static final String USERNAME_EMAIL = "USERNAME";
	public static final String USERNAME_EMAIL_LOWER = "usernameOrEmail";
	public static final String PASSOWRD_LOWER = "password";
	public static final String X_FORWARDED_FOR = "X-FORWARDED-FOR";
	public static final String SOCIAL_REGISTER_ROUTE = "/register-social";
	public static final String PASSWORD = "PASSWORD";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_JSON_CHAR_SET  = "application/json;charset=UTF-8";
	public static final String CONTENT_FORM_URLENCODED = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String XML_HTTP_REQUEST = "XMLHttpRequest";
	public static final String X_REQUESTED_WITH = "X-Requested-With";
	public static final String SERVER_DATETIME_FORMAT = "yyyy-MM-dd";
	public static final String ADMIN_DATETIME_FORMAT = "yyyy-MM-dd hh:mmaaa";
	public static final String STRING_SUCCESS = "success";
	
	public static final String PASSWORD_REGREX = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z])";
	public static final String REQUEST_METHOD_POST = "POST";

	public static final String DATATABLE_REQUEST_PARAMETER_PAGE = "start";
	public static final int DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT = 1;
	public static final String DATATABLE_REQUEST_PARAMETER_LIMIT = "length";
	public static final int DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT = 50;
	public static final String UPDATE_ERROR = "Unexpected error updating record";
	
	public static final String DEFAULT_AMOUT_REGEX = "^\\d{0,9}(\\.\\d{1,9})?$";
	public static final String DEFAULT_PASSWORD = "password1";
	
	public static final String RETRIEVING_ERROR = "Unexpected Error Retrieving list";
	public static final String SAVE_ERROR = "Unexpected Error Saving record";
	
	public static final String DEFAULT_MAX_FAILED_LOGIN_NAME = "MAX_FAILED_LOGIN";
	public static final Integer DEFAULT_MAX_FAILED_LOGIN_VAL = 12;
	public static final String DEFAULT_MAX_PENDING_PAYMENT_DURATION = "DEFAULT_MAX_PENDING_PAYMENT_DURATION";
	public static final Integer DEFAULT_MAX_PENDING_PAYMENT_DURATION_VAL = 2;

    public static final String DEFAULT_MAX_CAMPAIGN_DURATION = "DEFAULT_MAX_CAMPAIGN_DURATION";
    public static final Integer DEFAULT_MAX_CAMPAIGN_DURATION_VAL = 30;

	public static final String DEFAULT_DOOMAIN_SUFFIX = "seekfunds.ng";
	public static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
	public static final String URL_SETUP_FIRST_PAY = "/setup";
	public static final String URL_SETUP_FIRST_PAY_TO = "/setup/pay-to";
	public static final String URL_LOGIN = "/login";
	public static final String URL_HOME = "/home";
	public static final String URL_ADMIN = "/admin";
	public static final String URL_SUPER = "/super";
	public static final String SESSION_TIMEOUT = "SESSION_TIMEOUT_IN_MINS";
	public static final Integer SESSION_TIMEOUT_VALUE = 24 * 60;
	public static final String LOCALE_NG = "NG";
	public static final String REGEX_ACCOUNT_NUMBER = "^[0-9]{10}$";
	//public static final String REGEX_ACCOUNT_NAME = "^([a-zA-Z]{3,20})$";
	public static final String REGEX_ACCOUNT_NAME = "^([a-zA-Z\\s]{3,20})";
	public static final String DEFAULT_EXPECTED_TIME_TO_PAY_IN_HRS = "EXP_PAY_TIME_IN_HRS";
	public static final String DEFAULT_EXPECTED_TIME_TO_PAY_IN_HRS_VAL = "24";
	public static final Long MINUTE_IN_MILLISECONDS = 60L * 1000;
	public static final Long HOUR_IN_MILLISECONDS = 60L * 60 *1000;
	public static final Object TPL_PAYMENTS = "payments";
	public static final Object TPL_SETTINGS = "settings";
	public static final String NON_PAYMENT_TIME_ALLOWANCE_IN_HRS = "PAY_ALLOW_IN_HRS";
	public static final int NON_PAYMENT_TIME_ALLOWANCE_IN_HRS_VALUE = 3;
	public static final String JOB_NON_PAYMENT_QUEUE = "JOB_NON_PAYMENT_QUEUE";
	public static final int JOB_NON_PAYMENT_QUEUE_VALUE = 100;
	public static final String JOB_QUEUE_SIZE = "JOB_QUEUE_SIZE";
	public static final int JOB_QUEUE_SIZE_VAL = 100;
	public static final String TPL_SUPPORT = "support";
	public static final String SUPER_USERNAME = "DEFAULT_SUPER_USERNAME";
	public static final String SUPER_USERNAME_VAL = "super";
	public static final String SUPER_EMAIL = "DEFAULT_SUPER_EMAIL";
	public static final String SUPER_PASSWORD = "DEFAULT_SUPER_PASSWORD";
	public static final String SUPER_PHONE = "DEFAULT_SUPER_PHONE";
	public static final String MAX_SUPERS = "DEFAULT_SUPER_MAX";
	public static final String CONTEXT_KEY_PAYMENT_PACKAGE = "paymentPackages";
	public static final String CONTEXT_KEY_BANK = "banks";
	public static final String PASSWORD_RESET_TOKEN_EXPIRE_IN_HRS = "PASS_RES_TOKEN_EXPIRE_IN_HRS";
	public static final String PASSWORD_RESET_TOKEN_EXPIRE_VAL = "24";
	public static final String CONTEXT_KEY_PAYMENT_METHOD = "paymentMethods";
	public static final String NEXT_PAYMENT_TIME_IN_HRS = "NEXT_PAYMENT_TIME";
	public static final int NEXT_PAYMENT_TIME_IN_HRS_VAL = 36;
	public static final String LAUNCH_DATE = "2017/04/08 10:59:59";
    public static final String MOZILLA_AGENT = "Mozilla/5.0";
	public static final String PAYSTACK_SUCCESS_PAYMENT = "Successful" ;
    public static final String PAYSTACK_ABANDONED_PAYMENT = "Abandoned" ;
	public static String UPLOAD_FILE_PREFIX = "bulk_payment";
	public static final String REGREX_ACC_NUMBER = "^\\d{10}$";
	public static final String REGREX_BANK_CODE = "^\\d{3}$";

}
