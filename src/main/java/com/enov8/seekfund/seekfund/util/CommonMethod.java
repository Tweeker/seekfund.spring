package com.enov8.seekfund.seekfund.util;

import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.HibernateException;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
public class CommonMethod {
	private Pattern usernamePattern = Pattern.compile(Constant.usernamePattern);
	@Autowired
	DatabaseService dbService;

	@Value("${uploaddir}")
	private String UPLOAD_TEMP_DIR;
	public Collection<GrantedAuthority> getAuthorites( Set<AppUserRole> roles){
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			AppUserRole appUserRole = (AppUserRole) iterator.next();
			authorities.add(new
					SimpleGrantedAuthority(appUserRole.getName().getValue()));

		}

		return authorities;
	}
	public Collection<GrantedAuthority> getAuthorites(HashSet<AppUserRole> roles){
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			AppUserRole appUserRole = (AppUserRole) iterator.next();
			authorities.add(new
					SimpleGrantedAuthority(appUserRole.getName().getValue()));

		}

		return authorities;
	}



	public boolean userIsLoggedOn( Authentication authentication){

		boolean isLoggedIn = false;
		if(authentication != null
				&& authentication.getPrincipal() != null
				&& authentication.getPrincipal() instanceof AppUser
				&& ((AppUser) authentication.getPrincipal()).getId() != null){

			isLoggedIn= true;
		}


		return  isLoggedIn;
	}

	public boolean isValidPassword(String password, String hashedPassword) {

		return BCrypt.checkpw(password, hashedPassword);
	}
	public String hashPassword(String password) {

		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
	public boolean isValidUsername(String username) {

		Matcher matcher = usernamePattern.matcher(username);

		return matcher.matches();

	}
	public boolean checkPasswordStrength(String password){
		

	    return password.matches("^[A-Za-z0-9\\d=!\\-@._*]*$");
	    		

      }

	public boolean isValidEmail(String email) {

		return EmailValidator.getInstance().isValid(email);

	}


	public static String getBaseURL(HttpServletRequest request) {
		String scheme = request.getScheme() + "://";
		String serverName = request.getServerName();
		String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		String contextPath = request.getContextPath();
		return scheme + serverName + serverPort + contextPath;

	}

	public static boolean isValidString(String str) {
		boolean isValid = false;
		if (str != null && str.trim().length() > 0) {
			isValid = true;
		}

		return isValid;
	}
	public static boolean isValidStringStat(String str) {
		boolean isValid = false;
		if (str != null && str.length() > 0) {
			isValid = true;
		}

		return isValid;
	}
	public static String parseRequestBodyToString(HttpServletRequest request) {
		String line = null;
		StringBuffer jb = new StringBuffer();
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			/* report an error */ }
		System.out.println(jb.toString());
		return jb.toString();
	}

	public static String parseRequestBodyToString(ServletRequest request) {
		String line = null;
		StringBuffer jb = new StringBuffer();
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			/* report an error */ }

		return jb.toString();
	}

	public static Date getTodaysDateInSQLDateTime() {
		return new Date(Calendar.getInstance().getTimeInMillis());
	}


	public static boolean isValidDateString(String dateString) {

		if (!isValidStringStat(dateString)) {

			return false;
		}
		String format1 = Constant.SERVER_DATETIME_FORMAT;
		String format2 = "yyyy-mm-dd";
		String format = null;
		format = dateString.contains("/") ? format1 : format2;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			sdf.parse(dateString.trim());
		} catch (ParseException pe) {
			return false;
		}

		return true;
	}

	public static boolean isValidServerDefaultDateFormat(String dateString) {
		String format = Constant.SERVER_DATETIME_FORMAT;

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			sdf.parse(dateString.trim());
		} catch (ParseException pe) {
			return false;
		}

		return true;
	}

	public static String getUniqueIbankId(String loginId) {
		return loginId + (new SimpleDateFormat("yyyyMMddhhmmSSSS")).format(new java.util.Date())
				+ removeNdot("" + Math.random() * 10000000D);
	}

	private static String removeNdot(String tValue) {
		String decimalPart = "";
		String strValue = tValue;
		if (strValue.lastIndexOf(".") != -1) {
			decimalPart = strValue.substring(strValue.lastIndexOf(".") + 1);
			if (decimalPart.length() == 1)
				decimalPart = decimalPart + "0";
			strValue = strValue.substring(0, strValue.lastIndexOf(".")) + decimalPart;
		} else {
			strValue = strValue + "00";
		}
		return strValue;
	}

	public static String getRemoteIp(HttpServletRequest request) {

		String ipAddress = request.getHeader(Constant.X_FORWARDED_FOR);
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		return ipAddress;
	}
	
	public static String determinTargetUrl(AppUser appUser){

		
		
		return "";
		
		
	}


	/*public void upDateAppUserContext(AppUser appUser, Authentication auth){
		
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
	
		Authentication newAuth = new UsernamePasswordAuthenticationToken(appUser, appUser.getHashedPassword(), authorities);
		
		SecurityContextHolder.getContext().setAuthentication(newAuth);
		
	}*/
	
	public boolean isCountDownOver(){
		boolean ready = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		try {
			java.util.Date launchDate = sdf.parse(Constant.LAUNCH_DATE);
			if(System.currentTimeMillis() > launchDate.getTime()){
				ready = true;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ready;
	}

	public boolean isValidAccountNumber(String accountNumber){
		if(accountNumber == null){return false;}

		if(accountNumber.trim().matches(Constant.REGEX_ACCOUNT_NUMBER)){

			return true;
		}

		return false;


	}

	public File convert(MultipartFile file) throws IOException {

		System.out.println("Conv file location "  +file.getOriginalFilename());
		//convFile.createNewFile();
		Path path = Paths.get(UPLOAD_TEMP_DIR);
		Path abPath  = path.resolve(file.getOriginalFilename()).toAbsolutePath();
		System.out.println(" abPath " + abPath);
		Files.copy(file.getInputStream(), abPath);
		//FileOutputStream fos = new FileOutputStream(convFile);
		//fos.write(file.getBytes());
		//fos.close();

		return abPath.toFile();
	}

	public String getFileNameForUpload(AppUserCampaign appUserCampaign,
									   AppUser appUser,String fileName){
		final String uuid = UUID.randomUUID().toString().replace("-", "");

		return "campaign-"+ appUserCampaign.getId()+"/" + uuid+"." + getExtensionByApacheCommonLib(fileName);
	}

	public String getExtensionByApacheCommonLib(String filename) {
		return FilenameUtils.getExtension(filename);
	}


}
