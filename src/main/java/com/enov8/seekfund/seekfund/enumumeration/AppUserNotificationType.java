package com.enov8.seekfund.seekfund.enumumeration;

public enum AppUserNotificationType {

    USER_CREATION("user_creation"), CAMPAIGN_CREATION("campaign_creation"),  CAMPAIGN_ENDED("campaign_ended");

    private final String title;
    AppUserNotificationType(String title) {
        this.title = title;
    }

    public String getValue() {
        return this.title;
    }

    public static AppUserNotificationType getEnumFromString(String text) {
        for (AppUserNotificationType t : AppUserNotificationType.values()) {
            if (t.title.equalsIgnoreCase(text)) {
                return t;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }
}
