package com.enov8.seekfund.seekfund.enumumeration;

public enum CampaignStatus {
    PENDING("pending"), RUNNING("running"), COMPLETED("completed"), CANCELED("canceled");

    private final String title;

    CampaignStatus(String title) {
        this.title = title;
    }

    public String getValue() {
        return this.title;
    }

    public static CampaignStatus getEnumFromString(String text) {
        for (CampaignStatus t : CampaignStatus.values()) {
            if (t.title.equalsIgnoreCase(text)) {
                return t;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }
}
