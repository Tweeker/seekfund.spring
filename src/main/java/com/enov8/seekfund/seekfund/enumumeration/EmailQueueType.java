package com.enov8.seekfund.seekfund.enumumeration;

public enum EmailQueueType {

    USER_REGISTER("user_register");

    private final String title;

    EmailQueueType(String title) {
        this.title = title;
    }

    public String getValue() {
        return this.title;
    }

    public static EmailQueueType getEnumFromString(String text) {
        for (EmailQueueType t : EmailQueueType.values()) {
            if (t.title.equalsIgnoreCase(text)) {
                return t;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }
}
