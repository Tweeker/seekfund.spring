package com.enov8.seekfund.seekfund.enumumeration;

public enum GenericStatusConstant {
	ACTIVE("active"),INACTIVE("inactive"),DELETED("deleted"),SUSPENDED("suspended"), PENDING("pending");
	
	private final String title;
	GenericStatusConstant(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static GenericStatusConstant getEnumFromString(String text) {
		for (GenericStatusConstant t : GenericStatusConstant.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}



	public static  String getallEnums(){
		StringBuilder enums  = new StringBuilder(GenericStatusConstant.class.getSimpleName() +" : ");
		int count  = 0;
		int totalEnums  = GenericStatusConstant.values().length;
		for (GenericStatusConstant t : GenericStatusConstant.values()) {


			if(count < totalEnums) {
				enums.append(t+ " | ");
			}else {
				enums.append(t);
			}



		}

		return enums.toString();

	}
}
