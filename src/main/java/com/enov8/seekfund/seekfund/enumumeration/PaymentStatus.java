package com.enov8.seekfund.seekfund.enumumeration;

public enum PaymentStatus {
	START("start"), PENDING("pending"), COMPLETE("complete"),
	ABANDONED("abandoned"),UNCONFIRMED("unconfirmed"),
	FAIL("fail"), RETRY("retry");

	private final String title;

	PaymentStatus(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static PaymentStatus getEnumFromString(String text) {
		for (PaymentStatus t : PaymentStatus.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
