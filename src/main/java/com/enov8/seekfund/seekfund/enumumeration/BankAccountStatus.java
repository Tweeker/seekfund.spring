package com.enov8.seekfund.seekfund.enumumeration;

public enum BankAccountStatus {

	SUSPENDED("suspended"), ACTIVE("active"), BLOCKED("block"),VALIDATING("validating"),UPDATED("updating");

	private final String title;

	BankAccountStatus(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static BankAccountStatus getEnumFromString(String text) {
		for (BankAccountStatus t : BankAccountStatus.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
