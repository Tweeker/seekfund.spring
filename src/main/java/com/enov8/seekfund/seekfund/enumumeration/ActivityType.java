package com.enov8.seekfund.seekfund.enumumeration;

public enum ActivityType {
	UPDATE("update"),DELETE("delete"),INSERT("insert"), LOGIN("login"), LOGOUT("logout");
	
	private final String title;
	ActivityType(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static ActivityType getEnumFromString(String text) {
		for (ActivityType t : ActivityType.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
