package com.enov8.seekfund.seekfund.enumumeration;

public enum NotificationType {
	UPDATE("update"),DELETE("delete"),INSERT("insert"), LOGIN("login"), LOGOUT("logout");

	private final String title;
	NotificationType(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static NotificationType getEnumFromString(String text) {
		for (NotificationType t : NotificationType.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
