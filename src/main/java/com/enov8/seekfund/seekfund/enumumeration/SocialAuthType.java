package com.enov8.seekfund.seekfund.enumumeration;

public enum SocialAuthType {
	FACEBOOK("facebook"),TWITTER("twitter"),GOOGLE("google");

	private final String title;
	SocialAuthType(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static SocialAuthType getEnumFromString(String text) {
		for (SocialAuthType t : SocialAuthType.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
