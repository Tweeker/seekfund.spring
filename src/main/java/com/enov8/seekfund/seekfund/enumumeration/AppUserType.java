package com.enov8.seekfund.seekfund.enumumeration;

public enum AppUserType  {

	DIRECTOR("director"), USER("user"), SUPER("super");

	private final String title;

	AppUserType(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static AppUserType getEnumFromString(String text) {
		for (AppUserType t : AppUserType.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}

}
