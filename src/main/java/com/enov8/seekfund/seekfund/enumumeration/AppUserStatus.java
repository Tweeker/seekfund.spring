package com.enov8.seekfund.seekfund.enumumeration;



public enum AppUserStatus {

	ACTIVE("active"),INACTIVE("inactive"),DELETED("deleted"),SUSPENDED("suspended"),
	AWAITING_EMAIL_CONF("awating_email_conf"), LOCKED("locked");
	
	private final String title;
	AppUserStatus(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static AppUserStatus getEnumFromString(String text) {
		for (AppUserStatus t : AppUserStatus.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
