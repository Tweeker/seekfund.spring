package com.enov8.seekfund.seekfund.enumumeration;

public enum AppUserRoleConstant {
	USER("user"),ADMIN("admin"),SUPER("super"),SOCIAL_AUTH("social_auth");
	
	private final String title;
	AppUserRoleConstant(String title) {
		this.title = title;
	}

	public String getValue() {
		return this.title;
	}

	public static AppUserRoleConstant getEnumFromString(String text) {
		for (AppUserRoleConstant t : AppUserRoleConstant.values()) {
			if (t.title.equalsIgnoreCase(text)) {
				return t;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
