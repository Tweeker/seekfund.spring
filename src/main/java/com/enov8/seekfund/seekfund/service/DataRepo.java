package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.model.Bank;
import com.enov8.seekfund.seekfund.model.CampaignCategory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataRepo {


    public List<Bank> getBanks() {
        List<Bank> banks = new ArrayList<Bank>();
        Bank bank = new Bank();
        bank.setCode("057");
        bank.setName("Zenith Bank PLC");
        bank.setShortName("Zenith Bank PLC");
        banks.add(bank);
        bank = new Bank();
        bank.setCode("058");
        bank.setName("Guaranty Trust Bank");
        bank.setShortName("GTB");
        banks.add(bank);
        bank = new Bank();
        bank.setCode("011");
        bank.setName("First Bank Nig Plc");
        bank.setShortName("FBN");

        banks.add(bank);

        return banks;

    }

    public List<CampaignCategory> getDefaultCampaignCategory(){

        List<CampaignCategory> categories = new ArrayList<>();


        CampaignCategory politics  = new CampaignCategory();

        politics.setName("Politics");
        categories.add(politics);




        CampaignCategory entertainment  = new CampaignCategory();

        entertainment.setName("Entertainment");
        categories.add(entertainment);

        CampaignCategory social  = new CampaignCategory();

        social.setName("Social");

        categories.add(social);

        CampaignCategory health  = new CampaignCategory();

        health.setName("Health");

        categories.add(health);


        return categories;

    }





}
