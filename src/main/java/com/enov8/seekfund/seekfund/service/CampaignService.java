package com.enov8.seekfund.seekfund.service;

import com.amazonaws.services.s3.AmazonS3;
import com.enov8.seekfund.seekfund.enumumeration.*;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.*;
//import com.enov8.seekfund.seekfund.pojo.PopularCampaignRqPojo;
import com.enov8.seekfund.seekfund.pojo.CampaignCommentPojo;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignImagePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.omg.CORBA.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CampaignService {

    private List<CurrentCampaignPojo> trendingCampaign  = new ArrayList<>();



    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CriteriaProjectService criteriaProjectService;
    @Autowired
    DatabaseService databaseService;

    @Value("${amazon.s3.default-bucket}")
    private String DEFAULT_BUCKET;

    @Autowired
    CommonMethod commonMethod;

    @Autowired
    AmazonS3 amazonS3;

    @Value("${seekfund.campaign.image.max}")
    private Integer DEFAULT_MAX_CAMPAIGN_IMAGES;

    @Autowired
    CampaignDonationService campaignDonationService;

    @Value("${seekfund.campaign.image.prefix}")
    private String CAMPAIGN_IMAGE_PREFIX;

    @Autowired
    PaystackService paystackService;

    public CampaignDonation verifyPayment(CampaignDonation campaignDonation){
        CampaignDonation campaignDonation1 = null;
        try{
            campaignDonation1 = verifyPayment(campaignDonation.getPaymentRef(),campaignDonation.getCampaign().getId());}
        catch (Exception e){

        }
        return campaignDonation1;

    }

    public CampaignDonation verifyPayment(String paymentRef, Long campaignId) throws IllegalArgumentException,
            SeekFundException,
            Exception{
        Assert.notNull(paymentRef,"Payment Ref cannot be null");

        Assert.isTrue(paymentRef.length()>0  , "Payment Ref cannot be empty");

        Assert.notNull(campaignId,"campaignId cannot be null");

        CampaignDonation campaignDonation = campaignDonationService.getCampaignDonation(paymentRef,campaignId);

        Assert.notNull(campaignId,"campaignDonation not found");

        if(campaignDonation.getPaymentStatus().getValue().equalsIgnoreCase(PaymentStatus.COMPLETE.getValue())){

            throw new SeekFundException("Donation is Currently " + campaignDonation.getPaymentStatus());
        }

        VerifyTransactionResp resp = paystackService.verifyPayment(campaignDonation.getPaymentRef());

        if(resp.getStatus()){

            if(resp.getData().getGatewayResponse().equalsIgnoreCase(Constant.PAYSTACK_SUCCESS_PAYMENT)){
                campaignDonation.setPaymentStatus(PaymentStatus.COMPLETE);
                campaignDonation.setDateCompleted(new Timestamp(System.currentTimeMillis()));
                databaseService.updateRecord(campaignDonation);

                AppUserCampaign appUserCampaign =
                        (AppUserCampaign) databaseService.getRecordById
                                (AppUserCampaign.class,campaignDonation.getCampaign().getId());
                appUserCampaign.setAmountRaised(appUserCampaign.getAmountRaised().add(campaignDonation.getAmount()));
                databaseService.updateRecord(appUserCampaign);

            }else if(resp.getData().getStatus().equalsIgnoreCase(Constant.PAYSTACK_ABANDONED_PAYMENT)){
                campaignDonation.setPaymentStatus(PaymentStatus.ABANDONED);
                campaignDonation.setDateCompleted(new Timestamp(System.currentTimeMillis()));
                databaseService.updateRecord(campaignDonation);

            }else{
                campaignDonation.setPaymentStatus(PaymentStatus.FAIL);
                campaignDonation.setDateCompleted(new Timestamp(System.currentTimeMillis()));

                databaseService.updateRecord(campaignDonation);

            }

            ObjectMapper objectMapper = new ObjectMapper();

            campaignDonation.setReturnText(objectMapper.writeValueAsString(resp));

            databaseService.updateRecord(campaignDonation);



        }else{
            campaignDonation.setPaymentStatus(PaymentStatus.UNCONFIRMED);
            campaignDonation.setDateCompleted(new Timestamp(System.currentTimeMillis()));
            ObjectMapper objectMapper = new ObjectMapper();

            campaignDonation.setReturnText(objectMapper.writeValueAsString(resp));

            databaseService.updateRecord(campaignDonation);

            throw new SeekFundException("We cannot confirm payment at the moment!.");
        }



        return campaignDonation;

    }

    public void getCampaignById3(){

        try{
            String hql  = " select p from AppUserCampaign p " +
                    " left join p.images as img where p.id = 47 and" +
                    " img.status  = '"+ GenericStatusConstant.PENDING +"'";

            AppUserCampaign appUserCampaign = (AppUserCampaign) databaseService.getUniqueRecordByHql(hql);


            System.out.println("=============");



        }
        catch (Exception e){}


    }


    public void getCampaignById2(){
        Criteria campaign = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class,"c");
       // Criteria images = campaign.createCriteria("c.images",JoinType.LEFT_OUTER_JOIN);
        //images.add(Restrictions.eq("status",GenericStatusConstant.PENDING));
       // images.setFetchMode("c.images", FetchMode.JOIN);
        //47L,152L
        campaign.add(Restrictions.eq("c.id",47L));
        //campaign.createAlias("c.images","images");
        Criteria imageCriteria = campaign.createCriteria("c.images","images",JoinType.LEFT_OUTER_JOIN);

        //campaign.setFetchMode("images", FetchMode.JOIN);

        imageCriteria.add((Restrictions.eq("images.status", GenericStatusConstant.PENDING)));
        //images.add(Restrictions.eq("dealerId",new Long(dealerId)));
        campaign.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); // depends on wat u want
        //152
        AppUserCampaign appUserCampaign =(AppUserCampaign) campaign.uniqueResult();
        System.out.println("=============");



    }

    public CurrentCampaignPojo getCampaignById(Long id){

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);

        criteria.createAlias("appUser","a");
     /*   criteria
                .add(Restrictions.in("c.status",
                        CampaignStatus.COMPLETED,
                        CampaignStatus.RUNNING
                ))*/
        criteria .add(Restrictions.eq("id",id));

        //criteria.createAlias("c.images","images");
       // criteria.add((Restrictions.eq("images.status", GenericStatusConstant.ACTIVE)));

        criteria.setFetchMode("images", FetchMode.EAGER);

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")

                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("a.id"), "postedById")
                .add(Projections.property("a.username"), "postedByName")
                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("description"), "description")
                .add(Projections.property("dateCreated"), "dateCreated")
                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("amountRaised"), "amountRaised")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");


        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
        CurrentCampaignPojo currentCampaignPojo = (CurrentCampaignPojo) criteria.uniqueResult();




        return currentCampaignPojo;
    }

    public List<CurrentCampaignPojo> searchCampaignsByCategory(Long categoryId, Integer start , Integer Length) throws Exception {
        String hql  =
                "  select a.startDate as startDate, a.id as id," +
                        " a.appUser.id as postedById, " +
                        "" +
                        " a.appUser.username as postedByName, " +
                        " a.endDate as endDate, " +
                        " a.description as description, " +
                        " a.dateCreated as dateCreated, " +
                        " a.campaignCategory as  campaignCategory, " +
                        " a.amount as amount, " +
                        " a.amountRaised as amountRaised, " +
                        "  a.status as status, " +
                        "  a.name as name ,  " +
                        " (select m.fullUrl " +
                        " from CampaignImage m where " +
                        " m.appUserCampaign.id =  a.id and m.status = '" +GenericStatusConstant.ACTIVE+
                        "' and m.id = (select max(p.id) " +
                        " from CampaignImage " +
                        " p where p.appUserCampaign.id = a.id and p.status = '" +
                        GenericStatusConstant.ACTIVE+"' )) as campaignImage " +
                        " from AppUserCampaign a " +
                        " where a.status = :category " +
                        " and a.campaignCategory.id = " +categoryId;


                List<CurrentCampaignPojo> campaignPojos =
                        (List<CurrentCampaignPojo>)sessionFactory.getCurrentSession().createQuery(hql)
                                .setParameter("category",CampaignStatus.RUNNING)
                        .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class)).list();



                return campaignPojos;

    }
    public CurrentCampaignPojo getCampaignByIdWithDetached(Long id){

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("a.id"), "postedById")
                .add(Projections.property("a.username"), "postedByName")
                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("description"), "description")
                .add(Projections.property("dateCreated"), "dateCreated")
                .add(Projections.property("campaignImage"), "campaignImage")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("amountRaised"), "amountRaised")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");

        String hql  =
                "  select a.startDate as startDate, a.id as id," +
                        " a.appUser.id as postedById, " +
                        "" +
                        " a.appUser.username as postedByName, " +
                        " a.endDate as endDate, " +
                        " a.description as description, " +
                        " a.dateCreated as dateCreated, " +
                        " a.campaignCategory as  campaignCategory, " +
                        " a.amount as amount, " +
                        " a.amountRaised as amountRaised, " +
                        "  a.status as status, " +
                        "  a.name as name ,  " +
                        " (select m.fullUrl " +
                        " from CampaignImage m where " +
                        " m.appUserCampaign.id =  a.id and m.status = '" +GenericStatusConstant.ACTIVE+
                        "' and m.id = (select max(p.id) " +
                        " from CampaignImage " +
                        " p where p.appUserCampaign.id = a.id and p.status = '" +
                        GenericStatusConstant.ACTIVE+"' )) as campaignImage " +
                        " from AppUserCampaign a where a.id = " + id +"  and a.status = '" + CampaignStatus.RUNNING+"'";


        try{
            CurrentCampaignPojo c  = (CurrentCampaignPojo) databaseService.getUniqueRecordByHql(hql,CurrentCampaignPojo.class);



            return c;
        }
        catch (Exception e){
            e.printStackTrace();
        }



        CurrentCampaignPojo currentCampaignPojo = new CurrentCampaignPojo();


        return currentCampaignPojo;
    }
    public DataTableResponsePojo2<CampaignCommentPojo> getCampaignComment(GenericStatusConstant commentStatus,
                                                                          Long campaignId, Integer start, Integer limit){

        DataTableResponsePojo2<CampaignCommentPojo> dataComments = new DataTableResponsePojo2<>();
        Criteria criteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);

        criteria.createAlias("commentedBy","a");
        countCriteria.createAlias("commentedBy","a");

        criteria.createAlias("campaign","c");
        countCriteria.createAlias("campaign","c");

        criteria.add(Restrictions.eq("status", commentStatus));
        countCriteria.add(Restrictions.eq("status", commentStatus));

        criteria.add(Restrictions.eq("c.id", campaignId));
        countCriteria.add(Restrictions.eq("c.id", campaignId));

        criteria.add(Restrictions.eq("a.status", AppUserStatus.ACTIVE));
      countCriteria.add(Restrictions.eq("a.status", AppUserStatus.ACTIVE));

        ProjectionList projectionList = criteriaProjectService.getCampaignCommentProjection();

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CampaignCommentPojo.class));



        criteria.setFirstResult( (start-1) * limit);
        criteria.setMaxResults(limit);


        criteria.addOrder(Order.desc("dateCreated"));


       List<CampaignCommentPojo> comments = criteria.list();

    Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        dataComments.setRecordsTotal(totalResult);
        dataComments.setRecordsFiltered(totalResult);
        dataComments.setData(comments);


        return dataComments;

    }


    public DataTableResponsePojo2<CampaignCommentPojo> getCampaignComment(Integer start, Integer limit){

        DataTableResponsePojo2<CampaignCommentPojo> dataComments = new DataTableResponsePojo2<>();
        Criteria criteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);

        criteria.createAlias("commentedBy","a");
        countCriteria.createAlias("commentedBy","a");

        criteria.createAlias("campaign","c");
        countCriteria.createAlias("campaign","c");

        ProjectionList projectionList = criteriaProjectService.getCampaignCommentProjection();

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CampaignCommentPojo.class));



        criteria.setFirstResult( (start-1) * limit);
        criteria.setMaxResults(limit);


        criteria.addOrder(Order.desc("dateCreated"));


        List<CampaignCommentPojo> comments = criteria.list();

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        dataComments.setRecordsTotal(totalResult);
        dataComments.setRecordsFiltered(totalResult);
        dataComments.setData(comments);


        return dataComments;

    }


   /* public PopularCampaignRqPojo getPopularCampaign() throws Exception{

        String hql = "select p.appUserCampaign from PopularCampaign p where p.status  = '" +GenericStatusConstant.ACTIVE +"' order by p.appUserCampaign.startDate desc";

        PopularCampaignRqPojo popularCampaignRqPojo = new PopularCampaignRqPojo();




        return  popularCampaignRqPojo;



    }*/
   public CurrentCampaignPojo getMyCampaignById(AppUser appUser, Long campaignId) throws Exception{
       Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
       criteria
               .add(Restrictions.eq("id", campaignId))
               .add(Restrictions.eq("appUser.id",appUser.getId()));

       ProjectionList projectionList = Projections.projectionList()
               .add(Projections.property("id"), "id")
               .add(Projections.property("startDate"), "startDate")
               .add(Projections.property("endDate"), "endDate")
               .add(Projections.property("description"), "description")
               .add(Projections.property("campaignCategory"), "campaignCategory")
               .add(Projections.property("amount"), "amount")
               .add(Projections.property("amountRaised"), "amountRaised")
               .add(Projections.property("status"), "status")
               .add(Projections.property("name"), "name");

       criteria.setProjection(projectionList)
               .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
       CurrentCampaignPojo currentCampaignPojo = (CurrentCampaignPojo) criteria.uniqueResult();

       Assert.notNull(currentCampaignPojo,"Cannot find  campaign!.");



       return currentCampaignPojo;



   }
    public CampaignImage getMyCampaignImageById(AppUser appUser, Long imageId) throws Exception{
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignImage.class);
        criteria
                .add(Restrictions.eq("id", imageId))



                .add(Restrictions.eq("a.appUser.id",appUser.getId()));

        criteria.createAlias("appUserCampaign","a");
        CampaignImage campaignImage = (CampaignImage) criteria.uniqueResult();

        Assert.notNull(campaignImage,"Cannot find  campaign image!.");



        return campaignImage;



    }
    public AppUserCampaign getCurrentCampaign(AppUser appUser) throws Exception{

        String hql  = " select p from AppUserCampaign p where p.status in ('"+
                CampaignStatus.PENDING
                +"','"+
                CampaignStatus.RUNNING
                +"') and p.appUser.id =  " + appUser.getId();

        List<AppUserCampaign> appUserCampaigns = (List<AppUserCampaign>)
                databaseService.getAllRecordsByHql(hql);

        Assert.notNull(appUserCampaigns,"Cannot find campaign");

        Assert.notEmpty(appUserCampaigns,"No current campaign");

        return appUserCampaigns.get(0);
    }
    public CurrentCampaignPojo getCurrentCampaignPojo(AppUser appUser){

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.PENDING,
                        CampaignStatus.RUNNING
                ))
                .add(Restrictions.eq("appUser.id",appUser.getId()));

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("startDate"), "startDate")
                .add(Projections.property("endDate"), "endDate")
                .add(Projections.property("description"), "description")
                .add(Projections.property("campaignCategory"), "campaignCategory")
                .add(Projections.property("amount"), "amount")
                .add(Projections.property("status"), "status")
                .add(Projections.property("name"), "name");

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
        List<CurrentCampaignPojo> currentCampaignPojos = criteria.list();

        Assert.notNull(currentCampaignPojos,"Cannot find current campaign!.");

        Assert.notEmpty(currentCampaignPojos,"Campaign is empty!.");


        return currentCampaignPojos.get(0);

    }

    private  String uploadImage(File file, AppUserCampaign appUserCampaign,AppUser appUser ) throws SeekFundException{


        Assert.notNull(file,"Invalid file");
        String fileName = null;
        try{
            fileName  = commonMethod.getFileNameForUpload(appUserCampaign,
                    appUser,file.getName());

            amazonS3.putObject(DEFAULT_BUCKET, fileName,
                    file) ;

        }
        catch (Exception e){
            //todo set success false here
            fileName = null;
            e.printStackTrace();}
        finally { try{ if(file != null){file.delete();} }
        catch (Exception e){e.printStackTrace();}
        }

        if(file == null){ throw new SeekFundException("Cannot upload file at the moment!. ECS01");}

        return fileName;
    }

    public List<CampaignImagePojo>  getCampaignImages(AppUserCampaign  appUserCampaign) throws Exception{

        Assert.notNull(appUserCampaign, "AppUser cannt be null");


        String hql  = " select c.id as id, " +
                " c.appUserCampaign.id as campaiginId, " +
                "" +
                " c.baseUrl as baseUrl," +
                " c.fullUrl as fullUrl, c.status as status, " +
                "c.dateCreated as dateCreated from  " +
                " CampaignImage c " +
                " where c.appUserCampaign.id = " + appUserCampaign.getId()  + " " +
                "and  c.status  = '" + GenericStatusConstant.ACTIVE + "'";

        List<CampaignImagePojo> images  = (List<CampaignImagePojo> )
                databaseService.getAllRecordsByHql(hql,
                CampaignImagePojo.class);


        return images;

    }

    public void addCampaignImage(AppUser appUser, File file) throws SeekFundException, IllegalArgumentException, Exception{
        //check if he has a current campaign
        //get size of current campaign images
        AppUserCampaign appUserCampaign = getCurrentCampaign(appUser);
        String hql  = "select count(pi) from " +
                "AppUserCampaign p join p.images pi where pi.status  = '"
                + GenericStatusConstant.ACTIVE +"' and p.id  = " + appUserCampaign.getId();



        Long count  = (Long) databaseService.getUniqueRecordByHql(hql);
        Assert.isTrue(count < DEFAULT_MAX_CAMPAIGN_IMAGES , "Maximum campaign images " + DEFAULT_MAX_CAMPAIGN_IMAGES + " reached!");


        String s3KeyFile  =  uploadImage(file,appUserCampaign,appUser);
        CampaignImage campaignImage = new CampaignImage();
        campaignImage.setAppUserCampaign(appUserCampaign);
        String fullUrl = CAMPAIGN_IMAGE_PREFIX+s3KeyFile;
        campaignImage.setBaseUrl(s3KeyFile);
        campaignImage.setFullUrl(fullUrl);

        databaseService.saveRecord(campaignImage);

        if(appUserCampaign.getCampaignImage() == null){
            appUserCampaign.setCampaignImage(fullUrl);
        }

        appUserCampaign.getImages().add(campaignImage);


        databaseService.updateRecord(appUserCampaign);

        if(appUserCampaign.getCampaignImage() == null){

            appUserCampaign.setCampaignImage(CAMPAIGN_IMAGE_PREFIX+s3KeyFile);
            databaseService.updateRecord(appUserCampaign);
        }




        //upload image
        //campaignImage.set

    }

    public void incrementSearchCount(Long campaignId){

        try{
            AppUserCampaign appUserCampaign = (AppUserCampaign)databaseService.getRecordById(AppUserCampaign.class,campaignId);
            if(appUserCampaign != null){

                appUserCampaign.setSearchCount(appUserCampaign.getSearchCount()+1);

                databaseService.saveRecord(appUserCampaign);

            }

        } catch (Exception e){
            e.printStackTrace();
        }


    }
    //@PostConstruct
    public void init() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class,"c");

        ProjectionList projectionList = criteriaProjectService.getProjectForSearchCampaign();

        criteria.add(Restrictions.ge("c.searchCount",1L));

        criteria
                .add(Restrictions.in("status",
                        CampaignStatus.RUNNING
                ));


        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));

        criteria.addOrder(Order.desc("c.searchCount"));
        criteria.addOrder(Order.desc("c.dateCreated"));

        criteria.setFirstResult(0);
        criteria.setMaxResults(4);
        this.trendingCampaign = criteria.list();



    }

    public List<CurrentCampaignPojo> getTrendingCampaign() {
        return trendingCampaign;
    }

    public void setTrendingCampaign(List<CurrentCampaignPojo> trendingCampaign) {
        this.trendingCampaign = trendingCampaign;
    }


    public PopularCampaign addPopularCampaign(AppUserCampaign appUserCampaign, GenericStatusConstant status,Long order) throws SeekFundException , IllegalArgumentException, Exception {

        Assert.notNull(appUserCampaign,"AppUser campaign cannot be null");

        String hql  = " select count(p) from PopularCampaign p where p.appUserCampaign.id  = " + appUserCampaign.getId();

        Long count  = (Long)databaseService.getUniqueRecordByHql(hql);

        Assert.isTrue(count ==0 ,"Campaign Already exist in this category!.");


        PopularCampaign popularCampaign = new PopularCampaign();


        popularCampaign.setAppUserCampaign(appUserCampaign);

        hql = "select s from SocialAuthUser s where s.appUser.id = " + appUserCampaign.getAppUser().getId();



       List<SocialAuthUser> socialAuthUsers =
                ( List<SocialAuthUser> )databaseService.getAllRecordsByHql(hql,0,1);

            if(socialAuthUsers != null
                    && !socialAuthUsers.isEmpty()){

                if(socialAuthUsers.get(0).getProfileImage() != null){

                    popularCampaign.setCampaignImage(socialAuthUsers.get(0).getProfileImage());

                }
                //use the data image
            }

        databaseService.saveRecord(popularCampaign);


        databaseService.createAuditTrail(
                        "Created new Popular campaign admin - Popular campaign with id : " + popularCampaign.getId(),
                PopularCampaign.class.getSimpleName(),
                ActivityType.INSERT, 0L, popularCampaign.getId());



        return popularCampaign;

    }

    public void endCampaignOnDueDate(AppUserCampaign appUserCampaign) {

       try{
           Calendar todayCalendar  = Calendar.getInstance();
           Calendar endCalendar  = Calendar.getInstance();
           endCalendar.setTime(new Date(appUserCampaign.getEndDate().getTime()));
           if(todayCalendar.after(endCalendar)){

               appUserCampaign.setStatus(CampaignStatus.COMPLETED);

               databaseService.updateRecord(appUserCampaign);


               databaseService.createAuditTrail("Cron Task" + "" +
                               " " +
                               "Updated  Campaign status to : " + CampaignStatus.COMPLETED,
                       AppUserCampaign.class.getSimpleName(),
                       ActivityType.UPDATE, 0L, appUserCampaign.getId());


               databaseService.createNotification(appUserCampaign.getAppUser(),
                       "Your campaign  " + appUserCampaign.getName() + "" +
                               " has come to an end!.",
                       AppUserNotificationType.CAMPAIGN_ENDED);


           }
       }
       catch (Exception e){
           e.printStackTrace();
       }





    }
}
