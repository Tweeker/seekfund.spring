package com.enov8.seekfund.seekfund.service;

import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Service;

@Service
public class CriteriaProjectService {

    public ProjectionList getProjectForSearchCampaign(){

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("c.id"), "id")
                .add(Projections.property("c.searchCount"), "searchCount")
                .add(Projections.property("c.startDate"), "startDate")
                .add(Projections.property("c.endDate"), "endDate")
                .add(Projections.property("c.description"), "description")
                .add(Projections.property("c.campaignImage"), "campaignImage")
                .add(Projections.property("c.dateCreated"), "dateCreated")
                .add(Projections.property("c.amountRaised"), "amountRaised")
                .add(Projections.property("c.campaignCategory"), "campaignCategory")
                .add(Projections.property("c.amount"), "amount")
                .add(Projections.property("c.status"), "status")
                .add(Projections.property("c.name"), "name");


        return projectionList;
    }

    public ProjectionList getProjectForPaymentDetails(){

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("b.id"), "id")
                .add(Projections.property("b.bank"), "bank")
                .add(Projections.property("b.accountNumber"), "accountNumber")
                .add(Projections.property("b.dateCreated"), "dateCreated")
                .add(Projections.property("b.dateModified"), "dateModified")
                .add(Projections.property("b.modifiedBy"), "modifiedBy")
                .add(Projections.property("b.status"), "status")
                .add(Projections.property("appUser.username"), "username")
                .add(Projections.property("appUser.id"), "userId")
                .add(Projections.property("b.accountName"), "accountName");


        return projectionList;
    }
    public ProjectionList getProjectionForPopularCampaign(){

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("p.pOrder"), "order")
                .add(Projections.property("p.id"), "id")
                .add(Projections.property("c.id"), "campaign.id")
                .add(Projections.property("c.name"), "campaign.name")
                .add(Projections.property("c.amount"), "campaign.amount")
                .add(Projections.property("c.campaignImage"), "campaignImage")
                .add(Projections.property("c.amountRaised"), "campaign.amountRaised")
                .add(Projections.property("c.dateCreated"), "campaign.dateCreated")
                .add(Projections.property("c.status"), "campaign.status")
                .add(Projections.property("c.description"), "campaign.description")
                .add(Projections.property("c.name"), "campaign.name")
                .add(Projections.property("c.campaignCategory"), "campaign.campaignCategory")

                .add(Projections.property("p.campaignImage"), "campaignImage");


        return  projectionList;

    }

    public ProjectionList getCampaignCommentProjection() {
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("a.id"), "commentedById")
                .add(Projections.property("a.username"), "commentedByName")
                .add(Projections.property("c.id"), "campaignId")
                .add(Projections.property("comment"), "comment")
                .add(Projections.property("dateCreated"), "dateCreated")
                .add(Projections.property("status"), "status");

        return  projectionList;
    }
}
