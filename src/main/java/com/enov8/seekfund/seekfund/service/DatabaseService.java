package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.AppUserNotificationType;
import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.NotificationType;
import com.enov8.seekfund.seekfund.model.*;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import org.hibernate.*;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@Repository
@Transactional
public class DatabaseService {
	@Autowired
	private SessionFactory sessionFactory;
	

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#saveRecord(java.lang.Object)
	 */
	public void saveRecord(Object object) throws HibernateException, Exception {
		Session session = getSession();

		session.save(object);

	}
	



	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAllRecordsByHql(java.lang.String)
	 */
	public Object getAllRecordsByHql(String hql) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery(hql);

		return query.list();

	}

	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAllRecordsByHql(java.lang.String, int, int)
	 */
	public Object getAllRecordsByHql(String hql, int start, int limit) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(limit);

		return query.list();

	}
	public Object getAllRecordsByHql(String hql, Class clazz) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery(hql);

		query.setResultTransformer(Transformers.aliasToBean(clazz));

		return query.list();

	}
	public Object getAllRecordsByHql(String hql, Class<?> clazz, int skip, int limit) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery(hql);
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		query.setFirstResult(skip);
		query.setMaxResults(limit);
		return query.list();

	}
	
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getUniqueRecordByHql(java.lang.String)
	 */
	public Object getUniqueRecordByHql(String hql) throws HibernateException, Exception {
		Session session = getSession();
		
		Query query = session.createQuery(hql);

		return query.uniqueResult();

	}

	public Object getUniqueRecordByHql(String hql, Class<?> clazz) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery(hql);
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		return query.uniqueResult();

	}
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#deleteRecord(java.lang.Object)
	 */
	public void deleteRecord(Object object) throws HibernateException, Exception {
		getSession().delete(object);
		

	}

	

	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getRecordById(java.lang.Class, java.lang.Long)
	 */
	
	public Object getRecordById(Class<?> clazz, Long id) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery("select c from " + clazz.getSimpleName() + " c where c.id = " + id);

		return query.uniqueResult();

	}



	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAllRecords(java.lang.Class)
	 */

	public Object getAllRecords(Class<?> clazz) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery("select c from " + clazz.getSimpleName() + "  c");

		return query.list();

	}
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#updateRecord(java.lang.Object)
	 */

	public void updateRecord(Object obj) throws HibernateException, Exception {
	    getSession().update(obj);

		

	}


	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAllRecordsByCriteria(org.hibernate.Criteria)
	 */

	public List<?> getAllRecordsByCriteria(Criteria criteria) throws HibernateException, Exception {

		return criteria.list();

	}



	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAllRecords(java.lang.Class, int, int)
	 */

	public List<?> getAllRecords(Class<?> clazz, int start, int end) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery("select c from " + clazz.getSimpleName() + " c");
		query.setFirstResult(start);
		query.setMaxResults(end);

		return query.list();

	}


	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getRecordCount(java.lang.Class)
	 */

	public Long getRecordCount(Class<?> clazz) throws HibernateException, Exception {
		Session session = getSession();

		Query query = session.createQuery("select count(c) from " + clazz.getSimpleName() + " c");

		return Long.valueOf(query.uniqueResult()+"");

	}



	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getCriteria(java.lang.Class)
	 */

	public Criteria getCriteria(Class<?> clazz) throws HibernateException, Exception {
		Session session = getSession();

		Criteria criteria = session.createCriteria(clazz.getClass());

		return criteria;

	}
	
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#createAuditTrail(java.lang.String, java.lang.String, com.hyip.bomtcash.enumumeration.ActivityType, java.lang.Long, java.lang.Long)
	 */

	public void createAuditTrail(String actionPerformed, String entityAffected, ActivityType activityType, Long userId, Long recordId)throws HibernateException, Exception{
		
		AuditTrail auditTrail = new AuditTrail();
		auditTrail.setActionPerformed(actionPerformed);
		auditTrail.setEntityAffected(entityAffected);
		auditTrail.setActivityType(activityType);
		auditTrail.setRecordId(recordId);
		auditTrail.setUserId(userId);
		try{	getSession().save(auditTrail);}
		catch (Exception e){
			e.printStackTrace();
		}

	}

	public void createNotification(AppUser appUser, String description, AppUserNotificationType notificationType)throws HibernateException, Exception{

		AppUserNotification notification = new AppUserNotification();

		notification.setAppUser(appUser);
		notification.setDescription(description);

		notification.setNotificationType(notificationType);

		try{
			getSession().save(notification);}
		catch (Exception e){
			e.printStackTrace();
		}


	}
	
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAppSettingByName(java.lang.String)
	 */

	public AppSetting getAppSettingByName(String settingName){
		
		Session session = getSession();

		Criteria criteria = session.createCriteria(AppSetting.class);
		criteria.add(Restrictions.ilike("name", settingName));
		
		
		return (AppSetting) criteria.uniqueResult();
		
	}
	/* (non-Javadoc)
	 * @see com.hyip.bomtcash.service.DatabaseService#getAppSettingByName(java.lang.String, java.lang.String)
	 */

	public AppSetting getAppSettingByName(String settingName, String settingValue){
		
		Session session = getSession();

		Criteria criteria = session.createCriteria(AppSetting.class);
		criteria.add(Restrictions.ilike("name", settingName));
		
		
		
		AppSetting setting = (AppSetting) criteria.uniqueResult();
		
		if(setting == null){
			
			setting = new AppSetting();
			setting.setName(settingName);
			setting.setValue(settingValue);
			session.save(setting);
		}
		
		
		return setting;
		
	}

	public void test(){
		AppUser appUser = null;
		AppUserCampaign appUserCampaign  = new AppUserCampaign();

		appUserCampaign.setName("Nose Implant");
		appUserCampaign.setStartDate(new Timestamp(System.currentTimeMillis()));
		appUserCampaign.setEndDate(new Timestamp(System.currentTimeMillis()));
		appUserCampaign.setStatus(CampaignStatus.RUNNING);
		appUserCampaign.setDescription("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia," +
				" looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n" +
				"\n" +
				"The standard chunk of Lorem Ipsum used since the 1500s is reproduced" +
				" below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum " +
				"et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by " +
				"English versions from the 1914 translation by H. Rackham. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.");
		appUserCampaign.setAmount(new BigDecimal("2000"));
		String hql  = "select l from AppUser l where l.username  = 'olajire'";
		try {
			 appUser = (AppUser) getUniqueRecordByHql(hql);
			appUserCampaign.setAppUser(appUser);
			CampaignCategory campaignCategory = (CampaignCategory) getRecordById(CampaignCategory.class,10L);
			appUserCampaign.setCampaignCategory(campaignCategory);
			saveRecord(appUserCampaign);

		} catch (Exception e) {
			e.printStackTrace();
		}


		/*Criteria criteria = null;
		try {
			criteria = sessionFactory.getCurrentSession().createCriteria(AppUserCampaign.class);
			criteria
					.add(Restrictions.in("status",
							CampaignStatus.PENDING,
							CampaignStatus.RUNNING
					))
			.add(Restrictions.eq("appUser.id",appUser.getId()));

			ProjectionList projectionList = Projections.projectionList()
					.add(Projections.property("id"), "id")
					.add(Projections.property("id"), "id")
					.add(Projections.property("startDate"), "startDate")
					.add(Projections.property("endDate"), "endDate")
					.add(Projections.property("description"), "description")
					.add(Projections.property("campaignCategory"), "campaignCategory")
					.add(Projections.property("amount"), "amount")
					.add(Projections.property("status"), "status")
					.add(Projections.property("name"), "name");

			criteria.setProjection(projectionList)
					.setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));
			List<CurrentCampaignPojo> currentCampaignPojos = criteria.list();


			System.out.println("=========================");

		} catch (Exception e) {
			e.printStackTrace();
		}
*/
	}
}
