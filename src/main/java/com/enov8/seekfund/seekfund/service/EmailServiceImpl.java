package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.mail.Mail;
import com.enov8.seekfund.seekfund.model.EmailQueue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

//@Service
public class EmailServiceImpl implements EmailService {
    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
    private JavaMailSender emailSender;
    private SessionFactory sessionFactory;
    private DatabaseService databaseService;
    private SpringTemplateEngine templateEngine;

    public EmailServiceImpl(JavaMailSender emailSender,
                            DatabaseService databaseService,
                            SessionFactory sessionFactory,
                            SpringTemplateEngine templateEngine) {

        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
        this.databaseService = databaseService;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void sendSimpleMessage(Mail mail) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

      /*  helper.addAttachment("logo.png",
                new ClassPathResource("templates/memorynotfound-logo-60x-60x.png"));*/

        Context context = new Context();
       // context.setVariables(mail.getModel());
        String html = templateEngine.process("mail/welcome", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }

    @Override
    public void sendUserEmailConfirmation(EmailQueue emailQueue) throws MessagingException, IOException {




        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context context = new Context();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String json  = Pattern.compile("\\\\")
                .matcher(emailQueue.getData())
                .replaceAll("\\\\\\\\");
        Map<String, Object> map = new HashMap<String, Object>();
        Mail mail = new Mail();
        mail.setTo(emailQueue.getToEmail());
        mail.setFrom(emailQueue.getFromEmail());
        mail.setSubject("Welcome to Seekfunds.");
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        mail.setModel(map);
        context.setVariables(mail.getModel());
        String html = templateEngine.process("mail/"+emailQueue.getEmailTemplate(), context);
        String[] toArray  = mail.getTo().split(",");
        if(toArray.length > 1){
            helper.setTo(toArray);
        }else{
            helper.setTo(mail.getTo());
        }
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
        logger.info("sent email to " + mail.getTo());



    }

}
