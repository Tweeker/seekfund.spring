package com.enov8.seekfund.seekfund.service;

import com.amazonaws.services.opsworks.model.App;
import com.enov8.seekfund.seekfund.config.SeekfundProperties;
import com.enov8.seekfund.seekfund.enumumeration.*;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.model.EmailQueue;
import com.enov8.seekfund.seekfund.model.SocialAuthUser;
import com.enov8.seekfund.seekfund.model.attribute.SocialRegister;
import com.enov8.seekfund.seekfund.pojo.email.ConfirmEmailPojo;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class AppUserCreationService {

    private static final Logger logger = LoggerFactory.getLogger(AppUserCreationService.class);
    DatabaseService databaseService;

    SessionFactory sessionFactory;

    CommonMethod commonMethod;
    @Autowired
    AesService aesService;
    @Autowired
    SeekfundProperties seekfundProperties;

    private final  String ivKey = "wmJiSEvR7yAC5fyB";
    private final String secretKey =  "ssdkF$KUy2O#D%kd";

    public AppUserCreationService(DatabaseService databaseService,
                                  SessionFactory sessionFactory,CommonMethod commonMethod) {
        this.databaseService = databaseService;
        this.sessionFactory = sessionFactory;
        this.commonMethod = commonMethod;
    }

    public AppUser activateUserEmail(String encrytedData, String remoteIp) throws Exception{


        Assert.notNull(encrytedData,"Unexpected data");
        AppUser toDecryptAppUser  = new AppUser();
       String encrypt = aesService.decrypt(encrytedData,ivKey,secretKey);

        ObjectMapper objectMapper = new ObjectMapper();
        toDecryptAppUser = objectMapper.readValue(encrypt,AppUser.class);

        Assert.notNull(toDecryptAppUser.getId(),"Unexpected Id ");


        AppUser appUser  = (AppUser) databaseService.getRecordById(AppUser.class,toDecryptAppUser.getId());

         Assert.notNull(appUser,"User does not exist!");

        Assert.isTrue(!appUser.isEmailConfirmed(),"User is currently active");
        //TODO remove later
        if(toDecryptAppUser.getHashEntry() == null){
            toDecryptAppUser.setHashEntry(toDecryptAppUser.getResetPassswordTokenAlt());
        }
        Assert.notNull(toDecryptAppUser.getHashEntry(),"no token found (2)");

        Assert.isTrue(appUser.getStatus().getValue()
                .equalsIgnoreCase(AppUserStatus.AWAITING_EMAIL_CONF.getValue())," User is currently " + appUser.getStatus());
        Assert.notNull(appUser.getHashEntry(),"no token found (1)");

        if(!toDecryptAppUser.getHashEntry().equalsIgnoreCase(appUser.getHashEntry())){

            throw new Exception("invalid token");
        }



        appUser.setHashEntry(null);
        appUser.setStatus(AppUserStatus.ACTIVE);
        appUser.setEmailConfirmed(true);
        appUser.setDateEmailConfirmed(new Timestamp(System.currentTimeMillis()));
        appUser.setResetPassswordTokenAlt(null);
        appUser.setResetPassswordToken(null);

        databaseService.updateRecord(appUser);
        logger.info("==========user activation complete ================");

        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "Confirmed Email with Ip" +
                        " : " + remoteIp, AppUser.class.getSimpleName(),
                ActivityType.UPDATE, appUser.getId(), appUser.getId());



        return appUser;


    }
    public String createActivationEmailQueue(AppUser appUser) throws Exception{

        Assert.notNull(appUser,"AppUser cannot be null");

        String uuid  = UUID.randomUUID().toString();

        AppUser toEncryptAppUser  = new AppUser();
        toEncryptAppUser.setId(appUser.getId());
        toEncryptAppUser.setHashEntry(uuid);

        String encrypt = aesService.encrypt(toEncryptAppUser,ivKey,secretKey);
        ConfirmEmailPojo confirmEmailPojo = new ConfirmEmailPojo();
        confirmEmailPojo.setHref(seekfundProperties.getBaseUrl()+"/activate/email/"+encrypt);
        confirmEmailPojo.setSenderName("Admin");
        confirmEmailPojo.setUsername(appUser.getUsername());

        EmailQueue emailQueue = new EmailQueue();
        emailQueue.setDateToSend(new java.sql.Timestamp(System.currentTimeMillis()));
        emailQueue.setEmailTemplate("welcome");
        emailQueue.setFromEmail("noreply@seekfunds.ng");
        emailQueue.setType(EmailQueueType.USER_REGISTER);
        emailQueue.setToEmail(appUser.getEmail());
        ObjectMapper objectMapper = new ObjectMapper();
        String objStr = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(confirmEmailPojo);
        //String objStr = objectMapper.writeValueAsString(confirmEmailPojo);
        emailQueue.setData(objStr);
        emailQueue.setTitle("Welcome to Seekfunds");
        emailQueue.setType(EmailQueueType.USER_REGISTER);

        databaseService.saveRecord(emailQueue);

        return uuid;


    }
    public AppUser createSocialAuthUser (
            SocialAuthUser socialAuthUser,
            SocialRegister socialRegister,
            String remoteIp)throws SeekFundException,Exception{

        boolean validUsername  = commonMethod.isValidString(socialRegister.getUsername()) &&
                commonMethod.isValidUsername(socialRegister.getUsername());


        if(!validUsername){

            throw new SeekFundException("Invalid Username");
        }

        List<AppUser> testAppUser = null;
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        criteria.add(Restrictions.eq("username", socialRegister.getUsername().
                trim()).ignoreCase());

        testAppUser = criteria.list();

        if(testAppUser != null && !testAppUser.isEmpty()){

            throw new SeekFundException("Username Already Exist");
        }


        AppUser appUserToCreate = new AppUser();

        appUserToCreate.setEmail(socialAuthUser.getEmail().trim());

        appUserToCreate.setHashedPassword("");

        appUserToCreate.setEmailConfirmed(Boolean.FALSE);

        appUserToCreate.setUsername(socialRegister.getUsername().trim().toLowerCase());

        appUserToCreate.setType(AppUserType.USER);

        appUserToCreate.setStatus(AppUserStatus.ACTIVE);
        //TODO send verification email


        AppUserRole role = new AppUserRole();
        role.setName(AppUserRoleConstant.USER);
        HashSet<AppUserRole> roles = new HashSet<AppUserRole>();
        roles.add(role);
        appUserToCreate.setRoles(roles);

        appUserToCreate.setLastLoginIP(remoteIp);
        appUserToCreate.setLoginCounter(1);

        databaseService.saveRecord(appUserToCreate);
        logger.info("==========user creation complete ================");

        databaseService.createAuditTrail(socialRegister.getUsername().trim() + " " +
                        "Registered Social User with Ip : " + remoteIp, AppUser.class.getSimpleName(),
                ActivityType.INSERT, appUserToCreate.getId(), appUserToCreate.getId());



        socialAuthUser.setAppUser(appUserToCreate);

        socialAuthUser.setStatus(GenericStatusConstant.ACTIVE);

        databaseService.updateRecord(socialAuthUser);


        return appUserToCreate;
    }
    public Collection<GrantedAuthority> getAuthorites(Set<AppUserRole> roles){
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
            AppUserRole appUserRole = (AppUserRole) iterator.next();
            authorities.add(new
                    SimpleGrantedAuthority(appUserRole.getName().getValue()));

        }

        return authorities;
    }


}
