package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.mail.Mail;
import com.enov8.seekfund.seekfund.model.EmailQueue;

import javax.mail.MessagingException;
import java.io.IOException;

public interface EmailService {
    void sendSimpleMessage(Mail mail) throws MessagingException, IOException;
    void sendUserEmailConfirmation(EmailQueue emailQueue) throws MessagingException, IOException;
}
