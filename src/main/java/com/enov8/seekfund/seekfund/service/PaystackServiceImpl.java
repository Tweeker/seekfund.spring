package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.ListTransactionResp;
import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.QueryString;
import com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber.ResolveAccountNumberResp;
import com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn.ResolveBvnResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontimeline.TransactionTimelineResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontotal.TransactionTotalResp;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

public class PaystackServiceImpl implements PaystackService {

    @Autowired
    @Qualifier("payStackClient")
    OkHttpClient okHttpClient;

    @Value("${paystack.host}")
    private String PAYSTACK_HOST;

    @Value("${paystack.header.auth}")
    private String PAYSTACK_SEEKFUND_PUBLIC_KEY;
    @Override
    public void testPaystackConnection() throws IOException {
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST).newBuilder();
        // urlBuilder.addQueryParameter("id", "1");



        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)

                .build();
        ///OkHttpClient client = new OkHttpClient();
        Call call = okHttpClient.newCall(request);

        Response response = call.execute();
       System.out.println(response.body().string());

    }


    @Override
    public VerifyTransactionResp verifyPayment(String reference) throws IOException {
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/transaction/verify/"+reference).newBuilder();
        // urlBuilder.addQueryParameter("id", "1");



        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        ///OkHttpClient client = new OkHttpClient();
        Call call = okHttpClient.newCall(request);

        VerifyTransactionResp resp = null;
        Response response = call.execute();
        System.out.println(response.message());
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

            resp = objectMapper.readValue(response.body().string(),VerifyTransactionResp.class);

        }
        //System.out.println(response.body().string());


        return resp;

    }

    @Override
    public ListTransactionResp listTransactions(QueryString queryString) throws IOException {

        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/transaction").newBuilder();

        if(queryString.getAmountInKobo() != null){
            urlBuilder.addQueryParameter("amount", queryString.getAmountInKobo()+"");
        }
        if(queryString.getStatus() != null){
            urlBuilder.addQueryParameter("status", queryString.getStatus()+"");
        }
        if(queryString.getPage() != null){
            urlBuilder.addQueryParameter("page", queryString.getPage()+"");
        }
        if(queryString.getPerPage() != null){
            urlBuilder.addQueryParameter("perPage", queryString.getPerPage()+"");
        }
        if(queryString.getFromDate() != null){
            urlBuilder.addQueryParameter("from", queryString.getFromDate()+"");
        }
        if(queryString.getToDate() != null){
            urlBuilder.addQueryParameter("to", queryString.getToDate()+"");
        }

        String url = urlBuilder.build().toString();



        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        Call call = okHttpClient.newCall(request);
        ListTransactionResp resp = new ListTransactionResp();
        Response response = call.execute();
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

             resp  =  objectMapper.readValue(response.body().string(),ListTransactionResp.class);

            System.out.println("===============");

        }
        //System.out.println("Message " + response.message());
        return resp;

    }

    @Override
    public ResolveBvnResp resolveBvn(String bvn) throws IOException {
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/bank/resolve_bvn/"+bvn).newBuilder();



        String url = urlBuilder.build().toString();



        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        Call call = okHttpClient.newCall(request);
        ResolveBvnResp resp = new ResolveBvnResp();
        Response response = call.execute();
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

            resp  =  objectMapper.readValue(response.body().string(),ResolveBvnResp.class);

            System.out.println("===============");

        }
        //System.out.println("Message " + response.message());
        return resp;
    }

    @Override
    public ResolveAccountNumberResp resolveAcountNumber(String accountNumber, String bankCode) throws IOException {


        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/bank/resolve").newBuilder();

        urlBuilder.addQueryParameter("account_number", accountNumber);

        urlBuilder.addQueryParameter("bank_code", bankCode);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        Call call = okHttpClient.newCall(request);
        ResolveAccountNumberResp resp = new ResolveAccountNumberResp();

        Response response = call.execute();
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

            resp  =  objectMapper.readValue(response.body().string(),ResolveAccountNumberResp.class);

            System.out.println("===============");

        }
        //System.out.println("Message " + response.message());
        return resp;

    }

    private TransactionTotalResp transactionTotal( HttpUrl.Builder builder) throws IOException {

        String url = builder.build().toString();

        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        Call call = okHttpClient.newCall(request);
        TransactionTotalResp resp = new TransactionTotalResp();

        Response response = call.execute();
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

            resp  =  objectMapper.readValue(response.body().string(),TransactionTotalResp.class);

        }
        //System.out.println("Message " + response.message());
        return resp;
    }
    @Override
    public TransactionTotalResp transactionTotal( String toDate, String fromDate) throws IOException {

        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/transaction/totals").newBuilder();

        urlBuilder.addQueryParameter("from", fromDate);

        urlBuilder.addQueryParameter("to", toDate);


        return transactionTotal(urlBuilder);

    }

    @Override
    public TransactionTotalResp transactionTotal() throws IOException {

        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/transaction/totals").newBuilder();

        return transactionTotal(urlBuilder);
    }

    @Override
    public TransactionTimelineResp transactionTimeline(Long id) throws IOException {

        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(PAYSTACK_HOST+"/transaction/timeline/"+id).newBuilder();

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .header("User-Agent", Constant.MOZILLA_AGENT)
                .header("Authorization","Bearer "+ PAYSTACK_SEEKFUND_PUBLIC_KEY)
                .url(url)

                .build();

        Call call = okHttpClient.newCall(request);
        TransactionTimelineResp resp = new TransactionTimelineResp();

        Response response = call.execute();
        if(response.message().equalsIgnoreCase("OK")){
            ObjectMapper objectMapper = new ObjectMapper();

            resp  =  objectMapper.readValue(response.body().string(),TransactionTimelineResp.class);

        }
        //System.out.println("Message " + response.message());
        return resp;
    }


}
