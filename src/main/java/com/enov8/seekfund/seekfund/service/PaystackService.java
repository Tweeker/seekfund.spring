package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.ListTransactionResp;
import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.QueryString;
import com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber.ResolveAccountNumberResp;
import com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn.ResolveBvnResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontimeline.TransactionTimelineResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontotal.TransactionTotalResp;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;

import java.io.IOException;

public interface PaystackService {


    void testPaystackConnection() throws IOException;
    VerifyTransactionResp verifyPayment(String reference) throws IOException;
    ListTransactionResp listTransactions(QueryString queryString) throws IOException;
    ResolveBvnResp resolveBvn(String bvn) throws IOException;
    ResolveAccountNumberResp resolveAcountNumber(String accountNumber, String bankCode) throws IOException;
    TransactionTotalResp transactionTotal(String toDate, String fromDate) throws IOException;
    TransactionTotalResp transactionTotal() throws IOException;
    TransactionTimelineResp transactionTimeline(Long id) throws IOException;
}
