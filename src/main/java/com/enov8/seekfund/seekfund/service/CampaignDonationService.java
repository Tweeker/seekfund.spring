package com.enov8.seekfund.seekfund.service;

import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.CampaignDonation;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class CampaignDonationService {
    CommonMethod commonMethod;
    @Autowired
    DatabaseService dbService;
    @Autowired
    private SessionFactory sessionFactory;
    public String createPaymentRef(){

        String ts = String.valueOf(System.currentTimeMillis());
        String rand = UUID.randomUUID().toString();
        return DigestUtils.sha1Hex(ts + rand);


    }
    public CampaignDonation getCampaignDonation(String reference, Long campaignId){

        Assert.notNull(reference,"Reference is missing");
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class);

        criteria.createAlias("campaign","c");

        criteria
                .add(Restrictions.eq("paymentRef",
                        reference.trim()))
                .add(Restrictions.eq("c.id",campaignId));

        criteria.setMaxResults(1);



        List<CampaignDonation> donationList =  criteria.list();


        return  donationList.size() > 0 ? donationList.get(0) : null;

    }
    public CampaignDonation getPendingCampaignDonation(String sessionId, Long campaignId){

        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class);

        criteria.createAlias("campaign","c");

        criteria
                .add(Restrictions.eq("paymentStatus",
                        PaymentStatus.PENDING))

                .add(Restrictions.eq("c.id",campaignId))
                .add(Restrictions.eq("sessionId",sessionId));




        criteria.setMaxResults(1);



        List<CampaignDonation> donationList =  criteria.list();


        return  donationList.size() > 0 ? donationList.get(0) : null;












    }
}
