package com.enov8.seekfund.seekfund.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class AesService {
    private static  final Logger logger = LoggerFactory.getLogger(AesService.class);


    private Cipher getCypherInstance(int cipherMode,String ivKey, String secretKey) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {

        IvParameterSpec  ivParameterSpec = new IvParameterSpec(ivKey.getBytes("UTF-8"));
        SecretKeySpec  secretKeySpec = new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(cipherMode, secretKeySpec, ivParameterSpec);


        return cipher;

    }

    public String encrypt(String toBeEncrypt,String ivKey, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {

        Cipher cipher = getCypherInstance(Cipher.ENCRYPT_MODE,ivKey,secretKey);
        byte[] encrypted = cipher.doFinal(toBeEncrypt.getBytes());
        //return byteArrayToHex(encrypted);
        return Base64.encodeBase64String(encrypted);
    }

    public String encrypt(Object object,String ivKey, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, JsonProcessingException {


        Cipher cipher = getCypherInstance(Cipher.ENCRYPT_MODE,ivKey,secretKey);
        ObjectMapper objectMapper = new ObjectMapper();
        String toBeEncypted  = objectMapper.writeValueAsString(object);
        logger.info("to be encrypted  " + toBeEncypted);
        byte[] encrypted = cipher.doFinal(toBeEncypted.getBytes());
        return byteArrayToHex(encrypted);
        //return Base64.encodeBase64String(encrypted);


    }
    public String decrypt(String encrypted,String ivKey, String secretKey) throws InvalidAlgorithmParameterException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, UnsupportedEncodingException {
        Cipher cipher = getCypherInstance(Cipher.DECRYPT_MODE,ivKey,secretKey);
        byte[] decryptedBytes = cipher.doFinal(hexStringToByteArray(encrypted));
        return new String(decryptedBytes, 0, decryptedBytes.length, "ASCII");
      // return new String(decryptedBytes, 0, decryptedBytes.length, "ASCII");

        //return  new String(hexStringToByteArray(decryptedBytes.toString()));
    }
    private String byteArrayToHex(byte[] a)
    {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    //  Converting Hex String to Byte[]
    private  byte[] hexStringToByteArray(String s)
    {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }
}
