package com.enov8.seekfund.seekfund.exception;

public class RegisterException extends Exception{
	   public RegisterException(String message) {
	        super(message);
	    }
}
