package com.enov8.seekfund.seekfund.exception;

public class SeekFundException extends Exception{
	   public SeekFundException(String message) {
	        super(message);
	    }
}
