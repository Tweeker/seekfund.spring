package com.enov8.seekfund.seekfund.controller;

import com.enov8.seekfund.seekfund.api.ApiAdvice;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice(basePackages="com.enov8.seekfund.seekfund.controller", annotations = Controller.class)
public class WebControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(WebControllerAdvice.class);
    @ExceptionHandler(Exception.class)
    public @ResponseBody
    RestResponsePojo exception(Exception e, HttpServletResponse response) {
        logger.error("Exception during execution of  application " + e.getMessage());
        e.printStackTrace();
        logger.info("Please implement a web controller advice exception page!.");
        RestResponsePojo restPojo = new RestResponsePojo();
        restPojo.setReason("Unable to process request at the moment");
        restPojo.setSuccess(Boolean.FALSE);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(200);
        return restPojo;
    }
}
