package com.enov8.seekfund.seekfund.controller;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.enumumeration.*;

import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;

import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.model.SocialAuthUser;
import com.enov8.seekfund.seekfund.model.attribute.SocialRegister;
import com.enov8.seekfund.seekfund.pojo.AppUserProfilePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.pojo.twitter.verifycredentials.VerifyCredentials;
import com.enov8.seekfund.seekfund.security.OauthSocialUserRetriever;
import com.enov8.seekfund.seekfund.service.AesService;
import com.enov8.seekfund.seekfund.service.AppUserCreationService;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.social.oauth1.AuthorizedRequestToken;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Parameters;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;


@Controller
@Transactional
public class DefaultController {
    @Autowired
    DatabaseService databaseService;
    @Autowired CommonMethod commonMethod;
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AppUserDao appUserService;
    @Autowired
    AesService aesService;
    @Autowired
    AppUserCreationService appUserCreationService;
    @Autowired
    CampaignService campaignService;
    @Value("${paystack.pk_public_key}")
    private String PK_KEY ;
    @Autowired
    TwitterConnectionFactory connectionFactory;
    @Value("${twitter.callbackUrl}")
    String twitterCallbackUr;

    @Value("${twitter.consumerKey}")
    private String twitter_consumer_key;
    @Value("${twitter.consumerSecret}")
    private String twitter_consumer_secret;



    @GetMapping("/connect/twitter")
    public void  twitterConnect(HttpServletRequest request,
                              Authentication authentication,
                              HttpServletResponse response,
                              RedirectAttributes attributes) throws Exception {


        OAuth1Operations oauthOperations = connectionFactory.getOAuthOperations();
        String verifier = request.getParameter("oauth_verifier");
        OAuthToken requestToken = (OAuthToken) request.getSession().getAttribute("requestToken");
        request.getSession().removeAttribute("requestToken");
        OAuthToken accessToken = oauthOperations.exchangeForAccessToken(
                new AuthorizedRequestToken(requestToken, verifier), null);

          String sAccessToken = accessToken.getValue();
        String accessTokenSecret = accessToken.getSecret();
        Twitter twitter = new TwitterTemplate( twitter_consumer_key,
                twitter_consumer_secret, sAccessToken, accessTokenSecret );


        SecurityContext context =  SecurityContextHolder.getContext();
        TwitterProfile profile = twitter.userOperations().getUserProfile();

        logger.info(" ============== twitter profile id" + profile.getId() + " =====================");
        boolean isLoggedIn  = commonMethod.userIsLoggedOn(authentication);
        Authentication userAuth = null;
        String targetUrl  = "/";
        String hql  = "";
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        AppUser appUser = new AppUser();
        appUser.setSocialAuthType(SocialAuthType.TWITTER);

        AppUserRole role = new AppUserRole();
        role.setName(AppUserRoleConstant.SOCIAL_AUTH);
        HashSet<AppUserRole> roles = new HashSet<AppUserRole>();
        roles.add(role);

       SocialAuthUser socialAuthUser;
        if(isLoggedIn){
           hql = "select a from SocialAuthUser a where a.auth_id " +
                    "=" +
                    " '" + profile.getId()+ "' and a.authType =  " +
                    "'" + SocialAuthType.TWITTER + "'";
        }else{
            hql  = "select a from SocialAuthUser a where a.auth_id" +
                    " " +
                    "= '" +  profile.getId()+ "' and a.authType =  " +
                    "'" + SocialAuthType.TWITTER  + "'";

            appUser.setSocialPrincipal(profile.getId());
        }


        socialAuthUser = (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);


        if(socialAuthUser == null){

            OauthSocialUserRetriever oauthSocialUserRetriever = new OauthSocialUserRetriever();


            socialAuthUser = oauthSocialUserRetriever.
                    getTwitterUser(profile,sAccessToken,isLoggedIn,accessToken);


            databaseService.saveRecord(socialAuthUser);

            authorities =  commonMethod.getAuthorites(roles);

            if(isLoggedIn){
                targetUrl = "/home";
            }else{
                targetUrl = Constant.SOCIAL_REGISTER_ROUTE;
            }

            userAuth = new UsernamePasswordAuthenticationToken(appUser,"",
                    authorities);
            //todo set userauth here


        }else{

            if(isLoggedIn){
                socialAuthUser.setStatus(GenericStatusConstant.ACTIVE);
                AppUser sAppUser = (AppUser) authentication.getPrincipal();
                socialAuthUser.setAppUser(sAppUser);
                databaseService.updateRecord(socialAuthUser);
                //TODO ENSURE THAT IF USER IS ALREADY TIED WITH A SOCIAL AUTH USER ,...
                // THE LOGGED IN USER IS THE SAME AS SOCIAL AUTH USER
                targetUrl = "/home";

            }else{
                //user is not logged in
                if(socialAuthUser.getStatus().getValue()
                        .equalsIgnoreCase(GenericStatusConstant.ACTIVE.getValue())
                        && socialAuthUser.getAppUser() != null){

                    appUser = (AppUser) databaseService.getRecordById(AppUser.class,
                            socialAuthUser.getAppUser().getId());
                    authorities =  commonMethod.getAuthorites(appUser.getRoles());

                    userAuth = new UsernamePasswordAuthenticationToken(appUser,appUser.getHashedPassword(),
                            authorities);
                    //context.setAuthentication(userAuth);
                    targetUrl = "/home";
                }else{
                    authorities =  commonMethod.getAuthorites(roles);

                    userAuth = new UsernamePasswordAuthenticationToken(appUser,"",
                            authorities);
                    //context.setAuthentication(userAuth);
                    targetUrl = Constant.SOCIAL_REGISTER_ROUTE;

                }

            }


        }


        context.setAuthentication(userAuth);



/*

        RestTemplate restTemplate = ((TwitterTemplate) twitter).getRestTemplate();

        VerifyCredentials verifyCredentials = restTemplate
                .getForObject("https://api.twitter.com/1.1/account/" +
                                "verify_credentials.json?include_email=true",
                VerifyCredentials.class);
        System.out.println("Profile Info with Email: "+ verifyCredentials.toString());
*/

        response.sendRedirect( targetUrl);




    }
    @GetMapping("/signin/twitter")
    public void twitterSignin(HttpServletRequest request,
      HttpServletResponse response,
      RedirectAttributes attributes) throws Exception{

        OAuth1Operations oauthOperations = connectionFactory.getOAuthOperations();
        OAuthToken requestToken =
                oauthOperations.fetchRequestToken( twitterCallbackUr, null );

        request.getSession().setAttribute("requestToken", requestToken);

        String authorizeUrl = oauthOperations.buildAuthenticateUrl( requestToken.getValue(),
                OAuth1Parameters.NONE );
        response.sendRedirect( authorizeUrl );
    }

    private static final Logger logger = LoggerFactory.getLogger(DefaultController.class);

    @RequestMapping(value = "/swagger-ui.html", method = RequestMethod.GET,
            produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public Resource getFromClasspath() {
        return new ClassPathResource("META-INF/resources/swagger-ui.html");
    }

    @GetMapping("/activated/email")
    public ModelAndView  activateEmail(ModelMap model,

                                      HttpServletRequest httpServletRequest,
                                      RedirectAttributes attributes) throws Exception{

       // String username  = attributes.
        if(model.containsKey("username")){
            return new ModelAndView("/activate.email", model);
        }
        return new ModelAndView("redirect:/login", model);

    }
    @GetMapping("/campaign-donation/{campaignId}")
    public ModelAndView activateEmail(ModelMap model,
                      Principal principal,@PathVariable("campaignId") String campaignId,
                      RedirectAttributes attributes) throws Exception{

        Long lCampaignId  = Long.parseLong(
                campaignId
                .replace("SEEKFUND","")
                .replace("seekfund","")
                .replace("-",""));



        CurrentCampaignPojo currentCampaignPojo = campaignService.getCampaignById(lCampaignId);

        if(currentCampaignPojo == null){
            model.addAttribute("error","No Campaign Found with Reference " + campaignId);
        }
        if(currentCampaignPojo != null && currentCampaignPojo.getStatus() != null){

            if(!currentCampaignPojo.getStatus().getValue().equalsIgnoreCase(CampaignStatus.RUNNING.getValue())){
                model.addAttribute("error","Campaign is currently " + currentCampaignPojo.getStatus());
            }
        }


        model.addAttribute("pk_key",PK_KEY);
        model.addAttribute("campaign",currentCampaignPojo);

        return new ModelAndView("/campaign_donate", model);

    }
    @GetMapping("/activate/email/{encryptedData}")
    public RedirectView activateEmail(Model model, @PathVariable("encryptedData") String encryptedData,

                                      HttpServletRequest httpServletRequest,
                                      RedirectAttributes attributes) throws Exception{

        logger.info(encryptedData);
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);


        AppUser appUser = appUserCreationService.activateUserEmail(encryptedData, remoteIp);

        attributes.addFlashAttribute("username", appUser.getUsername());


        //attributes.addFlashAttribute("success","true");


        return new RedirectView("/activated/email");
       // return new ModelAndView("/activate.email", model);


    }
    //@ModelAttribute("appUser")
    public RestResponsePojo getAppUser(Authentication authentication) {
        RestResponsePojo restResponsePojo = new RestResponsePojo();
        AppUserProfilePojo appUserProfilePojo = new AppUserProfilePojo();
        try {
            if (authentication != null
                    && authentication.getPrincipal() != null
                    && authentication.getPrincipal() instanceof AppUser) {

                AppUser appUser = (AppUser) authentication.getPrincipal();
                restResponsePojo.setSuccess(false);

                if (appUser != null && appUser.getId() != null) {
                    /*String hql = " select a.id as id, a.username as username , " +
                            "a.email as email, " +
                            " " +
                            "a.dateCreated as dateCreated" +
                            " " +
                            " from AppUser a where a.id = " + appUser.getId();


                    appUserProfilePojo = (AppUserProfilePojo) databaseService
                            .getUniqueRecordByHql(hql, AppUserProfilePojo.class);*/
                  //  AppUserProfilePojo AppUserProfilePojo = new AppUserProfilePojo();
                    appUserProfilePojo.setDateCreated(appUser.getDateCreated());
                    appUserProfilePojo.setEmail(appUser.getEmail());
                    appUserProfilePojo.setUsername(appUser.getUsername());
                    appUserProfilePojo.setId(appUser.getId());
                    restResponsePojo.setData(appUserProfilePojo);
                    restResponsePojo.setSuccess(true);


                }

            }

        } catch (Exception e) {
            logger.info(e.getMessage());
            // e.printStackTrace();
        }

        return restResponsePojo;

    }
    @GetMapping(Constant.SOCIAL_REGISTER_ROUTE)
    public String socialRegister(Model model, Authentication authentication) throws Exception {

        model.addAttribute("appUser", getAppUser(authentication));

        AppUser appUser = (AppUser) authentication.getPrincipal();
        if(appUser != null && appUser.getSocialPrincipal() != null && appUser.getSocialAuthType() != null){
           String  hql  = "select a from SocialAuthUser a where a.auth_id " +
                    "= '" + appUser.getSocialPrincipal().toString()+ "' and a.authType =  " +
                    "'" + appUser.getSocialAuthType() + "'";

            SocialAuthUser socialAuthUser =  (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);
            model.addAttribute("socialAuthUser",socialAuthUser);


        }

        model.addAttribute("socialRegister",new SocialRegister());
        return "social_auth";
    }
    @PostMapping(Constant.SOCIAL_REGISTER_ROUTE)
    public RedirectView addSocialAccount(Model model,
     RedirectAttributes attributes,
       HttpServletRequest httpServletRequest ,
     @ModelAttribute SocialRegister socialRegister , Authentication authentication) throws Exception {

        AppUser appUser = (AppUser) authentication.getPrincipal();
        if(appUser == null){ throw new Exception("Cannot find stored user");}

        if(appUser.getId() != null){ throw new Exception("User already stored");}

        if(!(appUser.getSocialPrincipal() != null && appUser.getSocialAuthType() != null)) {

            throw new Exception("Unexpected data in authentication!!.");
        }
        String  hql  = "select a from SocialAuthUser a where a.auth_id " +
                "= '" + appUser.getSocialPrincipal().toString()+ "' and a.authType =  " +
                "'" + appUser.getSocialAuthType() + "'";

        SocialAuthUser socialAuthUser =  (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);

        if(socialAuthUser == null){
            attributes.addFlashAttribute("errorMessage","Cannot find social user");

            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);

        }

        if(!socialAuthUser.getStatus().getValue()
                .equalsIgnoreCase(GenericStatusConstant.PENDING.getValue())){
            attributes.addFlashAttribute("errorMessage","Social Register is not pending!.");
            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);

        }




        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);

        try{

            AppUser testEmailUser  =  appUserService.getAppUserByEmail(socialAuthUser.getEmail());
            if(testEmailUser != null){
                attributes.addFlashAttribute("errorMessage","Email "+ socialAuthUser.getEmail()+
                        " cannot be used for this process !.");
                return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);
            }
            AppUser createdUser  = appUserCreationService
                    .createSocialAuthUser(socialAuthUser,socialRegister,remoteIp);

            Collection<GrantedAuthority> authorities =
                    appUserCreationService.getAuthorites(createdUser.getRoles());




            Authentication userAuth = new UsernamePasswordAuthenticationToken(createdUser,"",
                    authorities);
            SecurityContext context =  SecurityContextHolder.getContext();
            context.setAuthentication(userAuth);
            logger.info("DOne creating social User");


        }catch (SeekFundException e){
            attributes.addFlashAttribute("errorMessage",e.getMessage());

            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);
        } catch (Exception e){
            e.printStackTrace();
            attributes.addFlashAttribute("errorMessage",e.getMessage());

            return  new RedirectView("/error");
        }
        return new RedirectView("/home");

    }
    @GetMapping("/accessDenied")
    public String accessDenied(HttpServletResponse httpServletResponse,Authentication authentication, Model model) {

        //System.out.println(httpServletResponse.getStatus());

        return "error/403";
    }
    //value = {"*", "/home/**", "/campaigns/**"})
    @GetMapping(value={"*", "/home/**", "/campaigns/**","/campaign/**"})
    public String index(HttpServletResponse httpServletResponse,Authentication authentication, Model model){

       //System.out.println(httpServletResponse.getStatus());
        //TODO get OAUTH details
        //
        //restResponsePojo.setData(appUserProfilePojo);


        model.addAttribute("appUser", getAppUser(authentication));
        return "index";

    }
}
