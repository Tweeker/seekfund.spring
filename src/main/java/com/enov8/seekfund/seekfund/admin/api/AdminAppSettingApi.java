package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.api.DefaultApi;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppSetting;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.CampaignDonation;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignDonationPojo;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.CriteriaProjectService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@Transactional
public class AdminAppSettingApi {

    private SimpleDateFormat sdf  = new SimpleDateFormat(Constant.SERVER_DATETIME_FORMAT);
    @Autowired
    DatabaseService databaseService;


    @Autowired
    CommonMethod commonMethod;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CampaignService campaignService;
    @Autowired
    DefaultApi defaultApi;
    @Autowired
    CriteriaProjectService criteriaProjectService;
    @ApiOperation(value = "get App Setting")
    @GetMapping("/admin/api/setting")
    public RestResponsePojo2<DataTableResponsePojo2<AppSetting>> getAppSetting(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
                                                                                     @RequestParam(value =
               Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
               defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start
            ) throws SeekFundException, Exception {

        RestResponsePojo2<DataTableResponsePojo2<AppSetting>>
                restResponsePojo = new RestResponsePojo2<>();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppSetting.class,"a");
        Criteria countCriteria = sessionFactory.getCurrentSession().createCriteria(AppSetting.class,"a");


        //criteria.setResultTransformer(Transformers.aliasToBean(AppSetting.class));
        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());




        List<AppSetting> appSettings = (List<AppSetting>)  criteria.list();

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<AppSetting> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(appSettings);

        restResponsePojo.setData(dtRes);


        return  restResponsePojo;

    }
    @ApiOperation(value = "Update App Setting")
    @PutMapping("/admin/api/setting")
    public RestResponsePojo2<Boolean> updateAppSetting(@RequestBody AppSetting appSetting
         ) throws SeekFundException, Exception {

        RestResponsePojo2<Boolean>
                restResponsePojo = new RestResponsePojo2<>();


        Assert.notNull(appSetting.getId(), "Cannot find id in app setting");

        Assert.isTrue(commonMethod.isValidString(appSetting.getName())," setting name is required ");

        Assert.isTrue(commonMethod.isValidString(appSetting.getValue())," setting value is required ");


        AppSetting mAppSetting  = (AppSetting) databaseService.getRecordById(AppSetting.class,appSetting.getId());

        mAppSetting.setName(appSetting.getName());
        mAppSetting.setValue(appSetting.getValue());

        databaseService.updateRecord(mAppSetting);

        databaseService.createAuditTrail(  "Admin  " +
                        "updated app setting   with id : " + appSetting.getId(),
                AppSetting.class.getSimpleName(),
                ActivityType.UPDATE, 0L, appSetting.getId());

        restResponsePojo.setSuccess(true);

        return  restResponsePojo;

    }
    @ApiOperation(value = "Create App Setting")
    @PostMapping("/admin/api/setting")
    public RestResponsePojo2<Boolean> createAppSetting(@RequestBody AppSetting appSetting
    ) throws SeekFundException, Exception {

        RestResponsePojo2<Boolean>
                restResponsePojo = new RestResponsePojo2<>();



        Assert.isTrue(commonMethod.isValidString(appSetting.getName())," setting name is required ");

        Assert.isTrue(commonMethod.isValidString(appSetting.getValue())," setting value is required ");

        appSetting.setId(null);

        databaseService.saveRecord(appSetting);

        databaseService.createAuditTrail(  "Admin  " +
                        "Created app setting   with id : " + appSetting.getId(),
                AppSetting.class.getSimpleName(),
                ActivityType.INSERT, 0L, appSetting.getId());

        restResponsePojo.setSuccess(true);

        return  restResponsePojo;

    }
}
