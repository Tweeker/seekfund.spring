package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.BankAccountStatus;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserBankAccount;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.projection.AppUserBankAccountPojo;
import com.enov8.seekfund.seekfund.pojo.projection.AppUserPojo;
import com.enov8.seekfund.seekfund.pojo.request.UpdatePaymentDetailsRqPojo;
import com.enov8.seekfund.seekfund.service.CriteriaProjectService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@RestController
@Transactional
public class AdminAppUserPaymentDetailsApi {

    private static final Logger logger = LoggerFactory.getLogger(AdminAppUserPaymentDetailsApi.class);
    @Autowired
    DatabaseService databaseService;

    @Autowired
    AppUserDao appUserService;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CriteriaProjectService criteriaProjectService;

    @Autowired
    CommonMethod commonMethod;

    //TODO GET CURRENT CAMPAIGN
    //TODO ACTIVATE APPUSER CAMPAIGN
    @ApiOperation(value = "get all paymentdetails!")
    @GetMapping("/admin/api/paymentdetails")
    public RestResponsePojo2<DataTableResponsePojo2<AppUserBankAccountPojo>>
        getPaymentDetails(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT + "") Long limit,
           @RequestParam(value =
            Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT + "")
            Long start,
            @RequestParam(value = "status", required = false) BankAccountStatus status,
            @RequestParam(value = "query", required = false) String query) throws
            Exception, SeekFundException {

        RestResponsePojo2<DataTableResponsePojo2<AppUserBankAccountPojo>> restResponsePojo =
                new RestResponsePojo2<>();


        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserBankAccount.class,"b");
        Criteria countCriteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserBankAccount.class,"b");

        criteria.createAlias("b.appUser","appUser");
        countCriteria.createAlias("b.appUser","appUser");

        ProjectionList projectionList = criteriaProjectService.getProjectForPaymentDetails();

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(AppUserBankAccountPojo.class));

        if (commonMethod.isValidString(query)) {
            criteria.add(Restrictions.ilike("appUser.username",
                    "%" + query.trim() + "%"));
            countCriteria.add(Restrictions.ilike("appUser.username",
                    "%" + query.trim() + "%"));
        }
        if (status != null && commonMethod.isValidString(status.getValue())) {
            criteria.add(Restrictions.eq("b.status", status));
            countCriteria.add(Restrictions.eq("b.status",status));
        }

        criteria.setMaxResults(limit.intValue());

        criteria.setFirstResult(start.intValue() - 1);

        criteria.addOrder(Order.desc("b.dateCreated"));


        List<AppUserBankAccountPojo> paymentDetails = criteria.list();


        Long totalResult = (Long) countCriteria.setProjection(Projections.rowCount()).uniqueResult();


        DataTableResponsePojo2<AppUserBankAccountPojo> dtRes = new DataTableResponsePojo2<>();


        dtRes.setData(paymentDetails);

        dtRes.setRecordsTotal(totalResult);


        restResponsePojo.setData(dtRes);


        return restResponsePojo;


    }
    @ApiOperation(value = "update  payment details of  appUser by app user id !")
    @PutMapping("/admin/api/paymentdetails/user/{userId}")
    public RestResponsePojo
    updatePaymentDetailsOfAppuser(@PathVariable("userId") Long userId,
              @RequestBody UpdatePaymentDetailsRqPojo updatePaymentDetailsRq) throws
            Exception, SeekFundException {

        RestResponsePojo restResponsePojo = new RestResponsePojo();

        AppUser appUser = appUserService.getAppUserById(userId);

        if(appUser == null){ throw new SeekFundException("Cannot find user with id " + userId);}

        if(!appUser.getStatus().getValue().equalsIgnoreCase(AppUserStatus.ACTIVE.getValue())){
            throw new SeekFundException("AppUser is currently " + appUser.getStatus());

        }

        String hql  = "select p from  AppUserBankAccount p where p.appUser.id = " + appUser.getId();

        AppUserBankAccount appUserBankAccount  = (AppUserBankAccount)
                databaseService.getUniqueRecordByHql(hql);

        if(appUserBankAccount  == null){ throw new SeekFundException("Cannot find app " +
                "user payment details,probably not created!.");}

        if(updatePaymentDetailsRq.getAccountStatus() != null){

            appUserBankAccount.setStatus(updatePaymentDetailsRq.getAccountStatus());

        }
        if(commonMethod.isValidString(updatePaymentDetailsRq.getAccountName())){

            appUserBankAccount.setAccountName(updatePaymentDetailsRq.getAccountName());
        }

        databaseService.updateRecord(appUserBankAccount);

        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "Updated through admin - appUserBankAccount with id : " + appUserBankAccount.getId(),
                AppUserBankAccount.class.getSimpleName(),
                ActivityType.UPDATE, 0L, appUserBankAccount.getId());


        return  restResponsePojo;





    }
    @ApiOperation(value = "get payment details of  appUser by app user id !")
    @GetMapping("/admin/api/paymentdetails/user/{userId}")
    public RestResponsePojo2<AppUserBankAccountPojo>
    getPaymentDetailsOfUserByUserId(@PathVariable("userId") Long userId) throws
            Exception, SeekFundException {


        RestResponsePojo2<AppUserBankAccountPojo> restResponsePojo = new RestResponsePojo2<>();

        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserBankAccount.class,"b");

        criteria.createAlias("b.appUser","appUser");
        criteria.add(Restrictions.eq("appUser.id", userId));

        ProjectionList projectionList = criteriaProjectService.getProjectForPaymentDetails();

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(AppUserBankAccountPojo.class));


        AppUserBankAccountPojo appUserBankAccountPojo = (AppUserBankAccountPojo) criteria.uniqueResult();

        restResponsePojo.setData(appUserBankAccountPojo);

        return restResponsePojo;


    }




}
