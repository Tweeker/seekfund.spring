package com.enov8.seekfund.seekfund.admin.api;


import com.enov8.seekfund.seekfund.api.DefaultApi;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.CampaignCategory;
import com.enov8.seekfund.seekfund.model.PopularCampaign;
import com.enov8.seekfund.seekfund.pojo.*;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignImagePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.pojo.request.AddPopularCampaignRq;
import com.enov8.seekfund.seekfund.pojo.request.UpdatePopularCampaignRq;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.CriteriaProjectService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@Transactional
public class AdminCampaignApi {
    private static final Logger logger  = LoggerFactory.getLogger(AdminCampaignApi.class);
    @Autowired
    DatabaseService databaseService;


    @Autowired
    CommonMethod commonMethod;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CampaignService campaignService;
    @Autowired
    DefaultApi defaultApi;
    @Autowired
    CriteriaProjectService criteriaProjectService;
    //todo getcampaigns

    //todo promote campaigns

    //approve promote campaigns

    //todo add campaign category
    @ApiOperation(value = "add  campaign Category")
    @PostMapping("/admin/api/campaign/category")
    public RestResponsePojo2<CampaignCategory> addCampaignCategory(
            @RequestBody CampaignCategory campaignCategory,
            @RequestParam(value = "ignoreConflict",
            defaultValue = "false") Boolean ignoreConflict) throws SeekFundException, Exception {

        RestResponsePojo2<CampaignCategory> restResponsePojo = new RestResponsePojo2<>();
        Long  totalResult = 0L;
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(CampaignCategory.class);
        if(!ignoreConflict){
            criteria.add(Restrictions.ilike("name","%"+campaignCategory.getName().trim()+"%"));

            totalResult = (Long)criteria.setProjection(Projections.rowCount()).uniqueResult();

            if(totalResult > 0 ){ throw new
                    SeekFundException(" Already " + totalResult + " Category(s) with similiar name ");}

        }

        campaignCategory.setId(null);

        databaseService.saveRecord(campaignCategory);


        restResponsePojo.setData(campaignCategory);

        return restResponsePojo;


    }

    @ApiOperation(value = "update  campaign Category")
    @PutMapping("/admin/api/campaign/category/{categoryId}")
    public RestResponsePojo2<CampaignCategory> addCampaignCategory(
            @RequestBody CampaignCategory campaignCategory,
            @PathVariable("categoryId") Long categoryId) throws SeekFundException, Exception {

        RestResponsePojo2<CampaignCategory> restResponsePojo = new RestResponsePojo2<>();
        Long  totalResult = 0L;
        CampaignCategory sCategory  = (CampaignCategory)
                databaseService.getRecordById(CampaignCategory.class,categoryId);

        if(sCategory  == null){ throw new Exception("cannot find campaign category in database");}

        if(commonMethod.isValidString(campaignCategory.getDescription())){
            sCategory.setDescription(campaignCategory.getDescription());
        }
        if(commonMethod.isValidString(campaignCategory.getName())){
            sCategory.setName(campaignCategory.getName());
        }

        sCategory.setStatus(campaignCategory.getStatus());


        databaseService.updateRecord(sCategory);

        restResponsePojo.setData(sCategory);

        return restResponsePojo;


    }
    @ApiOperation(value = "get list of  campaign Categories")
    @GetMapping("/admin/api/campaign/category")
    public RestResponsePojo2<List<CampaignCategory>> getCampaignCategories(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
       @RequestParam(value =
               Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
               defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start,
       @RequestParam(value = "query", required = false) String query) throws SeekFundException, Exception{

        RestResponsePojo2<List<CampaignCategory>> restResponsePojo = new RestResponsePojo2<>();

        List<CampaignCategory> categories = new ArrayList<>();

        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(CampaignCategory.class);

        if(commonMethod.isValidString(query)){

            criteria.add(Restrictions.ilike("name","%"+query.trim()+"%"));

        }

        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());


        categories = criteria.list();




        restResponsePojo.setData(categories);


        return restResponsePojo;


    }


    @ApiOperation(value = "get list of  campaigns")
    @GetMapping("/admin/api/campaigns")
    public RestResponsePojo2<DataTableResponsePojo2<CurrentCampaignPojo>>
            getCampaigns(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
                  @RequestParam(value =
                   Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
                   defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"")
                          Long start,
        @RequestParam(value="status", required = false)  CampaignStatus status,
        @RequestParam(value = "query", required = false) String query) throws SeekFundException, Exception {


        RestResponsePojo2<DataTableResponsePojo2<CurrentCampaignPojo>> restResponsePojo2  = new RestResponsePojo2<>();

        // httpServletRequest.getHeaderNames();


        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        criteria.createAlias("c.campaignCategory", "campaignCategory");
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        countCriteria.createAlias("c.campaignCategory", "campaignCategory");
        criteria.addOrder(Order.desc("c.dateCreated"));
        criteria.addOrder(Order.desc("c.name"));
/*        private  String postedByName;
        private Long postedById;*/
        criteria.createAlias("c.appUser", "appUser");
        countCriteria.createAlias("c.appUser", "appUser");

        if(status != null){
            criteria
                    .add(Restrictions.eq("c.status",
                            status
                    ));
            countCriteria
                    .add(Restrictions.eq("c.status",
                            status
                    ));

        }

        if(commonMethod.isValidString(query)){
            criteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));
            countCriteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));

        }

        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("c.id"), "id")
                .add(Projections.property("c.searchCount"), "searchCount")
                .add(Projections.property("c.startDate"), "startDate")
                .add(Projections.property("c.endDate"), "endDate")
                .add(Projections.property("c.description"), "description")
                .add(Projections.property("appUser.username"), "postedByName")
                .add(Projections.property("appUser.id"), "postedById")
                .add(Projections.property("c.campaignImage"), "campaignImage")
                .add(Projections.property("c.dateCreated"), "dateCreated")
                .add(Projections.property("c.amountRaised"), "amountRaised")
                .add(Projections.property("c.campaignCategory"), "campaignCategory")
                .add(Projections.property("c.amount"), "amount")
                .add(Projections.property("c.status"), "status")
                .add(Projections.property("c.name"), "name");

        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));


        List<CurrentCampaignPojo> currentCampaignPojos = (List<CurrentCampaignPojo>) criteria.list();
        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<CurrentCampaignPojo> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(currentCampaignPojos);

        restResponsePojo2.setData(dtRes);







        return restResponsePojo2;

    }


    //TODO update campaign


    @ApiOperation(value = "approve   campaign by campaign id ")
    @PutMapping("/admin/api/campaigns/approve/{campaignId}")
    public RestResponsePojo approveCampaign( @PathVariable("campaignId") Long campaignId) throws Exception, SeekFundException, IllegalArgumentException{

        RestResponsePojo restResponsePojo = new RestResponsePojo();

        AppUserCampaign appUserCampaign = (AppUserCampaign) databaseService.getRecordById(AppUserCampaign.class, campaignId);

        Assert.notNull(appUserCampaign,"Cannot find apppuser campaign");


        Assert.isTrue(appUserCampaign.getStatus().getValue()
                        .equalsIgnoreCase(CampaignStatus.PENDING.getValue()),
                "Campaign is currently  " + appUserCampaign.getStatus()
        );


        appUserCampaign.setStatus(CampaignStatus.RUNNING);

        appUserCampaign.setDateApproved(new Timestamp(System.currentTimeMillis()));

        databaseService.updateRecord(appUserCampaign);


        databaseService.createAuditTrail(  "Admin  " +
                        "approved through campaign  with id : " + appUserCampaign.getId(),
                AppUserCampaign.class.getSimpleName(),
                ActivityType.UPDATE, 0L, appUserCampaign.getId());


        return restResponsePojo;


    }

    @ApiOperation(value = "get campaign by id")
    @GetMapping("/admin/api/campaigns/campaign/{campaignId}")
    public RestResponsePojo2<CurrentCampaignPojo> getCampaignById( @PathVariable("campaignId") Long campaignId) throws Exception, SeekFundException, IllegalArgumentException{

        RestResponsePojo2<CurrentCampaignPojo> restResponsePojo = new RestResponsePojo2();


        CurrentCampaignPojo currentCampaignPojo = campaignService.getCampaignById(campaignId);

        Assert.notNull(currentCampaignPojo,"Cannot find apppuser campaign");



        restResponsePojo.setData(currentCampaignPojo);

        return restResponsePojo;


    }
    @ApiOperation(value = "get campaign images by id")
    @GetMapping("/admin/api/campaigns/images/{campaignId}")
    public RestResponsePojo2<List<CampaignImagePojo>> getCampaignByImagesId(@PathVariable("campaignId") Long campaignId) throws Exception, SeekFundException, IllegalArgumentException{

        RestResponsePojo2<List<CampaignImagePojo>> restResponsePojo = new RestResponsePojo2();

        String hql  = " select c.id as id, " +
                " " +
                " c.status as status , " +
                " c.appUserCampaign.id as campaiginId, " +
                " c.dateCreated as dateCreated," +
                " c.baseUrl as baseUrl, c.fullUrl as fullUrl, c.status as status, " +
                " c.dateCreated as dateCreated from  " +
                " CampaignImage c " +
                " where c.appUserCampaign.id = " + campaignId  + " " +
                "";
           List<CampaignImagePojo> images = (List<CampaignImagePojo> )
                databaseService.getAllRecordsByHql(hql,
                        CampaignImagePojo.class);


        restResponsePojo.setData(images);

        return restResponsePojo;


    }
    @ApiOperation(value = "get list of  campaigns by category id ")
    @GetMapping("/admin/api/campaigns/category/{categoryId}")
    public RestResponsePojo2<DataTableResponsePojo2<CurrentCampaignPojo>>
    getCampaignsByCategory(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
                 @PathVariable("categoryId") Long categoryId,
                 @RequestParam(value =
                         Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
                         defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start,
               @RequestParam(value = "status", required = false) CampaignStatus status,
                 @RequestParam(value = "query", required = false) String query) throws SeekFundException, Exception {


        RestResponsePojo2<DataTableResponsePojo2<CurrentCampaignPojo>> restResponsePojo2  = new RestResponsePojo2<>();

        // httpServletRequest.getHeaderNames();


        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        criteria.createAlias("c.campaignCategory", "campaignCategory");
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(AppUserCampaign.class,"c");
        countCriteria.createAlias("c.campaignCategory", "campaignCategory");
        criteria.addOrder(Order.desc("c.dateCreated"));
        criteria.addOrder(Order.desc("c.name"));


        if(status != null){
            criteria
                    .add(Restrictions.eq("c.status",
                            status
                    ));

            countCriteria
                    .add(Restrictions.eq("c.status",
                            status
                    ));

        }


        if(commonMethod.isValidString(query)){
            criteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));
            countCriteria.add(Restrictions.ilike("c.name","%"+query.trim()+"%"));

        }



        criteria.add(Restrictions.eq("campaignCategory.id",categoryId));
        countCriteria.add(Restrictions.eq("campaignCategory.id",categoryId));


        ProjectionList projectionList = criteriaProjectService.getProjectForSearchCampaign();
        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());
        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CurrentCampaignPojo.class));


        criteria.addOrder(Order.desc("c.dateCreated"));

        List<CurrentCampaignPojo> currentCampaignPojos = (List<CurrentCampaignPojo>) criteria.list();

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<CurrentCampaignPojo> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(currentCampaignPojos);

        restResponsePojo2.setData(dtRes);







        return restResponsePojo2;

    }
    @ApiOperation(value = "add  Popular Campaign")
    @PostMapping("/admin/api/campaign/popular")
    public RestResponsePojo2<Boolean> addPopularCampaign(@RequestBody AddPopularCampaignRq addPopularCampaignRq) throws SeekFundException, Exception{

        RestResponsePojo2<Boolean>  restResponsePojo2 = new RestResponsePojo2<>();

        if(addPopularCampaignRq.getCampaignId()  == null){ throw new SeekFundException("Campaign Cannot be empty");}


        AppUserCampaign appUserCampaign = (AppUserCampaign)databaseService.getRecordById(AppUserCampaign.class,addPopularCampaignRq.getCampaignId());

        if(appUserCampaign == null){ throw new SeekFundException("Cannot find app user campaign ");}

        if(! ( appUserCampaign.getStatus().getValue().equalsIgnoreCase(CampaignStatus.RUNNING.getValue()) ||
                appUserCampaign.getStatus().getValue().equalsIgnoreCase(CampaignStatus.COMPLETED.getValue()))){
            throw new SeekFundException("Campaign is currently  " + appUserCampaign.getStatus());
        }

        if(addPopularCampaignRq.getOrder() == null){
            addPopularCampaignRq.setOrder(0L);
        }

        if(addPopularCampaignRq.getStatus() == null){
            addPopularCampaignRq.setStatus(GenericStatusConstant.ACTIVE);
        }



        campaignService.addPopularCampaign(appUserCampaign,addPopularCampaignRq.getStatus(),addPopularCampaignRq.getOrder());



        restResponsePojo2.setSuccess(true);


        return restResponsePojo2;


    }
    @ApiOperation(value = "update  Popular Campaign by id of category")
    @GetMapping("/admin/api/campaign/popular")
    public RestResponsePojo2<DataTableResponsePojo2<PopularCampaignPojo>> getPopularCampaigns(
            @RequestParam(name = "start" , defaultValue = "1") Integer start,
            @RequestParam(name = "length", defaultValue = "10") Integer length){


        return defaultApi.getPopularCampaigns(start,length);

    }
    @ApiOperation(value = "update  Popular Campaign by id of category")
    @PutMapping("/admin/api/campaign/popular/{id}")
    public RestResponsePojo2<Boolean> updatePopularCampaign(
            @PathVariable("id") Long id,
            @RequestBody UpdatePopularCampaignRq updatePopularCampaignRq) throws SeekFundException, Exception {

        RestResponsePojo2<Boolean> restResponsePojo2 = new RestResponsePojo2<>();


        PopularCampaign popularCampaign  = (PopularCampaign)databaseService.getRecordById(PopularCampaign.class,id);


        Assert.notNull(popularCampaign, "Cannot find popular campaign");


        if(updatePopularCampaignRq.getStatus() != null){

            popularCampaign.setStatus(updatePopularCampaignRq.getStatus());
        }

        if(updatePopularCampaignRq.getOrder() != null){

            popularCampaign.setpOrder(updatePopularCampaignRq.getOrder());
        }


        databaseService.updateRecord(popularCampaign);




        restResponsePojo2.setSuccess(true);
        return restResponsePojo2;


    }
}
