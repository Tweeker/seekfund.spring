package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.Bank;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
@RestController
@Transactional
public class AdminBankApi {

    private static final Logger logger  = LoggerFactory.getLogger(AdminBankApi.class);
    @Autowired
    DatabaseService databaseService;

    @Autowired
    AppUserDao appUserService;

    @Autowired
    CommonMethod commonMethod;

    @Autowired
    SessionFactory sessionFactory;
    //todo add bank

    @ApiOperation(value = "update bank")
    @PutMapping("/admin/api/bank/{bankId}")
    public RestResponsePojo2<Bank> updateBank(@RequestBody Bank bank, @PathVariable("bankId") Long bankId) throws SeekFundException, Exception{

        RestResponsePojo2<Bank> restResponsePojo = new RestResponsePojo2<Bank>();


        Bank sBank  = (Bank) databaseService.getRecordById(Bank.class,bankId);


        if(sBank == null){ throw  new SeekFundException("Bank not available is database");}

        if(commonMethod.isValidString(bank.getName())){
            sBank.setName(bank.getName());
        }

        if(commonMethod.isValidString(bank.getCode())){
            String hql = " select count(b) from Bank b where b.code  = '"+ bank.getCode().trim() +"'";
            Long count  = (Long)databaseService.getUniqueRecordByHql(hql);
            if(count > 0){ throw  new SeekFundException("Bank Code " +  bank.getCode() +" already exist!.");}

            sBank.setCode(bank.getCode());
        }
        if(commonMethod.isValidString(bank.getShortName())){
            sBank.setShortName(bank.getShortName());
        }

        databaseService.updateRecord(sBank);

        restResponsePojo.setData(sBank);


        return restResponsePojo;


    }
    @ApiOperation(value = "add bank , use set query param force to true to prevent api from searching for related entry")
    @PostMapping("/admin/api/bank")
    public RestResponsePojo2<Bank> addBank(@RequestBody Bank bank, @RequestParam(value = "ignoreConflict",
            defaultValue = "false") Boolean ignoreConflict) throws Exception{

        RestResponsePojo2<Bank> restResponsePojo = new RestResponsePojo2<Bank>();
        Long  totalResult = 0L;
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(Bank.class);
        if(!ignoreConflict){
            criteria.add(Restrictions.ilike("name","%"+bank.getName().trim()+"%"));

            totalResult = (Long)criteria.setProjection(Projections.rowCount()).uniqueResult();

            if(totalResult > 0 ){ throw new
                    SeekFundException(" Already " + totalResult + " bank(s) with similiar name ");}

        }

        bank.setId(null);

        databaseService.saveRecord(bank);

        restResponsePojo.setData(bank);


        return restResponsePojo;


    }

    @ApiOperation(value = "get list of banks for application")
    @GetMapping("/admin/api/bank")
    public RestResponsePojo2<List<Bank>>  getBanks() throws Exception{
        RestResponsePojo2<List<Bank>> restResponsePojo = new RestResponsePojo2<List<Bank>>();

        List<Bank> banks  = new ArrayList<>();


        banks = (List<Bank>) databaseService.getAllRecords(Bank.class);

        restResponsePojo.setData(banks);
        return  restResponsePojo;


    }

}
