package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.api.DefaultApi;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppSetting;
import com.enov8.seekfund.seekfund.model.AppUserCampaign;
import com.enov8.seekfund.seekfund.model.CampaignComment;
import com.enov8.seekfund.seekfund.pojo.CampaignCommentPojo;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.request.UpdateCampaignCommentPojo;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.CriteriaProjectService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@Transactional
public class AdminCampaignCommentApi {



    private SimpleDateFormat sdf  = new SimpleDateFormat(Constant.SERVER_DATETIME_FORMAT);
    @Autowired
    DatabaseService databaseService;


    @Autowired
    CommonMethod commonMethod;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CampaignService campaignService;
    @Autowired
    DefaultApi defaultApi;
    @Autowired
    CriteriaProjectService criteriaProjectService;


    @ApiOperation(value = "update campaign  Comment status")
    @PostMapping("/admin/api/comment")
    public RestResponsePojo updateCampaignComment(
            @RequestBody UpdateCampaignCommentPojo updateCampaignCommentPojo) throws IllegalArgumentException,SeekFundException, Exception{

            RestResponsePojo restResponsePojo  = new RestResponsePojo();
        Assert.notNull(updateCampaignCommentPojo.getStatus(), "Please include a status " + GenericStatusConstant.getallEnums());


        CampaignComment campaignComment  = (CampaignComment)
                    databaseService.getRecordById(CampaignComment.class,updateCampaignCommentPojo.getCommentId());

        Assert.notNull(campaignComment,"No comment with id  " + updateCampaignCommentPojo.getCommentId());



        campaignComment.setStatus(updateCampaignCommentPojo.getStatus());

        databaseService.updateRecord(campaignComment);

        databaseService.createAuditTrail(" Admin " +
                        "update comment with id " +
                        campaignComment.getId() + " to  " + updateCampaignCommentPojo.getStatus(),
                AppUserCampaign.class.getSimpleName(),
                ActivityType.UPDATE, 0L, campaignComment.getId());



        return restResponsePojo;


    }

    @ApiOperation(value = "get Comments")
    @GetMapping("/admin/api/comment")
    public RestResponsePojo2<DataTableResponsePojo2<CampaignCommentPojo>> getCampaignComments(
    @RequestParam(value = "dateCreatedTo", required = false) String dateCreatedTo,
    @RequestParam(value = "dateCreatedFrom", required = false) String dateCreatedFrom,
    @RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Integer limit,
@RequestParam(value ="status", required = false) GenericStatusConstant commentStatus,
@RequestParam(value = "campaignId", required = false) Long campaignId,
@RequestParam(value =
Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Integer start
    ) throws SeekFundException, Exception {
        RestResponsePojo2<DataTableResponsePojo2<CampaignCommentPojo>> responsePojo2 = new RestResponsePojo2<>();
        DataTableResponsePojo2<CampaignCommentPojo> dataComments = new DataTableResponsePojo2<>();


        Criteria criteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);
        Criteria countCriteria  = sessionFactory.getCurrentSession()
                .createCriteria(CampaignComment.class);

        criteria.createAlias("commentedBy","a");
        countCriteria.createAlias("commentedBy","a");

        criteria.createAlias("campaign","c");
        countCriteria.createAlias("campaign","c");
        boolean validDates  = commonMethod.isValidDateString(dateCreatedFrom) && commonMethod.isValidDateString(dateCreatedTo);


        if(validDates){


            criteria.add(Restrictions.between("dateCreated",
                    new Timestamp(sdf.parse(dateCreatedFrom).getTime()),
                    new Timestamp(sdf.parse(dateCreatedTo).getTime())));
            countCriteria.add(Restrictions.between("dateCreated",
                    new Timestamp(sdf.parse(dateCreatedFrom).getTime()),
                    new Timestamp(sdf.parse(dateCreatedTo).getTime())));

        }
        if(commentStatus != null){

            criteria.add(Restrictions.eq("status", commentStatus));
            countCriteria.add(Restrictions.eq("status", commentStatus));
        }

        if(campaignId != null){

            criteria.add(Restrictions.eq("c.id", campaignId));
            countCriteria.add(Restrictions.eq("c.id", campaignId));
        }


        ProjectionList projectionList = criteriaProjectService.getCampaignCommentProjection();

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(CampaignCommentPojo.class));



        criteria.setFirstResult( (start-1) * limit);
        criteria.setMaxResults(limit);


        criteria.addOrder(Order.desc("dateCreated"));


        List<CampaignCommentPojo> comments = criteria.list();

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        dataComments.setRecordsTotal(totalResult);
        dataComments.setRecordsFiltered(totalResult);
        dataComments.setData(comments);

        responsePojo2.setData(dataComments);

        return responsePojo2;


    }
}
