package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.enumumeration.ActivityType;
import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;
import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.AppUserType;
import com.enov8.seekfund.seekfund.exception.RegisterException;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.AppUserRole;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.projection.AppUserPojo;
import com.enov8.seekfund.seekfund.pojo.request.AdminRegisterUserPojo;
import com.enov8.seekfund.seekfund.service.AppUserCreationService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

@RestController
@Transactional
public class AdminAppUserApi {

    private static final Logger logger = LoggerFactory.getLogger(AdminAppUserApi.class);
    @Autowired
    DatabaseService databaseService;

    @Autowired
    AppUserDao appUserService;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    CommonMethod commonMethod;
    @Autowired
    AppUserCreationService appUserCreationService;

    @ApiOperation(value = "Create app User!")
    @PostMapping("/admin/api/user")
    public RestResponsePojo2<Boolean> createUser(
            HttpServletRequest httpServletRequest,
            @RequestBody AdminRegisterUserPojo registerUserPojo) throws RegisterException,
            SeekFundException,
            Exception{
        RestResponsePojo2<Boolean>   restResponsePojo2 = new RestResponsePojo2<>();

        if(registerUserPojo.getRole() == null){ throw  new RegisterException("Invalid " +
                "role or no Role, Please pass either ADMIN or USER");}




        boolean isValidPassword = commonMethod.isValidString(registerUserPojo.getPassword())
                && commonMethod.checkPasswordStrength(registerUserPojo.getPassword());
        // && CommonMethod.checkPasswodStrength(password);
        boolean isValidEmail = commonMethod.isValidString(registerUserPojo.getEmail()) &&
                commonMethod.isValidEmail(registerUserPojo.getEmail());

        if(!registerUserPojo.getEmail().
                toLowerCase().trim().
                endsWith(Constant.DEFAULT_DOOMAIN_SUFFIX.toLowerCase().trim())){

            throw  new RegisterException("Invalid Admin user domain");
        }

        boolean isValidUsername = commonMethod.isValidUsername(registerUserPojo.getUsername());

        if (!isValidUsername) {
            throw new RegisterException("Invalid Username!.");
        }

        if (!isValidEmail) {
            throw new RegisterException("Invalid Email!.");
        }

        if (!isValidPassword) {
            throw new RegisterException("Check Password Strength!.");
        }
        logger.info("==========Checking user creation ================");

        logger.info("===============================");

        // AppUser testUserUsername =
        // appUserDao.getAppUserByUsername(username.toLowerCase().trim());

        List<AppUser> testAppUser = null;
        Criteria criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        criteria.add(Restrictions.eq("username", registerUserPojo.getUsername().trim()).ignoreCase());

        testAppUser = criteria.list();

        if(testAppUser != null && !testAppUser.isEmpty()){
            throw new RegisterException("Username Already Exist");
        }
        testAppUser = null;
        criteria  = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        criteria.add(Restrictions.eq("email", registerUserPojo.getEmail().trim()).ignoreCase());

        testAppUser = criteria.list();

        if(testAppUser != null && !testAppUser.isEmpty()){
            throw new RegisterException("Email Already Exist");
        }
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);

        AppUser appUser = new AppUser();

        appUser.setEmail(registerUserPojo.getEmail().trim().toLowerCase());

        appUser.setHashedPassword(commonMethod.hashPassword(registerUserPojo.getPassword().trim()));



        appUser.setUsername(registerUserPojo.getUsername().trim().toLowerCase());

        appUser.setType(AppUserType.USER);
        appUser.setEmailConfirmed(Boolean.FALSE);
        appUser.setStatus(AppUserStatus.AWAITING_EMAIL_CONF);
        if(registerUserPojo.getConfirmEmail()){

            try{
                String encryppt  = appUserCreationService.createActivationEmailQueue(appUser);
                appUser.setHashEntry(encryppt);
            } catch (Exception e){e.printStackTrace();}
        }else{
            appUser.setEmailConfirmed(Boolean.TRUE);
            appUser.setDateEmailConfirmed(new Timestamp(System.currentTimeMillis()));
            appUser.setStatus(AppUserStatus.ACTIVE);
        }

        //TODO send verification email


        AppUserRole role = new AppUserRole();
        role.setName(registerUserPojo.getRole());
        HashSet<AppUserRole> roles = new HashSet<AppUserRole>();
        roles.add(role);
        appUser.setRoles(roles);

        databaseService.saveRecord(appUser);



        logger.info("==========user creation complete ================");
        databaseService.createAuditTrail(registerUserPojo.getUsername().trim() +
                        " Registered via admin api  with Ip : " + remoteIp, AppUser.class.getSimpleName(),
                ActivityType.INSERT, appUser.getId(), appUser.getId());





        return  restResponsePojo2;


    }
    //TODO GET CURRENT CAMPAIGN
    //TODO ACTIVATE APPUSER CAMPAIGN
    @ApiOperation(value = "get all appUsers!")
    @GetMapping("/admin/api/user")
    public RestResponsePojo2<DataTableResponsePojo2<AppUserPojo>> activateUserEmail(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT + "") Long limit,
            @RequestParam(value =
            Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT + "") Long start,
            @RequestParam(value = "query", required = false) String query) throws
            Exception, SeekFundException {

        RestResponsePojo2<DataTableResponsePojo2<AppUserPojo>> restResponsePojo =
                new RestResponsePojo2<>();


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        Criteria countCriteria = sessionFactory.getCurrentSession().createCriteria(AppUser.class);
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("dateEmailConfirmed"), "dateEmailConfirmed")
                .add(Projections.property("emailConfirmed"), "emailConfirmed")
                .add(Projections.property("dateCreated"), "dateCreated")
                .add(Projections.property("dateModified"), "dateModified")
                .add(Projections.property("modifiedBy"), "modifiedBy")
                .add(Projections.property("username"), "username")
                .add(Projections.property("email"), "email")
                .add(Projections.property("status"), "status")
                .add(Projections.property("phoneNumber"), "phoneNumber")
                .add(Projections.property("altPhoneNumber"), "altPhoneNumber")
                .add(Projections.property("lastLoginIP"), "lastLoginIP")
                .add(Projections.property("dateModified"), "dateModified")
                .add(Projections.property("modifiedBy"), "modifiedBy")
                .add(Projections.property("lastLoginDate"), "lastLoginDate")
                .add(Projections.property("passwordResetExpireDate"), "passwordResetExpireDate")
                .add(Projections.property("loginCounter"), "loginCounter")
                .add(Projections.property("loginFailCounter"), "loginFailCounter")
                .add(Projections.property("logoutDate"), "logoutDate");

        criteria.setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(AppUserPojo.class));

        if (commonMethod.isValidString(query)) {
            criteria.add(Restrictions.ilike("username", "%" + query.trim() + "%"));
            countCriteria.add(Restrictions.ilike("username", "%" + query.trim() + "%"));
        }


        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());

        List<AppUserPojo> appUserPojos = criteria.list();


        Long totalResult = (Long) countCriteria.setProjection(Projections.rowCount()).uniqueResult();


        DataTableResponsePojo2<AppUserPojo> dtRes = new DataTableResponsePojo2<>();


        dtRes.setData(appUserPojos);

        dtRes.setRecordsTotal(totalResult);


        restResponsePojo.setData(dtRes);


        return restResponsePojo;


    }

    @ApiOperation(value = "activate appUser that hasn't confirmed email!")
    @PutMapping("/admin/api/user/{userId}")
    public RestResponsePojo activateUserEmail(@PathVariable("userId") Long userId) throws
            Exception, SeekFundException {


        RestResponsePojo restResponsePojo = new RestResponsePojo();
/*
        if (!commonMethod.isValidUsername(userId)) {

            throw new SeekFundException("Invalid username!.");
        }*/

        AppUser appUser = appUserService.getAppUserById(userId);

        if (appUser == null) {
            throw new SeekFundException("Cannot find AppUser");
        }

        if (!appUser.getStatus().getValue().equalsIgnoreCase(AppUserStatus.AWAITING_EMAIL_CONF.getValue())) {

            throw new SeekFundException("Current status of " + appUser.getUsername() + " is " + appUser.getStatus());
        }

        appUser.setStatus(AppUserStatus.ACTIVE);

        appUser.setDateEmailConfirmed(new Timestamp(System.currentTimeMillis()));

        appUser.setEmailConfirmed(true);


        databaseService.updateRecord(appUser);


        databaseService.createAuditTrail(appUser.getUsername().trim() + " " +
                        "Activated through admin - appUser with id : " + appUser.getId(),
                AppUser.class.getSimpleName(),
                ActivityType.UPDATE, 0L, appUser.getId());

        return restResponsePojo;


    }




}
