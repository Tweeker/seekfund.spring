package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.api.DefaultApi;
import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.CampaignCategory;
import com.enov8.seekfund.seekfund.model.CampaignDonation;
import com.enov8.seekfund.seekfund.pojo.DataTableResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.projection.CampaignDonationPojo;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.CriteriaProjectService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@Transactional
public class AdminCampaignDonationApi {

    private SimpleDateFormat sdf  = new SimpleDateFormat(Constant.SERVER_DATETIME_FORMAT);
    @Autowired
    DatabaseService databaseService;


    @Autowired
    CommonMethod commonMethod;

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    CampaignService campaignService;
    @Autowired
    DefaultApi defaultApi;
    @Autowired
    CriteriaProjectService criteriaProjectService;
    @ApiOperation(value = "get campaign Donation")
    @GetMapping("/admin/api/campaigndonation")
    public RestResponsePojo2<DataTableResponsePojo2<CampaignDonationPojo>> getCampaignDonation(@RequestParam(value
            = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT,
            defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_LIMIT_DEFAULT+"") Long limit,
                     @RequestParam(value =
               Constant.DATATABLE_REQUEST_PARAMETER_PAGE,
               defaultValue = Constant.DATATABLE_REQUEST_PARAMETER_PAGE_DEFAULT+"") Long start,
            @RequestParam(value = "status", required = false) PaymentStatus status,
           @RequestParam(value = "campaignId", required = false) Long  campaignId,
                @RequestParam(value = "dateCreatedTo", required = false) String dateCreatedTo,
           @RequestParam(value = "dateCreatedFrom", required = false) String dateCreatedFrom
            ) throws SeekFundException, Exception {

        RestResponsePojo2<DataTableResponsePojo2<CampaignDonationPojo>>
                restResponsePojo = new RestResponsePojo2<>();

        boolean validDates  = commonMethod.isValidDateString(dateCreatedFrom) && commonMethod.isValidDateString(dateCreatedTo);
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class,"cd");
        Criteria countCriteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class,"cd");

                criteria
                .createAlias("cd.donatedBy", "donatedBy",JoinType.LEFT_OUTER_JOIN);
        countCriteria
                .createAlias("cd.donatedBy", "donatedBy",JoinType.LEFT_OUTER_JOIN);


        criteria
                .createAlias("cd.campaign", "campaign");
        countCriteria
                .createAlias("cd.campaign", "campaign");

        if(status != null){

            criteria.add(Restrictions.eq("cd.paymentStatus",status));
            countCriteria.add(Restrictions.eq("cd.paymentStatus",status));
        }

        if(campaignId != null){

            criteria.add(Restrictions.eq("cd.campaign.id",campaignId));
            countCriteria.add(Restrictions.eq("cd.campaign.id",campaignId));

        }

        if(validDates){


           criteria.add(Restrictions.between("cd.dateCreated",
                    new Timestamp(sdf.parse(dateCreatedFrom).getTime()), new Timestamp(sdf.parse(dateCreatedTo).getTime())));
            countCriteria.add(Restrictions.between("cd.dateCreated",
                    new Timestamp(sdf.parse(dateCreatedFrom).getTime()), new Timestamp(sdf.parse(dateCreatedTo).getTime())));

        }

        ProjectionList projectionList = Projections


                .projectionList()
                .add(Projections.property("cd.id"), "id")
                .add(Projections.property("campaign.id"), "campaignId")
                .add(Projections.property("cd.dateCompleted"), "dateCompleted")
                .add(Projections.property("cd.dateCreated"), "dateCreated")
                .add(Projections.property("cd.comment"), "comment")
                .add(Projections.property("cd.paymentRef"), "paymentReference")
                .add(Projections.property("cd.amountRaised"), "amountRaised")
                .add(Projections.property("cd.amount"), "amount")
                .add(Projections.property("cd.paymentStatus"), "paymentStatus")
                .add(Projections.property("donatedBy.id"),"donatedById")
                .add(Projections.property("donatedBy.username"),"donatedByUsername");



        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(CampaignDonationPojo.class));
        criteria.setFirstResult((start.intValue()-1) * limit.intValue());
        criteria.setMaxResults(limit.intValue());

        criteria.addOrder(Order.desc("cd.dateCompleted"));
        criteria.addOrder(Order.desc("cd.amountRaised"));


        List<CampaignDonationPojo> campaignDonations = (List<CampaignDonationPojo>)  criteria.list();

        Long totalResult = (Long)countCriteria.setProjection(Projections.rowCount()).uniqueResult();

        DataTableResponsePojo2<CampaignDonationPojo> dtRes  = new DataTableResponsePojo2<>();

        dtRes.setRecordsTotal(totalResult);
        dtRes.setData(campaignDonations);

        restResponsePojo.setData(dtRes);


        return  restResponsePojo;

    }
    @GetMapping("/admin/api/campaigndonation/reference/{paymentRef}")
    public RestResponsePojo2<CampaignDonationPojo> getCampaignDonation(@PathVariable("paymentRef") String paymentReference
    ) throws SeekFundException, Exception {

        RestResponsePojo2<CampaignDonationPojo>
                restResponsePojo = new RestResponsePojo2<>();


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class,"cd");
        Criteria countCriteria = sessionFactory.getCurrentSession().createCriteria(CampaignDonation.class,"cd");

        criteria
                .createAlias("cd.donatedBy", "donatedBy",JoinType.LEFT_OUTER_JOIN);


        criteria
                .createAlias("cd.campaign", "campaign");



        if(paymentReference != null){
            criteria.add(Restrictions.eq("cd.paymentRef", paymentReference.trim()));

        }



        ProjectionList projectionList = Projections


                .projectionList()
                .add(Projections.property("cd.id"), "id")
                .add(Projections.property("campaign.id"), "campaignId")
                .add(Projections.property("cd.dateCompleted"), "dateCompleted")
                .add(Projections.property("cd.dateCreated"), "dateCreated")
                .add(Projections.property("cd.comment"), "comment")
                .add(Projections.property("cd.paymentRef"), "paymentReference")
                .add(Projections.property("cd.amountRaised"), "amountRaised")
                .add(Projections.property("cd.amount"), "amount")
                .add(Projections.property("cd.paymentStatus"), "paymentStatus")
                .add(Projections.property("donatedBy.id"),"donatedById")
                .add(Projections.property("donatedBy.username"),"donatedByUsername");



        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(CampaignDonationPojo.class));




        CampaignDonationPojo campaignDonation = (CampaignDonationPojo) criteria.uniqueResult();




        restResponsePojo.setData(campaignDonation);


        return  restResponsePojo;

    }

}
