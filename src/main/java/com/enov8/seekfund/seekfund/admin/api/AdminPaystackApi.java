package com.enov8.seekfund.seekfund.admin.api;

import com.enov8.seekfund.seekfund.model.CampaignCategory;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo2;
import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.ListTransactionResp;
import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.QueryString;
import com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber.ResolveAccountNumberResp;
import com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn.ResolveBvnResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontimeline.TransactionTimelineResp;
import com.enov8.seekfund.seekfund.pojo.paystack.transactiontotal.TransactionTotalResp;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;
import com.enov8.seekfund.seekfund.service.PaystackService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

@RestController
public class AdminPaystackApi {
    @Autowired
    PaystackService paystackService;
    @Autowired
    CommonMethod commonMethod;


    @ApiOperation(value = "get transaction from paystack by reference")
    @GetMapping("/admin/api/paystack/transaction/verify")
    public RestResponsePojo2<VerifyTransactionResp> verifyTransaction(
            @RequestParam(value = "reference", required = true) String reference) throws Exception{
        RestResponsePojo2<VerifyTransactionResp> resp  = new RestResponsePojo2<>();


        VerifyTransactionResp r  =   paystackService.verifyPayment(reference);

        resp.setData(r);




        return resp;



    }

    @ApiOperation(value = "resolve account number from paystack")
    @GetMapping("/admin/api/paystack/bank/resolve")
    public RestResponsePojo2<ResolveAccountNumberResp> resolveAccountNumber(
            @RequestParam(value = "accountNumber", required = true) String accountNumber,
            @RequestParam(value = "bankCode", required = true) String bankCode) throws Exception{
        RestResponsePojo2<ResolveAccountNumberResp> resp  = new RestResponsePojo2<>();


        Assert.isTrue(accountNumber.matches(Constant.REGREX_ACC_NUMBER),
                "Invalid account number pattern");

        Assert.isTrue(bankCode.matches(Constant.REGREX_BANK_CODE),
                "Invalid bank code  pattern");
        ResolveAccountNumberResp r  =   paystackService
                .resolveAcountNumber(accountNumber,bankCode);

        resp.setData(r);




        return resp;



    }
    @ApiOperation(value = "resolve bvn from paystack")
    @GetMapping("/admin/api/paystack/bank/resolve_bvn/{bvn}")
    public RestResponsePojo2<ResolveBvnResp> resolveBvn(@PathVariable("bvn") String bvn) throws Exception{
        RestResponsePojo2<ResolveBvnResp> resp  = new RestResponsePojo2<>();


        Assert.isTrue(bvn.length() > 3,
                " Invalid Bvn , Please check BVN");

        ResolveBvnResp r  =   paystackService.resolveBvn(bvn);

        resp.setData(r);

        return resp;



    }

    @ApiOperation(value = "list transactions from paystack")
    @GetMapping("/admin/api/paystack/transaction")
public RestResponsePojo2<ListTransactionResp> listTransaction(
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "toDate", required = false) String toDate,
        @RequestParam(value = "fromDate", required = false) String fromDate,
        @RequestParam(value = "perPage", required = false) Long perPage,
        @RequestParam(value = "page", required = false) Long page,
        @RequestParam(value = "customer", required = false, defaultValue = "0") Long customer,
        @RequestParam(value = "amountInKobo", required = false) Long amountInKobo) throws Exception{
        RestResponsePojo2<ListTransactionResp> resp  = new RestResponsePojo2<>();


        QueryString queryString = new QueryString( perPage,  page,  customer,
                 status,  fromDate,  toDate,  amountInKobo);





        ListTransactionResp r  =   paystackService.listTransactions(queryString);

        resp.setData(r);




        return resp;



    }


    @ApiOperation(value = "get transactions total paystack")
    @GetMapping("/admin/api/paystack/transaction/totals")
    public RestResponsePojo2<TransactionTotalResp> getTransactionTotals(

            @RequestParam(value = "toDate", required = false) String toDate,
            @RequestParam(value = "fromDate", required = false) String fromDate) throws Exception{
        RestResponsePojo2<TransactionTotalResp> resp  = new RestResponsePojo2<>();


        boolean validDateRange  = commonMethod.isValidString(toDate) && commonMethod.isValidString(fromDate);

        TransactionTotalResp r = null;

        if(validDateRange){
            r = paystackService.transactionTotal(toDate,fromDate);
        }else{
            r = paystackService.transactionTotal();
        }




        resp.setData(r);




        return resp;



    }

    @ApiOperation(value = "get transaction timeline from id")
    @GetMapping("/admin/api/paystack/transaction/timeline/{id}")
    public RestResponsePojo2<TransactionTimelineResp> getTransactionTimeline(@PathVariable("id") Long id) throws Exception{
        RestResponsePojo2<TransactionTimelineResp> resp  = new RestResponsePojo2<>();


       TransactionTimelineResp r = null;


       r = paystackService.transactionTimeline(id);


        resp.setData(r);




        return resp;



    }
}
