package com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Meta{

	@JsonProperty("free_calls_left")
	private int freeCallsLeft;

	@JsonProperty("calls_this_month")
	private int callsThisMonth;

	public void setFreeCallsLeft(int freeCallsLeft){
		this.freeCallsLeft = freeCallsLeft;
	}

	public int getFreeCallsLeft(){
		return freeCallsLeft;
	}

	public void setCallsThisMonth(int callsThisMonth){
		this.callsThisMonth = callsThisMonth;
	}

	public int getCallsThisMonth(){
		return callsThisMonth;
	}

	@Override
 	public String toString(){
		return 
			"Meta{" + 
			"free_calls_left = '" + freeCallsLeft + '\'' + 
			",calls_this_month = '" + callsThisMonth + '\'' + 
			"}";
		}
}