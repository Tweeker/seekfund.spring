package com.enov8.seekfund.seekfund.pojo.projection;

import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.model.CampaignCategory;
import com.enov8.seekfund.seekfund.model.CampaignImage;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrentCampaignPojo implements Serializable {


    private Long id;

    private String name;

    private Timestamp startDate;

    private Timestamp endDate;

    private Long searchCount ;

    private String campaignImage;


    private  String postedByName;
    private Long postedById;

    private CampaignStatus status ;
    private BigDecimal amountRaised = new BigDecimal("0");
    private String description;
    private  Set<CampaignImage> images;

    public Set<CampaignImage> getImages() {
        return images;
    }

    public void setImages(Set<CampaignImage> images) {
        this.images = images;
    }

    private CampaignCategory campaignCategory;
   // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.ADMIN_DATETIME_FORMAT)
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());

    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CampaignCategory getCampaignCategory() {
        return campaignCategory;
    }

    public void setCampaignCategory(CampaignCategory campaignCategory) {
        this.campaignCategory = campaignCategory;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountRaised() {
        return amountRaised;
    }

    public void setAmountRaised(BigDecimal amountRaised) {
        this.amountRaised = amountRaised;
    }

    public Long getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Long searchCount) {
        this.searchCount = searchCount;
    }

    public String getCampaignImage() {
        return campaignImage;
    }

    public void setCampaignImage(String campaignImage) {
        this.campaignImage = campaignImage;
    }

    public String getPostedByName() {
        return postedByName;
    }

    public void setPostedByName(String postedByName) {
        this.postedByName = postedByName;
    }

    public Long getPostedById() {
        return postedById;
    }

    public void setPostedById(Long postedById) {
        this.postedById = postedById;
    }
}
