package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class Log{

	@JsonProperty("start_time")
	private int startTime;

	@JsonProperty("input")
	private List<Object> input;

	@JsonProperty("success")
	private boolean success;

	@JsonProperty("mobile")
	private boolean mobile;

	@JsonProperty("history")
	private List<HistoryItem> history;

	@JsonProperty("errors")
	private int errors;

	@JsonProperty("time_spent")
	private int timeSpent;

	@JsonProperty("attempts")
	private int attempts;

	@JsonProperty("authentication")
	private Object authentication;

	public void setStartTime(int startTime){
		this.startTime = startTime;
	}

	public int getStartTime(){
		return startTime;
	}

	public void setInput(List<Object> input){
		this.input = input;
	}

	public List<Object> getInput(){
		return input;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMobile(boolean mobile){
		this.mobile = mobile;
	}

	public boolean isMobile(){
		return mobile;
	}

	public void setHistory(List<HistoryItem> history){
		this.history = history;
	}

	public List<HistoryItem> getHistory(){
		return history;
	}

	public void setErrors(int errors){
		this.errors = errors;
	}

	public int getErrors(){
		return errors;
	}

	public void setTimeSpent(int timeSpent){
		this.timeSpent = timeSpent;
	}

	public int getTimeSpent(){
		return timeSpent;
	}

	public void setAttempts(int attempts){
		this.attempts = attempts;
	}

	public int getAttempts(){
		return attempts;
	}

	public void setAuthentication(Object authentication){
		this.authentication = authentication;
	}

	public Object getAuthentication(){
		return authentication;
	}

	@Override
 	public String toString(){
		return 
			"Log{" + 
			"start_time = '" + startTime + '\'' + 
			",input = '" + input + '\'' + 
			",success = '" + success + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",history = '" + history + '\'' + 
			",errors = '" + errors + '\'' + 
			",time_spent = '" + timeSpent + '\'' + 
			",attempts = '" + attempts + '\'' + 
			",authentication = '" + authentication + '\'' + 
			"}";
		}
}