package com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@JsonProperty("dob")
	private String dob;

	@JsonProperty("mobile")
	private String mobile;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("bvn")
	private String bvn;

	@JsonProperty("formatted_dob")
	private String formattedDob;

	@JsonProperty("first_name")
	private String firstName;

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setBvn(String bvn){
		this.bvn = bvn;
	}

	public String getBvn(){
		return bvn;
	}

	public void setFormattedDob(String formattedDob){
		this.formattedDob = formattedDob;
	}

	public String getFormattedDob(){
		return formattedDob;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"dob = '" + dob + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",bvn = '" + bvn + '\'' + 
			",formatted_dob = '" + formattedDob + '\'' + 
			",first_name = '" + firstName + '\'' + 
			"}";
		}
}