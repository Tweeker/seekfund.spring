package com.enov8.seekfund.seekfund.pojo.paystack.verifypayment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
 class CustomField
{
    @JsonProperty("display_name")
    private String displayName;

    public String getDisplayName() { return this.displayName; }

    public void setDisplayName(String display_name) { this.displayName = displayName; }
    @JsonProperty("variable_name")
    private String variableName;

    public String getVariableName() { return this.variableName; }

    public void setVariableName(String variable_name) { this.variableName = variableName; }

    private String value;

    public String getValue() { return this.value; }

    public void setValue(String value) { this.value = value; }
}
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
 class Metadata
{
    @JsonProperty("custom_fields")
    private ArrayList<CustomField> customFields;

    public ArrayList<CustomField> getCustomFields() { return this.customFields; }

    public void setCustomFields(ArrayList<CustomField> customFields) { this.customFields = customFields; }

    private String referrer;

    public String getReferrer() { return this.referrer; }

    public void setReferrer(String referrer) { this.referrer = referrer; }
}
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
 class History
{
    private String type;

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private int time;

    public int getTime() { return this.time; }

    public void setTime(int time) { this.time = time; }
}
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
 class Log
{
    @JsonProperty("start_time")
    private int startTime;

    public int getStartTime() { return this.startTime; }

    public void setStartTime(int startTime) { this.startTime = startTime; }
    @JsonProperty("time_spent")
    private int timeSpent;

    public int getTimeSpent() { return this.timeSpent; }

    public void setTimeSpent(int time_spent) { this.timeSpent = time_spent; }

    private int attempts;

    public int getAttempts() { return this.attempts; }

    public void setAttempts(int attempts) { this.attempts = attempts; }

    private Object authentication;

    public Object getAuthentication() { return this.authentication; }

    public void setAuthentication(Object authentication) { this.authentication = authentication; }

    private int errors;

    public int getErrors() { return this.errors; }

    public void setErrors(int errors) { this.errors = errors; }

    private boolean success;

    public boolean getSuccess() { return this.success; }

    public void setSuccess(boolean success) { this.success = success; }

    private boolean mobile;

    public boolean getMobile() { return this.mobile; }

    public void setMobile(boolean mobile) { this.mobile = mobile; }

    private ArrayList<Object> input;

    public ArrayList<Object> getInput() { return this.input; }

    public void setInput(ArrayList<Object> input) { this.input = input; }

    private ArrayList<History> history;

    public ArrayList<History> getHistory() { return this.history; }

    public void setHistory(ArrayList<History> history) { this.history = history; }
}



@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VerifyTransactionResp
{
    private boolean status;

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private Data data;

    public Data getData() { return this.data; }

    public void setData(Data data) { this.data = data; }
}