package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class HistoryItem{

	@JsonProperty("time")
	private int time;

	@JsonProperty("type")
	private String type;

	@JsonProperty("message")
	private String message;

	public void setTime(int time){
		this.time = time;
	}

	public int getTime(){
		return time;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"HistoryItem{" + 
			"time = '" + time + '\'' + 
			",type = '" + type + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}