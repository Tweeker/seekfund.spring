package com.enov8.seekfund.seekfund.pojo.projection;

import com.enov8.seekfund.seekfund.enumumeration.AppUserStatus;
import com.enov8.seekfund.seekfund.enumumeration.AppUserType;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.sql.Timestamp;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppUserPojo implements Serializable {

    private Long id;
    private String username;
    private Timestamp dateEmailConfirmed;
    private Boolean emailConfirmed;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.ADMIN_DATETIME_FORMAT)
    private Timestamp dateCreated = new Timestamp(System.currentTimeMillis());
   // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.ADMIN_DATETIME_FORMAT)
    private Timestamp dateModified;

    private Long modifiedBy;

    private String hashedPassword;

    private String email;

    private String phoneNumber;

    private String altPhoneNumber;

    private String lastLoginIP;
    private Timestamp lastLoginDate;
    private Integer noOfTimesPaid = 0;

    private AppUserStatus status;



    private AppUserType type;

    private Timestamp logoutDate;


    public Timestamp getDateEmailConfirmed() {
        return dateEmailConfirmed;
    }

    public void setDateEmailConfirmed(Timestamp dateEmailConfirmed) {
        this.dateEmailConfirmed = dateEmailConfirmed;
    }

    public Boolean getEmailConfirmed() {
        return emailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAltPhoneNumber() {
        return altPhoneNumber;
    }

    public void setAltPhoneNumber(String altPhoneNumber) {
        this.altPhoneNumber = altPhoneNumber;
    }

    public String getLastLoginIP() {
        return lastLoginIP;
    }

    public void setLastLoginIP(String lastLoginIP) {
        this.lastLoginIP = lastLoginIP;
    }

    public Timestamp getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Timestamp lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getNoOfTimesPaid() {
        return noOfTimesPaid;
    }

    public void setNoOfTimesPaid(Integer noOfTimesPaid) {
        this.noOfTimesPaid = noOfTimesPaid;
    }

    public AppUserStatus getStatus() {
        return status;
    }

    public void setStatus(AppUserStatus status) {
        this.status = status;
    }

    public AppUserType getType() {
        return type;
    }

    public void setType(AppUserType type) {
        this.type = type;
    }

    public Timestamp getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(Timestamp logoutDate) {
        this.logoutDate = logoutDate;
    }

    public Timestamp getPasswordResetExpireDate() {
        return passwordResetExpireDate;
    }

    public void setPasswordResetExpireDate(Timestamp passwordResetExpireDate) {
        this.passwordResetExpireDate = passwordResetExpireDate;
    }

    public String getResetPassswordToken() {
        return resetPassswordToken;
    }

    public void setResetPassswordToken(String resetPassswordToken) {
        this.resetPassswordToken = resetPassswordToken;
    }

    public String getResetPassswordTokenAlt() {
        return resetPassswordTokenAlt;
    }

    public void setResetPassswordTokenAlt(String resetPassswordTokenAlt) {
        this.resetPassswordTokenAlt = resetPassswordTokenAlt;
    }

    public Integer getLoginCounter() {
        return loginCounter;
    }

    public void setLoginCounter(Integer loginCounter) {
        this.loginCounter = loginCounter;
    }

    public Integer getLoginFailCounter() {
        return loginFailCounter;
    }

    public void setLoginFailCounter(Integer loginFailCounter) {
        this.loginFailCounter = loginFailCounter;
    }

    private Timestamp passwordResetExpireDate;

    private String  resetPassswordToken;

    private String resetPassswordTokenAlt;

    private Integer loginCounter = 0;

    private Integer loginFailCounter = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
