package com.enov8.seekfund.seekfund.pojo.twitter.verifycredentials;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Place{

	@JsonProperty("country")
	private String country;

	@JsonProperty("country_code")
	private String countryCode;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("bounding_box")
	private BoundingBox boundingBox;

	@JsonProperty("place_type")
	private String placeType;

	@JsonProperty("name")
	private String name;

	@JsonProperty("attributes")
	private Attributes attributes;

	@JsonProperty("id")
	private String id;

	@JsonProperty("url")
	private String url;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public String getFullName(){
		return fullName;
	}

	public void setBoundingBox(BoundingBox boundingBox){
		this.boundingBox = boundingBox;
	}

	public BoundingBox getBoundingBox(){
		return boundingBox;
	}

	public void setPlaceType(String placeType){
		this.placeType = placeType;
	}

	public String getPlaceType(){
		return placeType;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAttributes(Attributes attributes){
		this.attributes = attributes;
	}

	public Attributes getAttributes(){
		return attributes;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"Place{" + 
			"country = '" + country + '\'' + 
			",country_code = '" + countryCode + '\'' + 
			",full_name = '" + fullName + '\'' + 
			",bounding_box = '" + boundingBox + '\'' + 
			",place_type = '" + placeType + '\'' + 
			",name = '" + name + '\'' + 
			",attributes = '" + attributes + '\'' + 
			",id = '" + id + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}