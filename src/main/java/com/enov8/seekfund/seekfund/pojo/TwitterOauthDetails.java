package com.enov8.seekfund.seekfund.pojo;

import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.TwitterProfile;

public class TwitterOauthDetails {

    private OAuthToken authToken;
    private TwitterProfile profile;

    public TwitterOauthDetails(OAuthToken authToken, TwitterProfile profile) {
        this.authToken = authToken;
        this.profile = profile;
    }

    public TwitterOauthDetails() {
    }

    public OAuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(OAuthToken authToken) {
        this.authToken = authToken;
    }

    public TwitterProfile getProfile() {
        return profile;
    }

    public void setProfile(TwitterProfile profile) {
        this.profile = profile;
    }
}
