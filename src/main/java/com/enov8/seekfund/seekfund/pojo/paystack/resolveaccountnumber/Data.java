package com.enov8.seekfund.seekfund.pojo.paystack.resolveaccountnumber;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data{

	@JsonProperty("account_number")
	private String accountNumber;

	@JsonProperty("account_name")
	private String accountName;

	public void setAccountNumber(String accountNumber){
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber(){
		return accountNumber;
	}

	public void setAccountName(String accountName){
		this.accountName = accountName;
	}

	public String getAccountName(){
		return accountName;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"account_number = '" + accountNumber + '\'' + 
			",account_name = '" + accountName + '\'' + 
			"}";
		}
}