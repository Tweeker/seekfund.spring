package com.enov8.seekfund.seekfund.pojo.twitter.verifycredentials;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VerifyCredentials{

	@JsonProperty("utc_offset")
	private int utcOffset;

	@JsonProperty("friends_count")
	private int friendsCount;

	@JsonProperty("profile_image_url_https")
	private String profileImageUrlHttps;

	@JsonProperty("listed_count")
	private int listedCount;

	@JsonProperty("profile_background_image_url")
	private String profileBackgroundImageUrl;

	@JsonProperty("default_profile_image")
	private boolean defaultProfileImage;

	@JsonProperty("favourites_count")
	private int favouritesCount;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("description")
	private String description;

	@JsonProperty("is_translator")
	private boolean isTranslator;

	@JsonProperty("profile_background_image_url_https")
	private String profileBackgroundImageUrlHttps;

	@JsonProperty("protected")
	private boolean jsonMemberProtected;

	@JsonProperty("screen_name")
	private String screenName;

	@JsonProperty("id_str")
	private String idStr;

	@JsonProperty("profile_link_color")
	private String profileLinkColor;

	@JsonProperty("show_all_inline_media")
	private boolean showAllInlineMedia;

	@JsonProperty("geo_enabled")
	private boolean geoEnabled;

	@JsonProperty("id")
	private int id;

	@JsonProperty("profile_background_color")
	private String profileBackgroundColor;

	@JsonProperty("lang")
	private String lang;

	@JsonProperty("profile_sidebar_border_color")
	private String profileSidebarBorderColor;

	@JsonProperty("profile_text_color")
	private String profileTextColor;

	@JsonProperty("verified")
	private boolean verified;

	@JsonProperty("profile_image_url")
	private String profileImageUrl;

	@JsonProperty("time_zone")
	private String timeZone;

	@JsonProperty("contributors_enabled")
	private boolean contributorsEnabled;

	@JsonProperty("url")
	private Object url;

	@JsonProperty("profile_background_tile")
	private boolean profileBackgroundTile;

	@JsonProperty("follow_request_sent")
	private Object followRequestSent;

	@JsonProperty("statuses_count")
	private int statusesCount;

	@JsonProperty("default_profile")
	private boolean defaultProfile;

	@JsonProperty("followers_count")
	private int followersCount;

	@JsonProperty("following")
	private Object following;

	@JsonProperty("profile_use_background_image")
	private boolean profileUseBackgroundImage;

	@JsonProperty("name")
	private String name;

	@JsonProperty("location")
	private String location;

	@JsonProperty("profile_sidebar_fill_color")
	private String profileSidebarFillColor;

	@JsonProperty("notifications")
	private Object notifications;

	@JsonProperty("status")
	private Status status;

	public void setUtcOffset(int utcOffset){
		this.utcOffset = utcOffset;
	}

	public int getUtcOffset(){
		return utcOffset;
	}

	public void setFriendsCount(int friendsCount){
		this.friendsCount = friendsCount;
	}

	public int getFriendsCount(){
		return friendsCount;
	}

	public void setProfileImageUrlHttps(String profileImageUrlHttps){
		this.profileImageUrlHttps = profileImageUrlHttps;
	}

	public String getProfileImageUrlHttps(){
		return profileImageUrlHttps;
	}

	public void setListedCount(int listedCount){
		this.listedCount = listedCount;
	}

	public int getListedCount(){
		return listedCount;
	}

	public void setProfileBackgroundImageUrl(String profileBackgroundImageUrl){
		this.profileBackgroundImageUrl = profileBackgroundImageUrl;
	}

	public String getProfileBackgroundImageUrl(){
		return profileBackgroundImageUrl;
	}

	public void setDefaultProfileImage(boolean defaultProfileImage){
		this.defaultProfileImage = defaultProfileImage;
	}

	public boolean isDefaultProfileImage(){
		return defaultProfileImage;
	}

	public void setFavouritesCount(int favouritesCount){
		this.favouritesCount = favouritesCount;
	}

	public int getFavouritesCount(){
		return favouritesCount;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setIsTranslator(boolean isTranslator){
		this.isTranslator = isTranslator;
	}

	public boolean isIsTranslator(){
		return isTranslator;
	}

	public void setProfileBackgroundImageUrlHttps(String profileBackgroundImageUrlHttps){
		this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
	}

	public String getProfileBackgroundImageUrlHttps(){
		return profileBackgroundImageUrlHttps;
	}

	public void setJsonMemberProtected(boolean jsonMemberProtected){
		this.jsonMemberProtected = jsonMemberProtected;
	}

	public boolean isJsonMemberProtected(){
		return jsonMemberProtected;
	}

	public void setScreenName(String screenName){
		this.screenName = screenName;
	}

	public String getScreenName(){
		return screenName;
	}

	public void setIdStr(String idStr){
		this.idStr = idStr;
	}

	public String getIdStr(){
		return idStr;
	}

	public void setProfileLinkColor(String profileLinkColor){
		this.profileLinkColor = profileLinkColor;
	}

	public String getProfileLinkColor(){
		return profileLinkColor;
	}

	public void setShowAllInlineMedia(boolean showAllInlineMedia){
		this.showAllInlineMedia = showAllInlineMedia;
	}

	public boolean isShowAllInlineMedia(){
		return showAllInlineMedia;
	}

	public void setGeoEnabled(boolean geoEnabled){
		this.geoEnabled = geoEnabled;
	}

	public boolean isGeoEnabled(){
		return geoEnabled;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProfileBackgroundColor(String profileBackgroundColor){
		this.profileBackgroundColor = profileBackgroundColor;
	}

	public String getProfileBackgroundColor(){
		return profileBackgroundColor;
	}

	public void setLang(String lang){
		this.lang = lang;
	}

	public String getLang(){
		return lang;
	}

	public void setProfileSidebarBorderColor(String profileSidebarBorderColor){
		this.profileSidebarBorderColor = profileSidebarBorderColor;
	}

	public String getProfileSidebarBorderColor(){
		return profileSidebarBorderColor;
	}

	public void setProfileTextColor(String profileTextColor){
		this.profileTextColor = profileTextColor;
	}

	public String getProfileTextColor(){
		return profileTextColor;
	}

	public void setVerified(boolean verified){
		this.verified = verified;
	}

	public boolean isVerified(){
		return verified;
	}

	public void setProfileImageUrl(String profileImageUrl){
		this.profileImageUrl = profileImageUrl;
	}

	public String getProfileImageUrl(){
		return profileImageUrl;
	}

	public void setTimeZone(String timeZone){
		this.timeZone = timeZone;
	}

	public String getTimeZone(){
		return timeZone;
	}

	public void setContributorsEnabled(boolean contributorsEnabled){
		this.contributorsEnabled = contributorsEnabled;
	}

	public boolean isContributorsEnabled(){
		return contributorsEnabled;
	}

	public void setUrl(Object url){
		this.url = url;
	}

	public Object getUrl(){
		return url;
	}

	public void setProfileBackgroundTile(boolean profileBackgroundTile){
		this.profileBackgroundTile = profileBackgroundTile;
	}

	public boolean isProfileBackgroundTile(){
		return profileBackgroundTile;
	}

	public void setFollowRequestSent(Object followRequestSent){
		this.followRequestSent = followRequestSent;
	}

	public Object getFollowRequestSent(){
		return followRequestSent;
	}

	public void setStatusesCount(int statusesCount){
		this.statusesCount = statusesCount;
	}

	public int getStatusesCount(){
		return statusesCount;
	}

	public void setDefaultProfile(boolean defaultProfile){
		this.defaultProfile = defaultProfile;
	}

	public boolean isDefaultProfile(){
		return defaultProfile;
	}

	public void setFollowersCount(int followersCount){
		this.followersCount = followersCount;
	}

	public int getFollowersCount(){
		return followersCount;
	}

	public void setFollowing(Object following){
		this.following = following;
	}

	public Object getFollowing(){
		return following;
	}

	public void setProfileUseBackgroundImage(boolean profileUseBackgroundImage){
		this.profileUseBackgroundImage = profileUseBackgroundImage;
	}

	public boolean isProfileUseBackgroundImage(){
		return profileUseBackgroundImage;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocation(String location){
		this.location = location;
	}

	public String getLocation(){
		return location;
	}

	public void setProfileSidebarFillColor(String profileSidebarFillColor){
		this.profileSidebarFillColor = profileSidebarFillColor;
	}

	public String getProfileSidebarFillColor(){
		return profileSidebarFillColor;
	}

	public void setNotifications(Object notifications){
		this.notifications = notifications;
	}

	public Object getNotifications(){
		return notifications;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"VerifyCredentials{" + 
			"utc_offset = '" + utcOffset + '\'' + 
			",friends_count = '" + friendsCount + '\'' + 
			",profile_image_url_https = '" + profileImageUrlHttps + '\'' + 
			",listed_count = '" + listedCount + '\'' + 
			",profile_background_image_url = '" + profileBackgroundImageUrl + '\'' + 
			",default_profile_image = '" + defaultProfileImage + '\'' + 
			",favourites_count = '" + favouritesCount + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",description = '" + description + '\'' + 
			",is_translator = '" + isTranslator + '\'' + 
			",profile_background_image_url_https = '" + profileBackgroundImageUrlHttps + '\'' + 
			",protected = '" + jsonMemberProtected + '\'' + 
			",screen_name = '" + screenName + '\'' + 
			",id_str = '" + idStr + '\'' + 
			",profile_link_color = '" + profileLinkColor + '\'' + 
			",show_all_inline_media = '" + showAllInlineMedia + '\'' + 
			",geo_enabled = '" + geoEnabled + '\'' + 
			",id = '" + id + '\'' + 
			",profile_background_color = '" + profileBackgroundColor + '\'' + 
			",lang = '" + lang + '\'' + 
			",profile_sidebar_border_color = '" + profileSidebarBorderColor + '\'' + 
			",profile_text_color = '" + profileTextColor + '\'' + 
			",verified = '" + verified + '\'' + 
			",profile_image_url = '" + profileImageUrl + '\'' + 
			",time_zone = '" + timeZone + '\'' + 
			",contributors_enabled = '" + contributorsEnabled + '\'' + 
			",url = '" + url + '\'' + 
			",profile_background_tile = '" + profileBackgroundTile + '\'' + 
			",follow_request_sent = '" + followRequestSent + '\'' + 
			",statuses_count = '" + statusesCount + '\'' + 
			",default_profile = '" + defaultProfile + '\'' + 
			",followers_count = '" + followersCount + '\'' + 
			",following = '" + following + '\'' + 
			",profile_use_background_image = '" + profileUseBackgroundImage + '\'' + 
			",name = '" + name + '\'' + 
			",location = '" + location + '\'' + 
			",profile_sidebar_fill_color = '" + profileSidebarFillColor + '\'' + 
			",notifications = '" + notifications + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}