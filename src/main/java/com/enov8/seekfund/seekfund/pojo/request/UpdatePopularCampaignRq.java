package com.enov8.seekfund.seekfund.pojo.request;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdatePopularCampaignRq implements Serializable {

    private GenericStatusConstant status;
    private Long order;



    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }
}
