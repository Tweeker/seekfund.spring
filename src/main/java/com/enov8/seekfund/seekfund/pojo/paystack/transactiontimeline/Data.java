package com.enov8.seekfund.seekfund.pojo.paystack.transactiontimeline;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data{

	@JsonProperty("input")
	private List<Object> input;

	@JsonProperty("success")
	private boolean success;

	@JsonProperty("mobile")
	private boolean mobile;

	@JsonProperty("channel")
	private String channel;

	@JsonProperty("history")
	private List<HistoryItem> history;

	@JsonProperty("errors")
	private int errors;

	@JsonProperty("time_spent")
	private int timeSpent;

	@JsonProperty("attempts")
	private int attempts;

	@JsonProperty("authentication")
	private Object authentication;

	public void setInput(List<Object> input){
		this.input = input;
	}

	public List<Object> getInput(){
		return input;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMobile(boolean mobile){
		this.mobile = mobile;
	}

	public boolean isMobile(){
		return mobile;
	}

	public void setChannel(String channel){
		this.channel = channel;
	}

	public String getChannel(){
		return channel;
	}

	public void setHistory(List<HistoryItem> history){
		this.history = history;
	}

	public List<HistoryItem> getHistory(){
		return history;
	}

	public void setErrors(int errors){
		this.errors = errors;
	}

	public int getErrors(){
		return errors;
	}

	public void setTimeSpent(int timeSpent){
		this.timeSpent = timeSpent;
	}

	public int getTimeSpent(){
		return timeSpent;
	}

	public void setAttempts(int attempts){
		this.attempts = attempts;
	}

	public int getAttempts(){
		return attempts;
	}

	public void setAuthentication(Object authentication){
		this.authentication = authentication;
	}

	public Object getAuthentication(){
		return authentication;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"input = '" + input + '\'' + 
			",success = '" + success + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",channel = '" + channel + '\'' + 
			",history = '" + history + '\'' + 
			",errors = '" + errors + '\'' + 
			",time_spent = '" + timeSpent + '\'' + 
			",attempts = '" + attempts + '\'' + 
			",authentication = '" + authentication + '\'' + 
			"}";
		}
}