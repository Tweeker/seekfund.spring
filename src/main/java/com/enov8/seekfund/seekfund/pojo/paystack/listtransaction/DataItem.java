package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@JsonProperty("amount")
	private int amount;

	@JsonProperty("metadata")
	private Metadata metadata;

	@JsonProperty("fees")
	private int fees;

	@JsonProperty("fees_split")
	private Object feesSplit;

	@JsonProperty("log")
	private Log log;

	@JsonProperty("subaccount")
	private Subaccount subaccount;

	@JsonProperty("channel")
	private String channel;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("ip_address")
	private String ipAddress;

	@JsonProperty("message")
	private Object message;

	@JsonProperty("gateway_response")
	private String gatewayResponse;

	@JsonProperty("reference")
	private String reference;

	@JsonProperty("paid_at")
	private String paidAt;

	@JsonProperty("authorization")
	private Authorization authorization;



	@JsonProperty("domain")
	private String domain;



	@JsonProperty("currency")
	private String currency;

	@JsonProperty("id")
	private int id;

	@JsonProperty("plan")
	private Plan plan;

	@JsonProperty("status")
	private String status;

	@JsonProperty("customer")
	private Customer customer;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setMetadata(Metadata metadata){
		this.metadata = metadata;
	}

	public Metadata getMetadata(){
		return metadata;
	}

	public void setFees(int fees){
		this.fees = fees;
	}

	public int getFees(){
		return fees;
	}

	public void setFeesSplit(Object feesSplit){
		this.feesSplit = feesSplit;
	}

	public Object getFeesSplit(){
		return feesSplit;
	}

	public void setLog(Log log){
		this.log = log;
	}

	public Log getLog(){
		return log;
	}

	public void setSubaccount(Subaccount subaccount){
		this.subaccount = subaccount;
	}

	public Subaccount getSubaccount(){
		return subaccount;
	}

	public void setChannel(String channel){
		this.channel = channel;
	}

	public String getChannel(){
		return channel;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setIpAddress(String ipAddress){
		this.ipAddress = ipAddress;
	}

	public String getIpAddress(){
		return ipAddress;
	}

	public void setMessage(Object message){
		this.message = message;
	}

	public Object getMessage(){
		return message;
	}

	public void setGatewayResponse(String gatewayResponse){
		this.gatewayResponse = gatewayResponse;
	}

	public String getGatewayResponse(){
		return gatewayResponse;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}



	public void setAuthorization(Authorization authorization){
		this.authorization = authorization;
	}

	public Authorization getAuthorization(){
		return authorization;
	}



	public void setDomain(String domain){
		this.domain = domain;
	}

	public String getDomain(){
		return domain;
	}

	public void setPaidAt(String paidAt){
		this.paidAt = paidAt;
	}

	public String getPaidAt(){
		return paidAt;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPlan(Plan plan){
		this.plan = plan;
	}

	public Plan getPlan(){
		return plan;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setCustomer(Customer customer){
		this.customer = customer;
	}

	public Customer getCustomer(){
		return customer;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"amount = '" + amount + '\'' + 
			",metadata = '" + metadata + '\'' + 
			",fees = '" + fees + '\'' + 
			",fees_split = '" + feesSplit + '\'' + 
			",log = '" + log + '\'' + 
			",subaccount = '" + subaccount + '\'' + 
			",channel = '" + channel + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",ip_address = '" + ipAddress + '\'' + 
			",message = '" + message + '\'' + 
			",gateway_response = '" + gatewayResponse + '\'' + 
			",reference = '" + reference + '\'' + 
			",paid_at = '" + paidAt + '\'' + 
			",authorization = '" + authorization + '\'' + 
			",createdAt = '" + createdAt + '\'' + 
			",domain = '" + domain + '\'' + 
			",paidAt = '" + paidAt + '\'' + 
			",currency = '" + currency + '\'' + 
			",id = '" + id + '\'' + 
			",plan = '" + plan + '\'' + 
			",status = '" + status + '\'' + 
			",customer = '" + customer + '\'' + 
			"}";
		}
}