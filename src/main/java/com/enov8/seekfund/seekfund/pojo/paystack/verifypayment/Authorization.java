package com.enov8.seekfund.seekfund.pojo.paystack.verifypayment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class Authorization
{
    @JsonProperty("authorization_code")
    private String authorisationCode;

    public String getAuthorizationCode() { return this.authorisationCode; }

    public void setAuthorizationCode(String authorisationCode) { this.authorisationCode = authorisationCode; }

    private String bin;

    public String getBin() { return this.bin; }

    public void setBin(String bin) { this.bin = bin; }

    private String last4;

    public String getLast4() { return this.last4; }

    public void setLast4(String last4) { this.last4 = last4; }
    @JsonProperty("exp_month")
    private String expMonth;

    public String getExpMonth() { return this.expMonth; }

    public void setExpMonth(String expMonth) { this.expMonth = expMonth; }
    @JsonProperty("exp_year")
    private String expYear;

    public String getExpYear() { return this.expYear; }

    public void setExpYear(String expYear) { this.expYear = expYear; }

    private String channel;

    public String getChannel() { return this.channel; }

    public void setChannel(String channel) { this.channel = channel; }

    private String cardType;

    public String getCardType() { return this.cardType; }
    @JsonProperty("card_type")
    public void setCardType(String cardType) { this.cardType = cardType; }

    private String bank;

    public String getBank() { return this.bank; }

    public void setBank(String bank) { this.bank = bank; }
    @JsonProperty("country_code")
    private String countryCode;

    public String getCountryCode() { return this.countryCode; }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    private String brand;

    public String getBrand() { return this.brand; }

    public void setBrand(String brand) { this.brand = brand; }

    private boolean reusable;

    public boolean getReusable() { return this.reusable; }

    public void setReusable(boolean reusable) { this.reusable = reusable; }

    private String signature;

    public String getSignature() { return this.signature; }

    public void setSignature(String signature) { this.signature = signature; }
}
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class Customer
{
    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }
    @JsonProperty("first_name")
    private String firstName;

    public String getFirstName() { return this.firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }
    @JsonProperty("last_name")
    private String lastName;

    public String getLastName() { return this.lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    private String email;

    public String getEmail() { return this.email; }

    public void setEmail(String email) { this.email = email; }
    @JsonProperty("customer_code")
    private String customerCode;

    public String getCustomerCode() { return this.customerCode; }

    public void setCustomerCode(String customerCode) { this.customerCode = customerCode; }

    private String phone;

    public String getPhone() { return this.phone; }

    public void setPhone(String phone) { this.phone = phone; }

    private Object metadata;

    public Object getMetadata() { return this.metadata; }

    public void setMetadata(Object metadata) { this.metadata = metadata; }
    @JsonProperty("risk_action")
    private String riskAction;

    public String getRiskAction() { return this.riskAction; }

    public void setRiskAction(String riskAction) { this.riskAction = riskAction; }
}