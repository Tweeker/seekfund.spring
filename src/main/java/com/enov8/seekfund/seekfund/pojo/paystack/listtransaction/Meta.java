package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class Meta{

	@JsonProperty("total")
	private int total;

	@JsonProperty("total_volume")
	private double totalVolume;

	@JsonProperty("pageCount")
	private int pageCount;

	@JsonProperty("perPage")
	private int perPage;

	@JsonProperty("page")
	private int page;

	@JsonProperty("skipped")
	private int skipped;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setTotalVolume(double totalVolume){
		this.totalVolume = totalVolume;
	}

	public double getTotalVolume(){
		return totalVolume;
	}

	public void setPageCount(int pageCount){
		this.pageCount = pageCount;
	}

	public int getPageCount(){
		return pageCount;
	}

	public void setPerPage(int perPage){
		this.perPage = perPage;
	}

	public int getPerPage(){
		return perPage;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setSkipped(int skipped){
		this.skipped = skipped;
	}

	public int getSkipped(){
		return skipped;
	}

	@Override
 	public String toString(){
		return 
			"Meta{" + 
			"total = '" + total + '\'' + 
			",total_volume = '" + totalVolume + '\'' + 
			",pageCount = '" + pageCount + '\'' + 
			",perPage = '" + perPage + '\'' + 
			",page = '" + page + '\'' + 
			",skipped = '" + skipped + '\'' + 
			"}";
		}
}