package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class Authorization{

	@JsonProperty("last4")
	private String last4;

	@JsonProperty("country_code")
	private String countryCode;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("signature")
	private String signature;

	@JsonProperty("authorization_code")
	private String authorizationCode;

	@JsonProperty("bin")
	private String bin;

	@JsonProperty("channel")
	private String channel;

	@JsonProperty("exp_month")
	private String expMonth;

	@JsonProperty("exp_year")
	private String expYear;

	@JsonProperty("card_type")
	private String cardType;

	@JsonProperty("brand")
	private String brand;

	@JsonProperty("reusable")
	private boolean reusable;

	public void setLast4(String last4){
		this.last4 = last4;
	}

	public String getLast4(){
		return last4;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setBank(String bank){
		this.bank = bank;
	}

	public String getBank(){
		return bank;
	}

	public void setSignature(String signature){
		this.signature = signature;
	}

	public String getSignature(){
		return signature;
	}

	public void setAuthorizationCode(String authorizationCode){
		this.authorizationCode = authorizationCode;
	}

	public String getAuthorizationCode(){
		return authorizationCode;
	}

	public void setBin(String bin){
		this.bin = bin;
	}

	public String getBin(){
		return bin;
	}

	public void setChannel(String channel){
		this.channel = channel;
	}

	public String getChannel(){
		return channel;
	}

	public void setExpMonth(String expMonth){
		this.expMonth = expMonth;
	}

	public String getExpMonth(){
		return expMonth;
	}

	public void setExpYear(String expYear){
		this.expYear = expYear;
	}

	public String getExpYear(){
		return expYear;
	}

	public void setCardType(String cardType){
		this.cardType = cardType;
	}

	public String getCardType(){
		return cardType;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	public void setReusable(boolean reusable){
		this.reusable = reusable;
	}

	public boolean isReusable(){
		return reusable;
	}

	@Override
 	public String toString(){
		return 
			"Authorization{" + 
			"last4 = '" + last4 + '\'' + 
			",country_code = '" + countryCode + '\'' + 
			",bank = '" + bank + '\'' + 
			",signature = '" + signature + '\'' + 
			",authorization_code = '" + authorizationCode + '\'' + 
			",bin = '" + bin + '\'' + 
			",channel = '" + channel + '\'' + 
			",exp_month = '" + expMonth + '\'' + 
			",exp_year = '" + expYear + '\'' + 
			",card_type = '" + cardType + '\'' + 
			",brand = '" + brand + '\'' + 
			",reusable = '" + reusable + '\'' + 
			"}";
		}
}