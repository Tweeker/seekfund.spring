package com.enov8.seekfund.seekfund.pojo;

import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;

import java.io.Serializable;

public class PopularCampaignRqPojo implements Serializable {

    private String yearRange;
    private CurrentCampaignPojo currentCampaign;

    public String getYearRange() {
        return yearRange;
    }

    public void setYearRange(String yearRange) {
        this.yearRange = yearRange;
    }

    public CurrentCampaignPojo getCurrentCampaign() {
        return currentCampaign;
    }

    public void setCurrentCampaign(CurrentCampaignPojo currentCampaign) {
        this.currentCampaign = currentCampaign;
    }
}
