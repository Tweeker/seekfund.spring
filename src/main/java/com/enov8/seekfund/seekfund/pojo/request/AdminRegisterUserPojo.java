package com.enov8.seekfund.seekfund.pojo.request;

import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminRegisterUserPojo implements Serializable {

/*    export type RegisterFormUser = {
        username: string;
        email: string;
        password: string;
        confirmPassword: string;
    };*/
    private AppUserRoleConstant role ;
    private Boolean confirmEmail  = false;
    private String username;
    private String email;
    private String password;
    private String confirmPassword;

    public AppUserRoleConstant getRole() {
        return role;
    }

    public void setRole(AppUserRoleConstant role) {
        this.role = role;
    }

    public Boolean getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(Boolean confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
