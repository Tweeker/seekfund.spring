package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class Customer{

	@JsonProperty("metadata")
	private Object metadata;

	@JsonProperty("risk_action")
	private String riskAction;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("id")
	private int id;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("customer_code")
	private String customerCode;

	@JsonProperty("email")
	private String email;

	public void setMetadata(Object metadata){
		this.metadata = metadata;
	}

	public Object getMetadata(){
		return metadata;
	}

	public void setRiskAction(String riskAction){
		this.riskAction = riskAction;
	}

	public String getRiskAction(){
		return riskAction;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getCustomerCode(){
		return customerCode;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"Customer{" + 
			"metadata = '" + metadata + '\'' + 
			",risk_action = '" + riskAction + '\'' + 
			",phone = '" + phone + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",id = '" + id + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",customer_code = '" + customerCode + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}