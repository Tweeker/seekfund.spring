package com.enov8.seekfund.seekfund.pojo.paystack.transactiontotal;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data{

	@JsonProperty("total_volume")
	private int totalVolume;

	@JsonProperty("total_volume_by_currency")
	private List<TotalVolumeByCurrencyItem> totalVolumeByCurrency;

	@JsonProperty("total_transactions")
	private int totalTransactions;

	@JsonProperty("pending_transfers_by_currency")
	private List<PendingTransfersByCurrencyItem> pendingTransfersByCurrency;

	@JsonProperty("pending_transfers")
	private int pendingTransfers;

	public void setTotalVolume(int totalVolume){
		this.totalVolume = totalVolume;
	}

	public int getTotalVolume(){
		return totalVolume;
	}

	public void setTotalVolumeByCurrency(List<TotalVolumeByCurrencyItem> totalVolumeByCurrency){
		this.totalVolumeByCurrency = totalVolumeByCurrency;
	}

	public List<TotalVolumeByCurrencyItem> getTotalVolumeByCurrency(){
		return totalVolumeByCurrency;
	}

	public void setTotalTransactions(int totalTransactions){
		this.totalTransactions = totalTransactions;
	}

	public int getTotalTransactions(){
		return totalTransactions;
	}

	public void setPendingTransfersByCurrency(List<PendingTransfersByCurrencyItem> pendingTransfersByCurrency){
		this.pendingTransfersByCurrency = pendingTransfersByCurrency;
	}

	public List<PendingTransfersByCurrencyItem> getPendingTransfersByCurrency(){
		return pendingTransfersByCurrency;
	}

	public void setPendingTransfers(int pendingTransfers){
		this.pendingTransfers = pendingTransfers;
	}

	public int getPendingTransfers(){
		return pendingTransfers;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"total_volume = '" + totalVolume + '\'' + 
			",total_volume_by_currency = '" + totalVolumeByCurrency + '\'' + 
			",total_transactions = '" + totalTransactions + '\'' + 
			",pending_transfers_by_currency = '" + pendingTransfersByCurrency + '\'' + 
			",pending_transfers = '" + pendingTransfers + '\'' + 
			"}";
		}
}