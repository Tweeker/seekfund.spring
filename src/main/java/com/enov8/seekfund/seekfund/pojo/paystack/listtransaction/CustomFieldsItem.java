package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class CustomFieldsItem{

	@JsonProperty("variable_name")
	private String variableName;

	@JsonProperty("display_name")
	private String displayName;

	@JsonProperty("value")
	private String value;

	public void setVariableName(String variableName){
		this.variableName = variableName;
	}

	public String getVariableName(){
		return variableName;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}

	public String getDisplayName(){
		return displayName;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"CustomFieldsItem{" + 
			"variable_name = '" + variableName + '\'' + 
			",display_name = '" + displayName + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}