package com.enov8.seekfund.seekfund.pojo.paystack.listtransaction;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.robohorse.robopojogenerator")
public class Metadata{

	@JsonProperty("referrer")
	private String referrer;

	@JsonProperty("custom_fields")
	private List<CustomFieldsItem> customFields;

	public void setReferrer(String referrer){
		this.referrer = referrer;
	}

	public String getReferrer(){
		return referrer;
	}

	public void setCustomFields(List<CustomFieldsItem> customFields){
		this.customFields = customFields;
	}

	public List<CustomFieldsItem> getCustomFields(){
		return customFields;
	}

	@Override
 	public String toString(){
		return 
			"Metadata{" + 
			"referrer = '" + referrer + '\'' + 
			",custom_fields = '" + customFields + '\'' + 
			"}";
		}
}