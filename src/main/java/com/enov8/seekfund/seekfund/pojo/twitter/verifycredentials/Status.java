package com.enov8.seekfund.seekfund.pojo.twitter.verifycredentials;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Status{

	@JsonProperty("in_reply_to_status_id_str")
	private String inReplyToStatusIdStr;

	@JsonProperty("in_reply_to_status_id")
	private long inReplyToStatusId;

	@JsonProperty("coordinates")
	private Coordinates coordinates;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("in_reply_to_user_id_str")
	private String inReplyToUserIdStr;

	@JsonProperty("truncated")
	private boolean truncated;

	@JsonProperty("source")
	private String source;

	@JsonProperty("retweet_count")
	private int retweetCount;

	@JsonProperty("retweeted")
	private boolean retweeted;

	@JsonProperty("geo")
	private Geo geo;

	@JsonProperty("in_reply_to_screen_name")
	private String inReplyToScreenName;

	@JsonProperty("id_str")
	private String idStr;

	@JsonProperty("in_reply_to_user_id")
	private int inReplyToUserId;

	@JsonProperty("contributors")
	private Object contributors;

	@JsonProperty("id")
	private long id;

	@JsonProperty("place")
	private Place place;

	@JsonProperty("text")
	private String text;

	@JsonProperty("favorited")
	private boolean favorited;

	public void setInReplyToStatusIdStr(String inReplyToStatusIdStr){
		this.inReplyToStatusIdStr = inReplyToStatusIdStr;
	}

	public String getInReplyToStatusIdStr(){
		return inReplyToStatusIdStr;
	}

	public void setInReplyToStatusId(long inReplyToStatusId){
		this.inReplyToStatusId = inReplyToStatusId;
	}

	public long getInReplyToStatusId(){
		return inReplyToStatusId;
	}

	public void setCoordinates(Coordinates coordinates){
		this.coordinates = coordinates;
	}

	public Coordinates getCoordinates(){
		return coordinates;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setInReplyToUserIdStr(String inReplyToUserIdStr){
		this.inReplyToUserIdStr = inReplyToUserIdStr;
	}

	public String getInReplyToUserIdStr(){
		return inReplyToUserIdStr;
	}

	public void setTruncated(boolean truncated){
		this.truncated = truncated;
	}

	public boolean isTruncated(){
		return truncated;
	}

	public void setSource(String source){
		this.source = source;
	}

	public String getSource(){
		return source;
	}

	public void setRetweetCount(int retweetCount){
		this.retweetCount = retweetCount;
	}

	public int getRetweetCount(){
		return retweetCount;
	}

	public void setRetweeted(boolean retweeted){
		this.retweeted = retweeted;
	}

	public boolean isRetweeted(){
		return retweeted;
	}

	public void setGeo(Geo geo){
		this.geo = geo;
	}

	public Geo getGeo(){
		return geo;
	}

	public void setInReplyToScreenName(String inReplyToScreenName){
		this.inReplyToScreenName = inReplyToScreenName;
	}

	public String getInReplyToScreenName(){
		return inReplyToScreenName;
	}

	public void setIdStr(String idStr){
		this.idStr = idStr;
	}

	public String getIdStr(){
		return idStr;
	}

	public void setInReplyToUserId(int inReplyToUserId){
		this.inReplyToUserId = inReplyToUserId;
	}

	public int getInReplyToUserId(){
		return inReplyToUserId;
	}

	public void setContributors(Object contributors){
		this.contributors = contributors;
	}

	public Object getContributors(){
		return contributors;
	}

	public void setId(long id){
		this.id = id;
	}

	public long getId(){
		return id;
	}

	public void setPlace(Place place){
		this.place = place;
	}

	public Place getPlace(){
		return place;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setFavorited(boolean favorited){
		this.favorited = favorited;
	}

	public boolean isFavorited(){
		return favorited;
	}

	@Override
 	public String toString(){
		return 
			"Status{" + 
			"in_reply_to_status_id_str = '" + inReplyToStatusIdStr + '\'' + 
			",in_reply_to_status_id = '" + inReplyToStatusId + '\'' + 
			",coordinates = '" + coordinates + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",in_reply_to_user_id_str = '" + inReplyToUserIdStr + '\'' + 
			",truncated = '" + truncated + '\'' + 
			",source = '" + source + '\'' + 
			",retweet_count = '" + retweetCount + '\'' + 
			",retweeted = '" + retweeted + '\'' + 
			",geo = '" + geo + '\'' + 
			",in_reply_to_screen_name = '" + inReplyToScreenName + '\'' + 
			",id_str = '" + idStr + '\'' + 
			",in_reply_to_user_id = '" + inReplyToUserId + '\'' + 
			",contributors = '" + contributors + '\'' + 
			",id = '" + id + '\'' + 
			",place = '" + place + '\'' + 
			",text = '" + text + '\'' + 
			",favorited = '" + favorited + '\'' + 
			"}";
		}
}