package com.enov8.seekfund.seekfund.pojo.request;

import com.enov8.seekfund.seekfund.enumumeration.BankAccountStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdatePaymentDetailsRqPojo {

    private String accountName;
    private BankAccountStatus accountStatus;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public BankAccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(BankAccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }
}
