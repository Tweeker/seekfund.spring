package com.enov8.seekfund.seekfund.pojo;

import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PopularCampaignPojo  implements Serializable {

    private Long id;

    private CurrentCampaignPojo campaign;

    private Long order;

    private GenericStatusConstant status;

    private String campaignImage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CurrentCampaignPojo getCampaign() {
        return campaign;
    }

    public void setCampaign(CurrentCampaignPojo campaign) {
        this.campaign = campaign;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public GenericStatusConstant getStatus() {
        return status;
    }

    public void setStatus(GenericStatusConstant status) {
        this.status = status;
    }

    public String getCampaignImage() {
        return campaignImage;
    }

    public void setCampaignImage(String campaignImage) {
        this.campaignImage = campaignImage;
    }
}
