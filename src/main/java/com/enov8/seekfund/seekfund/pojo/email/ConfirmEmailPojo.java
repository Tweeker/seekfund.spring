package com.enov8.seekfund.seekfund.pojo.email;


import java.io.Serializable;

public class ConfirmEmailPojo implements Serializable {

    private String username;
    private String href;
    private String senderName;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}
