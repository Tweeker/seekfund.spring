package com.enov8.seekfund.seekfund.pojo.projection;

import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.util.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignDonationPojo implements Serializable {

    private Long id;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.ADMIN_DATETIME_FORMAT)
    private Timestamp dateCreated;
    private BigDecimal amount;
    private PaymentStatus paymentStatus;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.ADMIN_DATETIME_FORMAT)
    private Timestamp dateCompleted;
    private String paymentReference;
    private Long campaignId;

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    private String comment;
    private String donatedByUsername;

    private BigDecimal amountRaised = new BigDecimal("0");
    private Long donatedById;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Timestamp getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Timestamp dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getDonatedByUsername() {
        return donatedByUsername;
    }

    public void setDonatedByUsername(String donatedByUsername) {
        this.donatedByUsername = donatedByUsername;
    }

    public Long getDonatedById() {
        return donatedById;
    }

    public void setDonatedById(Long donatedById) {
        this.donatedById = donatedById;
    }

    public BigDecimal getAmountRaised() {
        return amountRaised;
    }

    public void setAmountRaised(BigDecimal amountRaised) {
        this.amountRaised = amountRaised;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }
}
