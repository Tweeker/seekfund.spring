package com.enov8.seekfund.seekfund.pojo.paystack.resolvebvn;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ResolveBvnResp{

	@JsonProperty("data")
	private Data data;

	@JsonProperty("meta")
	private Meta meta;

	@JsonProperty("message")
	private String message;

	@JsonProperty("status")
	private boolean status;

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResolveBvnResp{" + 
			"data = '" + data + '\'' + 
			",meta = '" + meta + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}