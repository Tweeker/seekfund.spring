package com.enov8.seekfund.seekfund.pojo.paystack.verifypayment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public  class Data
{
    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    private String domain;

    public String getDomain() { return this.domain; }

    public void setDomain(String domain) { this.domain = domain; }

    private String status;

    public String getStatus() { return this.status; }

    public void setStatus(String status) { this.status = status; }

    private String reference;

    public String getReference() { return this.reference; }

    public void setReference(String reference) { this.reference = reference; }

    private int amount;

    public int getAmount() { return this.amount; }

    public void setAmount(int amount) { this.amount = amount; }

    private Object message;

    public Object getMessage() { return this.message; }

    public void setMessage(Object message) { this.message = message; }
    @JsonProperty("gateway_response")
    private String gatewayResponse;

    public String getGatewayResponse() { return this.gatewayResponse; }

    public void setGatewayResponse(String gatewayResponse) { this.gatewayResponse = gatewayResponse; }


    @JsonProperty("paid_at")
    private Date paidAt;

    public Date getPaidAt() { return this.paidAt; }

    public void setPaidAt(Date paidAt) { this.paidAt = paidAt; }
    @JsonProperty("created_at")
    private Date createdAt;

    public Date getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    private String channel;

    public String getChannel() { return this.channel; }

    public void setChannel(String channel) { this.channel = channel; }

    private String currency;

    public String getCurrency() { return this.currency; }

    public void setCurrency(String currency) { this.currency = currency; }
    @JsonProperty("ip_address")
    private String ipAddress;

    public String getIpAddress() { return this.ipAddress; }

    public void setIpAddress(String ipAddress) { this.ipAddress = ipAddress; }

    private Metadata metadata;

    public Metadata getMetadata() { return this.metadata; }

    public void setMetadata(Metadata metadata) { this.metadata = metadata; }

    private Log log;

    public Log getLog() { return this.log; }

    public void setLog(Log log) { this.log = log; }

    private int fees;

    public int getFees() { return this.fees; }

    public void setFees(int fees) { this.fees = fees; }
    @JsonProperty("fees_split")
    private Object feesSplit;

    public Object getFeesSplit() { return this.feesSplit; }

    public void setFeesSplit(Object feesSplit) { this.feesSplit = feesSplit; }

    private Authorization authorization;

    public Authorization getAuthorization() { return this.authorization; }

    public void setAuthorization(Authorization authorization) { this.authorization = authorization; }

    private Customer customer;

    public Customer getCustomer() { return this.customer; }

    public void setCustomer(Customer customer) { this.customer = customer; }

    private Object plan;

    public Object getPlan() { return this.plan; }

    public void setPlan(Object plan) { this.plan = plan; }




    @JsonProperty("transaction_date")
    private Date transactionDate;

    public Date getTransactionDate() { return this.transactionDate; }

    public void setTransactionDate(Date transactionDate) { this.transactionDate = transactionDate; }
    @JsonProperty("plan_object")
    private PlanObject planObject;

    public PlanObject getPlanObject() { return this.planObject; }

    public void setPlanObject(PlanObject plan_object) { this.planObject = plan_object; }

    private Subaccount subaccount;

    public Subaccount getSubaccount() { return this.subaccount; }

    public void setSubaccount(Subaccount subaccount) { this.subaccount = subaccount; }
}

