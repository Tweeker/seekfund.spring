package com.enov8.seekfund.seekfund.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MvcConfig.class, SecurityConfig.class })
@ComponentScan(value = { "com.enov8" })
public class RootConfig {

}
