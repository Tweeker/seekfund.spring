package com.enov8.seekfund.seekfund.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class AppListener implements ApplicationListener<ContextRefreshedEvent> {
	private static Logger logger = LoggerFactory.getLogger(AppListener.class);
	@Autowired
	StartupAction startupAction;


	@Value("${paystack.testconn}")
	private boolean PAYSTACK_TEST_CONN;

	private boolean contextStarted = false;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("=======================place startup actions ============================");

		//startupAction.testCampaign();

		//startupAction.testEntityManager();


		//startupAction.addCampaignCategory();


		///startupAction.listS3BucketsAvailable();






		//startupAction.testCampaignDonation();

		startupAction.addBanks();


		//startupAction.getUserBankAcc();

		//startupAction.textEmail();


		startupAction.testTrendingCampaign();

		if(PAYSTACK_TEST_CONN){

			startupAction.testPaystackConnection();
		}



		startupAction.testCriteriaDetached();

	//	startupAction.testCampaignWithImage();


		//startupAction.testWelcomeEmail();


//		/startupAction.testPaystackFeign();


		//startupAction.listPayStackTrans();


	}

}
