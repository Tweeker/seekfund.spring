package com.enov8.seekfund.seekfund.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.enov8.seekfund.seekfund.filter.AdminFilter;
import com.enov8.seekfund.seekfund.service.*;
import com.enov8.seekfund.seekfund.task.Scheduler;
import com.enov8.seekfund.seekfund.task.SchedulerImpl;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
/*import okhttp3.Credentials;
import okhttp3.OkHttpClient;*/
//import feign.okhttp.OkHttpClient;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.net.InetSocketAddress;
import java.net.Proxy;

@Component
public class BeanFactory {
    @Value("${amazon.aws.access-key-id}")
    private String AWS_ACCESS_KEY;

    @Value("${amazon.aws.access-key-secret}")
    private String AWS_SECRET_KEY;

    @Value("${amazon.aws.access-key-id}")
    private String AWS_S3_DEFAULT_BUCKEY;

    @Value("${amazon.region}")
    private String AWS_REGION;

    @Value("${seekfund.useProxy}")
    private Boolean USE_PROXY;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private SpringTemplateEngine templateEngine;
    @Value("${twitter.consumerKey}")
    private String twitter_consumer_key;
    @Value("${twitter.consumerSecret}")
    private String twitter_consumer_secret;


    @Bean
    @Qualifier(value = "entityManager")
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }
    @Bean
    public TwitterConnectionFactory connectionFactory (){

        TwitterConnectionFactory connectionFactory =
                new TwitterConnectionFactory( twitter_consumer_key,
                        twitter_consumer_secret);


        return connectionFactory;
    }
    @Bean
    public FilterRegistrationBean adminApiFilterBean() {

        FilterRegistrationBean adminApiFilter = new FilterRegistrationBean();
        AdminFilter adminFilter = new AdminFilter();
        adminApiFilter.setFilter(adminFilter);
        adminApiFilter.addUrlPatterns("/admin/api/*");
        adminApiFilter.setName("adminApiFilter");
        adminApiFilter.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE+3);
        return adminApiFilter;
    }

    @Bean
    public AmazonS3 amazonS3(){
        AWSCredentials credentials = new BasicAWSCredentials(
                AWS_ACCESS_KEY,
                AWS_SECRET_KEY
        );
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.fromName(AWS_REGION))
                .build();
    }
    @Bean
    public Scheduler scheduler(){

        return new SchedulerImpl();
    }
    @Bean(name = "emailService")
    private EmailService emailService(){

        return new EmailServiceImpl(emailSender, databaseService, sessionFactory, templateEngine);
    }
    @Bean(name = "payStackClient")
    public OkHttpClient okHttpClient(){
        OkHttpClient client = null;
        if(USE_PROXY){
            Proxy proxyTest = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(
                    "sv001v10k006.africa.int.zenithbank.com",
                    82));

            OkHttpClient.Builder builder = new OkHttpClient.Builder().
                    proxyAuthenticator((route, response) -> {
                        String credential = Credentials.basic("appdev2", "Obasanj0");
                        return response.request().newBuilder()
                                .header("Proxy-Authorization", credential)
                                .build();
                    }).
                    proxy(proxyTest);
             client = builder.build();

        }else{
             client = new OkHttpClient();
        }




        return client;

    }
    @Bean
    public PaystackService paystackService(){

        return new PaystackServiceImpl();
    }
    /*private ITransaction paystackTransaction(){
        Proxy proxyTest = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(
                "sv001v10k006.africa.int.zenithbank.com",
                82));

        OkHttpClient.Builder builder = new OkHttpClient.Builder().
                proxyAuthenticator((route, response) -> {
                    String credential = Credentials.basic("appdev2", "Obasanj0");
                    return response.request().newBuilder()
                            .header("Proxy-Authorization", credential)
                            .build();
                }).
                proxy(proxyTest);
        OkHttpClient client = builder.build();




        ITransaction iTransaction = Feign.builder().client(new OkHttpClient())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())

                .target(ITransaction.class, "https://api.paystack.co/transaction")
                ;

        return iTransaction;
    }*/




    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipart = new CommonsMultipartResolver();
        multipart.setMaxUploadSize(1 * 1024 * 1024);
        return multipart;
    }

    @Bean
    @Order(0)
    public MultipartFilter multipartFilter() {
        MultipartFilter multipartFilter = new MultipartFilter();
        multipartFilter.setMultipartResolverBeanName("multipartResolver");
        return multipartFilter;
    }
}
