package com.enov8.seekfund.seekfund.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;


import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import static org.thymeleaf.templatemode.TemplateMode.LEGACYHTML5;

//@EnableWebMvc
@Configuration
@ComponentScan("com.enov8")
public class MvcConfig  implements WebMvcConfigurer {
	@Autowired
	private ApplicationContext applicationContext;



	// start Thymeleaf specific configuration
	@Bean(name = "templateResolver")
	public SpringResourceTemplateResolver getTemplateResolver() {
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();

		templateResolver.setTemplateMode("LEGACYHTML5");

		templateResolver.setPrefix("/WEB-INF/templates/");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		templateResolver.addTemplateAlias("fragment", "fragment/fragment");

		templateResolver.setSuffix(".html");
		// templateResolver.setTemplateMode("HTML5");
		return templateResolver;
	}

	@Bean(name = "templateEngine")
	public SpringTemplateEngine getTemplateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();

		Set<IDialect> dialects = new HashSet<IDialect>();
		dialects.add(springSecurityDialect());

		//templateEngine.setEnableSpringELCompiler(true);
		templateEngine.setAdditionalDialects(dialects);
		//templateEngine.setTemplateResolver(getTemplateResolver());
		//templateEngine.addTemplateResolver(htmlTemplateResolver());
		templateEngine.addTemplateResolver(getTemplateResolver());


		return templateEngine;
	}
/*	@Bean(name = "emailTemplateResolver")
	public SpringResourceTemplateResolver htmlTemplateResolver(){
		SpringResourceTemplateResolver emailTemplateResolver = new SpringResourceTemplateResolver();
		emailTemplateResolver.setPrefix("classpath:/templates/mail/");
		emailTemplateResolver.setSuffix(".html");
		emailTemplateResolver.setTemplateMode("LEGACYHTML5");
		emailTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
		return emailTemplateResolver;
	}*/
	@Bean(name = "viewResolver")
	public ThymeleafViewResolver getViewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setApplicationContext(applicationContext);

		viewResolver.setTemplateEngine(getTemplateEngine());
		viewResolver.setCharacterEncoding("UTF-8");
		return viewResolver;
	}

	// end Thymeleaf specific configuration
	@Bean(name = "messageSource")
	public MessageSource getMessageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/WEB-INF/i18/seekfund");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**")
				.addResourceLocations("WEB-INF/resources/").setCachePeriod(0);
		registry.addResourceHandler("/static/**")
				.addResourceLocations("WEB-INF/resources/").setCachePeriod(0);
		registry.addResourceHandler("swagger-ui.html")
				.addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		SpringSecurityDialect dialect = new SpringSecurityDialect();
		return dialect;
	}

}
