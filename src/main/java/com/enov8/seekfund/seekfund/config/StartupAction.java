package com.enov8.seekfund.seekfund.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.PaymentStatus;
import com.enov8.seekfund.seekfund.mail.Mail;
import com.enov8.seekfund.seekfund.model.*;

import com.enov8.seekfund.seekfund.pojo.paystack.listtransaction.QueryString;
import com.enov8.seekfund.seekfund.pojo.paystack.verifypayment.VerifyTransactionResp;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.service.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Service

public class StartupAction {
    private static final Logger logger = LoggerFactory.getLogger(StartupAction.class);
    private DataRepo dataRepo;

    DatabaseService databaseService;

    SessionFactory sessionFactory;
    //@PersistenceContext
    EntityManager entityManager;


    CampaignService campaignService;
    @Autowired
    EmailService emailService;
    @Autowired
    PaystackService paystackService;

    @Autowired
    AmazonS3 amazonS3;
    @Autowired
    JavaMailSender emailSender;
    @Autowired
    SpringTemplateEngine templateEngine;


    private StartupAction(DataRepo dataRepo, DatabaseService databaseService,
                          EntityManager entityManager, CampaignService campaignService){
        this.dataRepo  = dataRepo;
        this.databaseService = databaseService;
        this.entityManager = entityManager;
        this.emailService = emailService;
        this.campaignService  = campaignService;
    }

    public void textEmail(){
        logger.info("Sending Email with Thymeleaf HTML Template Example");

        Mail mail = new Mail();
        mail.setFrom("richkingpere@gmail.com");
        mail.setTo("richkingpere@yahoo.com");
        mail.setSubject("Sending Email with Thymeleaf HTML Template Example");

        Map model = new HashMap();
        model.put("name", "Memorynotfound.com");
        model.put("location", "Belgium");
        model.put("signature", "https://memorynotfound.com");
        mail.setModel(model);
        try {
            emailService.sendSimpleMessage(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listS3BucketsAvailable(){


        try{
            List<Bucket> buckets =  amazonS3.listBuckets();

            logger.info("Bucket size " + buckets.size());

            for (Bucket bucket: buckets) {
                logger.info("Name " +  bucket.getName() +" Bucket owner " + bucket.getOwner());
                logger.info("Create Date " + bucket.getCreationDate());
                logger.info("=========================================");
            }
        } catch (Exception e){e.printStackTrace();}

    }
    public void getUserBankAcc(){



        String hql  = " select a.id as id, a.bank as bank," +
                " a.accountNumber as accountNumber, a.accountName as accountName , a.dateCreated as dateCreated, " +
                "a.dateModified as dateModified, a.status as status " +
                " from AppUserBankAccount a  " +
                "  where a.appUser.id  = " + 1;


        try {
            //Object[] rows = (Object[]) databaseService.getUniqueRecordByHql(hql);
            AppUserBankAccount appUserBankAccount  = (AppUserBankAccount) databaseService.getUniqueRecordByHql(hql,AppUserBankAccount.class);

            logger.info("====================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addCampaignCategory(){

        try{

            Long recordsTotal  = databaseService.getRecordCount(CampaignCategory.class);

            if(recordsTotal == 0){
                List<CampaignCategory> categories = dataRepo.getDefaultCampaignCategory();


                categories.forEach(campaignCategory -> {

                    try{ databaseService.saveRecord(campaignCategory); }
                    catch (Exception e){e.printStackTrace();}

                });
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void testCampaign(){

        databaseService.test();

    }

    public void testEntityManager() {

        CriteriaBuilder builder  = entityManager.getCriteriaBuilder();
        CriteriaQuery<AppUserCampaign> criteriaQuery = builder.createQuery(AppUserCampaign.class);

        Root<AppUserCampaign> root = criteriaQuery.from( AppUserCampaign.class );

        Metamodel m = entityManager.getMetamodel();
        EntityType<AppUserCampaign> appUserCampaignEntityType = m.entity(AppUserCampaign.class);
        Join<AppUserCampaign, AppUser> appUserCampaignAppUserJoin = root.join("appUser");



        criteriaQuery.where(builder.equal(appUserCampaignAppUserJoin.get("appUser.id"),5L));
        criteriaQuery.where(root.get("status").in(CampaignStatus.PENDING,
                CampaignStatus.RUNNING ));

        List<AppUserCampaign> people = entityManager.createQuery( criteriaQuery ).getResultList();

        System.currentTimeMillis();
    }

    public void testCampaignDonation() {

        CampaignDonation campaignDonation  = new CampaignDonation();

        campaignDonation.setPaymentStatus(PaymentStatus.COMPLETE);
        try{
            //AppUser appUser = (AppUser) databaseService.getRecordById(AppUser.class,1L);
            AppUserCampaign campaign =  (AppUserCampaign)databaseService.getRecordById(AppUserCampaign.class,47L);
           // campaignDonation.setDonatedBy(appUser);
            campaignDonation.setCampaign(campaign);
            campaignDonation.setDateCompleted(new Timestamp(System.currentTimeMillis()));
            campaignDonation.setPaymentRef("SEEKFUND-04");
            campaignDonation.setReturnMsg("00");
            campaignDonation.setComment("Sample Message ::: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid ea eligendi laborum non," +
                    " perspiciatis porro repellat voluptatibus. Aperiam autem doloremque eaque eligendi eum harum itaque minus possimus quia, sed ullam.");
            campaignDonation.setAmount(new BigDecimal("320"));


           databaseService.saveRecord(campaignDonation);

        }
        catch (Exception e){
            e.printStackTrace();
        }




    }

    public void addBanks(){

        try {
            Long recordsTotal  = databaseService.getRecordCount(Bank.class);

            if(recordsTotal == 0){

                dataRepo.getBanks().forEach(bank -> {
                    try{
                        databaseService.saveRecord(bank);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void testTrendingCampaign() {
        campaignService.init();
        List<CurrentCampaignPojo>  t  = campaignService.getTrendingCampaign();

        logger.info(t.size()+"");
    }



    public  void testWelcomeEmail(){


        try{

            EmailQueue emailQueue = (EmailQueue) databaseService.getRecordById(EmailQueue.class,165L);
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Context context = new Context();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            String json  = Pattern.compile("\\\\").matcher(emailQueue.getData()).replaceAll("\\\\\\\\");
              Map<String, Object> map = new HashMap<String, Object>();
            Mail mail = new Mail();
            map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
            mail.setModel(map);
            context.setVariables(mail.getModel());
            String html = templateEngine.process("mail/"+emailQueue.getEmailTemplate(), context);

            logger.info(html);
        }
        catch (Exception e){e.printStackTrace();}
        MimeMessage message = emailSender.createMimeMessage();



    }

    /*public void testPopularCampaign(){

        try{       campaignService.getPopularCampaign();}
        catch (Exception e){e.printStackTrace();}


    }*/

    public void testCampaignWithImage() {

        try{
            AppUserCampaign appUserCampaign  = (AppUserCampaign)
                    databaseService.getRecordById(AppUserCampaign.class,194L);
            campaignService.getCampaignImages(appUserCampaign);


        } catch (Exception e){
            e.printStackTrace();
        }


    }

    public  void listPayStackTrans(){


        try{
            QueryString queryString = new QueryString();
            queryString.setStatus("success");
            paystackService.listTransactions(queryString);

        } catch (Exception e){e.printStackTrace();}

    }
    public void testPaymentConnection() {
        try {
            //paystackService.testPaystackConnection();
            VerifyTransactionResp verifyTransactionResp =
                    paystackService.verifyPayment("e9a20c13862ccd75bb6a9b9922861c020c173420");

            ObjectMapper objectMapper = new ObjectMapper();
            logger.info(objectMapper.writeValueAsString(verifyTransactionResp));

            logger.info("=================");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void testPaystackConnection() {
        try{
            logger.info("======TESTING PAYSTACK CONNECTION =======");
        paystackService.testPaystackConnection();


           logger.info("=====DONE TESTING PAYSTACK============");
        }
        catch (Exception e){
          e.printStackTrace();
    }


//sv001v10k006.africa.int.zenithbank.com
       //172.29.90.43

 /*      System.setProperty("http.proxyUser", "appdev2");
       System.setProperty("http.proxyPassword", "Obasanj0");*/

/*

           ITransaction transaction = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(ITransaction.class,"https://api.paystack.co/transaction");

          String resp =   transaction.verify("b2095cae43ae1732e241d639d5b87995d352f3da");
*/

          logger.info("============");

    /*   Proxy proxyTest = new Proxy(Proxy.Type.HTTP,new InetSocketAddress("sv001v10k005.africa.int.zenithbank.com", 82));

       OkHttpClient.Builder builder = new OkHttpClient.Builder().
               proxyAuthenticator((route, response) -> {
                   String credential = Credentials.basic("appdev2", "Obasanj0");
                   return response.request().newBuilder()
                           .header("Proxy-Authorization", credential)
                           .build();
               }).
               proxy(proxyTest);
       OkHttpClient client = builder.build();

       HttpUrl.Builder urlBuilder
               = HttpUrl.parse("https://api.paystack.co").newBuilder();
       // urlBuilder.addQueryParameter("id", "1");



       String url = urlBuilder.build().toString();

       Request request = new Request.Builder()
               .url(url)

               .build();
       ///OkHttpClient client = new OkHttpClient();
       Call call = client.newCall(request);

       try{
           Response response = call.execute();
           System.out.println(response.body().string());
       }
       catch (Exception e){e.printStackTrace();}
*/



    }

    public void testCriteriaDetached() {

        //campaignService.getCampaignByIdWithDetached(107L);
        try {
            campaignService.searchCampaignsByCategory(8L,0,10);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
