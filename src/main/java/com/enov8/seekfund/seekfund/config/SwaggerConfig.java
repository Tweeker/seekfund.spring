package com.enov8.seekfund.seekfund.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${api.admin.security.key.name}")
    private String REST_SECURITY_ADMIN_KEY_NAME;
    @Value("${api.admin.security.key.value}")
    private String REST_SECURITY_ADMIN_KEY_VALUE;
    @Value("${swagger.host}")
    private String host;
    @Bean
    public Docket api() {
        Set<String> protocols = new HashSet<>();
        protocols.add("http");
        protocols.add("https");

        return new Docket(DocumentationType.SWAGGER_2)
               // .host("admin/api")
                .select()
                .apis(RequestHandlerSelectors.any())

                .apis(RequestHandlerSelectors.basePackage("com.enov8.seekfund.seekfund.admin"))
                //.paths(PathSelectors.ant("/admin/api/"))
                //.paths(PathSelectors.any())

                .build()
                .host(host)
                .protocols(protocols)
                .securitySchemes(Collections.singletonList(new ApiKey(REST_SECURITY_ADMIN_KEY_VALUE,REST_SECURITY_ADMIN_KEY_NAME, "header")))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Seekfunds Developer API",
                "Admin Api For Seekfunds",
                "API v1.0",
                "Terms of service",
                new Contact("Readdigo", "www.seekfunds.ng", "admin@seekfunds.com"),
                "License of API", "API license URL", Collections.emptyList());
    }


}
