package com.enov8.seekfund.seekfund.config;

import com.enov8.seekfund.seekfund.enumumeration.AppUserRoleConstant;
import com.enov8.seekfund.seekfund.enumumeration.SocialAuthType;
import com.enov8.seekfund.seekfund.security.*;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetailsSource;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private CurrentUserDetailsService currentUserDetailsService;
	@Autowired
	DatabaseService databaseService;
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/css/**", "/theme/**", "/assets/**").and().debug(false);
	}
	@Autowired
	@Qualifier("oauth2ClientContext")
	OAuth2ClientContext oauth2ClientContext;
	@Value("${security.require-ssl}")
	Boolean requireHTTPS;
	@Bean
	public CustomAuthenticationEntryPoint customAuthenticationEntryPoint() {
		return new CustomAuthenticationEntryPoint();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
;

		http.authorizeRequests()

		.antMatchers("/", "/assets/**","/static/**").permitAll()
		.antMatchers("/home/**").hasAuthority(AppUserRoleConstant.USER.getValue())
		.antMatchers(Constant.SOCIAL_REGISTER_ROUTE+"/**")
				.hasAuthority(AppUserRoleConstant.SOCIAL_AUTH.getValue())
				//.authenticated()
				//.hasAuthority(AppUserRoleConstant.SOCIAL_AUTH.getValue())
		.antMatchers("/setup/**")
				.hasAuthority(AppUserRoleConstant.USER.getValue())
		.antMatchers("/u/api/**")
				.hasAuthority(AppUserRoleConstant.USER.getValue())
		.antMatchers("/u/api/download/**").authenticated()
		.antMatchers("/super/**").hasAuthority(AppUserRoleConstant.SUPER.getValue())
		.and().exceptionHandling()
				.accessDeniedHandler(accessDeniedHandler()).accessDeniedPage("/accessDenied")
				.authenticationEntryPoint(customAuthenticationEntryPoint());
				//.accessDeniedHandler(accessDeniedHandler()).accessDeniedPage("/login");
		http.formLogin()
				//.loginProcessingUrl("/d/api/login")
				.passwordParameter(Constant.PASSOWRD_LOWER).usernameParameter(Constant.USERNAME_EMAIL_LOWER)
				.loginPage("/login")
				.failureUrl("/login?error").permitAll().and().logout().logoutUrl("/home/logout")
				.logoutSuccessUrl("/login").permitAll();
		
		http.csrf().disable().addFilter(customUsernamePasswordAuthenticationFilter());
		http.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
		//https://spring.io/guides/tutorials/spring-boot-oauth2/


		if(requireHTTPS){
			http.requiresChannel().anyRequest().requiresSecure();
		}


/*
	http.requiresChannel()
				.antMatchers("/u/api/**","/d/api/**","/login","/register").requiresSecure()
				.anyRequest().requiresInsecure();
*/

	/*	http.requiresChannel().anyRequest().requiresSecure();*/




	}
   @Bean
	public FilterRegistrationBean<OAuth2ClientContextFilter> oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
		FilterRegistrationBean<OAuth2ClientContextFilter> registration = new FilterRegistrationBean<OAuth2ClientContextFilter>();
		registration.setFilter(filter);
		registration.setOrder(-100);
		return registration;
	}

	@Bean
	public CustomUsernamePasswordAuthenticationFilter customUsernamePasswordAuthenticationFilter()
			throws Exception {
		CustomUsernamePasswordAuthenticationFilter customUsernamePasswordAuthenticationFilter = new CustomUsernamePasswordAuthenticationFilter();
		customUsernamePasswordAuthenticationFilter
				.setAuthenticationManager(authenticationManagerBean());
		customUsernamePasswordAuthenticationFilter
				.setAuthenticationSuccessHandler(customSuccessHandler());
		customUsernamePasswordAuthenticationFilter.setAuthenticationFailureHandler(customFailureHandler());
		return customUsernamePasswordAuthenticationFilter;
	}

	@Bean
	public CustomAuthenticationProvider customAuthenticationProvider() {
		return new CustomAuthenticationProvider();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(customAuthenticationProvider());


		//.userDetailsService(currentUserDetailsService);
		
	}
	@Bean
	public CustomSuccessHandler customSuccessHandler() {
		CustomSuccessHandler customSuccessHandler = new CustomSuccessHandler();
		
		return customSuccessHandler;
	}
	@Bean
	public CustomFailureHandler customFailureHandler(){
		
		return new CustomFailureHandler();
	}

	@Bean
	public AccessDeniedHandler accessDeniedHandler(){
		
		
		return new CustomAccessDeniedHandler();
	}
	@Bean
	@ConfigurationProperties("facebook")
	public ClientResources facebook() {
		return new ClientResources();
	}
	@Bean
	@ConfigurationProperties("google")
	public ClientResources google() {
		return new ClientResources();
	}
	@Bean
	@ConfigurationProperties("twitter")
	public ClientResources twitter() {
		return new ClientResources();
	}

	private Filter ssoFilter() {
		CompositeFilter filter = new CompositeFilter();
		List<Filter> filters = new ArrayList<>();
		filters.add(ssoFilter(facebook(), "/connect/facebook",SocialAuthType.FACEBOOK));
		filters.add(ssoFilter(google(), "/connect/google",SocialAuthType.GOOGLE));
		//filters.add(ssoFilter(twitter(), "/connect/twitter",SocialAuthType.TWITTER));
		filter.setFilters(filters);
		return filter;
	}


	private Filter ssoFilter(ClientResources client, String path,SocialAuthType authType) {

		OAuth2ClientAuthenticationProcessingAndSavingFilter filter = new OAuth2ClientAuthenticationProcessingAndSavingFilter(
				path);
		filter.setAuthenticationSuccessHandler(new CustomOAuthSuccessHandler(authType,databaseService));
		OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
		filter.setRestTemplate(template);




	/*	UserInfoTokenServices tokenServices = new UserInfoTokenServices(
				client.getResource().getUserInfoUri(), client.getClient().getClientId());*/
		CustomUserInfoTokenServices tokenServices = new CustomUserInfoTokenServices(
				client.getResource().getUserInfoUri(), client.getClient().getClientId());

		tokenServices.setRestTemplate(template);

		filter.setTokenServices(tokenServices);


		return filter;
	}


}
