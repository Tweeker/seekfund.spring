(function(){

    var campaignDonationPage   = $("#campaignDonationArea").html();


    var validatingPaymentHtml  = "" +
        "<br/></br><div class=\"ant-row-flex ant-row-flex-space-around ant-row-flex-middle\"><div class=\"\">" +
        "<b> Validating Payment</b>" +
        "</div></div>" +
        "" +
        ""+
        "<div class=\"ant-row-flex ant-row-flex-space-around ant-row-flex-middle\"><div class=\"\">" +
        "<div class=\"lds-facebook\"><div></div><div></div><div></div></div>" +
        "</div></div>" +
        "" +
        "";

    var resetForm  = function (){
        $("#phoneNumber").val("");
        $("#comment").val("");
        $("#amount").val("");
        $("#email").val("");
       // $("#campaignId").val("");

    }
    var donateSuccessMsg = function(){
      var tpl  = '<div data-show="true" class="ant-alert ant-alert-success ' +
            'ant-alert-with-description"><i class="anticon anticon-check-circle ant-alert-icon">' +
            '</i><span class="ant-alert-message">Donation Complete</span>' +
            '<span class="ant-alert-description">' +
            'Thank You for donation to seekfunds campaign!. Please return back to campaign or donate again.</span></div>';

        $("#errorNotification").html(tpl);

    }

    var addError =  function (err,alert) {


    if(!err){
        //$(".has-warning").removeClass("has-warning");
        $("#errorNotification").html("");
    }
    if(!Array.isArray(err)){

        $(".has-warning").removeClass("has-warning");
        return ;}
    var tpl  = '<div data-show="true" class="ant-alert ant-alert-error">';
    debugger;
    for (i = 0; i < err.length; i++) {
        var error = undefined;
        if(!!err[i].amount){
            //$(".amountItem.ant-form-item-control").addClass("has-warning");
            error   =  err[i].amount;}
        else  if(!!err[i].phone){
           // $(".phoneItem.ant-form-item-control").addClass("has-warning");
            error   =  err[i].phone;}
        else  if(!!err[i].comment){
            //$(".commentItem.ant-form-item-control").addClass("has-warning");
            error   =  err[i].comment;}
        else  if(!!err[i].email){
           // $(".emailItem.ant-form-item-control").addClass("has-warning");
            error   =  err[i].email;}
        else  if(!!err[i].err){  error   =  err[i].err;}
        if(!!error){
            tpl = tpl +  '<div>\n' +
                '<i class="anticon anticon-close-circle ant-alert-icon"></i>\n' +
                '\n' +
                ' <span class="ant-alert-message">'+ error +'</span>' +
                '<span class="ant-alert-description"></span>\n' +
                '\n' +
                '</div>';

        }


    }
    tpl = tpl+ "</div>";

        $("#errorNotification").html(tpl);
    }
    var IconType  = {

        creditCard  :'creditcard',
        loadingPay :'loadingPay'
    }

    var showicon = function(type){
        if(!type){return}
        if(type == IconType.creditCard){
            $("#icon").html('<i class="anticon anticon-credit-card"></i>');

        }else if(type == IconType.loadingPay){
            $("#icon").html('<i class="anticon anticon-spin anticon-loading"></i>');
        }
    }
    showicon(IconType.creditCard);

    verifyPaymentSuccess = function(resp){

        if(resp.success){
            resetForm();
            donateSuccessMsg();

        }else{
            if(!!resp.reason){
                addError([{"err":resp.reason}])
            }else{  addError([{"err":"Unexpected Error, Please try again later!."}])}
        }


    }
    verifyPaymentComplete = function(){
        $("#campaignDonationConf").hide();
        $("#campaignDonationArea").show();
    }
    verifyPaymentFailure = function(err){

        addError("Cannot verify Payment, Please contact support@seekfunds.ng")
        //console.log("Cannot verify Payment")

    }
    function payStackCallBack(response){
       // alert('success. transaction ref is ' + response.reference);

        var formInput  = getFormInput();

        $("#campaignDonationConf").show();
        $("#campaignDonationArea").hide();
        $("#campaignDonationConf").html(validatingPaymentHtml);
        $.ajax({
            type: "PUT",
            url: "/d/api/campaign-donation/ref/"+formInput.campaignId+"?reference=" +response.reference ,
            //data: JSON.stringify(formInput),
            contentType: 'application/json',
            success: verifyPaymentSuccess,
            dataType: "json",
            error : verifyPaymentFailure,
            complete : verifyPaymentComplete
        });



    }
    function payWithPaystack(val){

        var handler = PaystackPop.setup({
            key: window.pk_key,
            email: validateEmail(val.email) ?  val.email : "noreply@seekfunds.ng" /*'customer@email.com'*/,
            amount: val.amount,
            ref: ''+ val.ref /*Math.floor((Math.random() * 1000000000) + 1)*/, // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            metadata: {
                custom_fields: [
                    {
                        display_name: "Mobile Number",
                        variable_name: "mobile_number",
                        value: val.phone
                    }
                ]
            },
            callback:payStackCallBack,
            onClose: function(){
                addError([{"err":"unexpected Error, Please try again!."}])
            }
        });
        handler.openIframe();
    }

    var getAmount = function(){
        var sAmount  = $("#amount").val();
        if(!validateAmount(sAmount)){
            return 0.00
        }


    }
    var getFormInput  = function(){


        return {

            phone : $("#phoneNumber").val(),
            comment : $("#comment").val(),
            amount :  $("#amount").val(),
            email :  $("#email").val(),
            campaignId :  $("#campaignId").val(),
        }
    }

    var getRefSuccess = function(resp){

        var formInput  = getFormInput();

        if(resp.success){

            var obj = {
                amount :  formInput.amount*100,
                phone : formInput.phone,
                email : formInput.email,
                comment : formInput.comment,
                ref : resp.data,

            }
            payWithPaystack(obj);

        }else{
            if(!!resp.reason){
                addError([{"err":resp.reason}])
            }else{  addError([{"err":"Unexpected Error, Please try again later!."}])}
        }

        debugger;


    }
    var getRefError = function(data){


        debugger;


    }
    var getCampaignId  = function(){
        //TODO check to ensure their is campaign Id

        return $("#campaignId").val();

    }
    var getRefComplete = function(){
        showicon(IconType.creditCard);
        toggleButton();
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    function validateAmount(amount) {
        var re = /^\d{0,20}(\.\d{1,2})?$/;
        return re.test(String(amount).toLowerCase());
    }
    var checkForm  = function(){

        var err = [];
         var formInput  = getFormInput();
        debugger;
        if(!!formInput.email){


            if(!validateEmail(formInput.email)){
                err = err.concat({"email": " Invalid email : " + formInput.email+" entered!."});
                debugger;

            };
        }
        if(!!formInput.comment){

            if(!(formInput.comment.length  > 1 && formInput.comment.length < 500)){
                err = err.concat({"comment": "Comment out of range!."});
                debugger;

            }
        }
        if(!!formInput.phone){

            if(!(formInput.phone.length > 4 && formInput.phone.length < 12)){
                err = err.concat({"phone": " Invalid phone number : "+ formInput.phone+" entered!."});
                debugger;
               // err = err +  " Invalid phone number : "+ formInput.phone+" entered!."
            }
        }
        console.log(formInput.amount);
        if(!(!!formInput.amount && validateAmount(formInput.amount))){
            err =  err.concat({"amount": " Please enter a valid amount to donate"});
            debugger;
        }
       /* if(!formInput.amount){
            err.push[{"amount": " Please enter an amount to donate "}];
            //err = err +  " Please enter an amount to donate ";
        }else if(!validateAmount(formInput.amount)){
            err.push[{"amount": " Please enter a valid amount to donate"}];
            /!*err = err +  " Please enter a valid amount to donate ";*!/
        }*/
        return err;

    }
    var toggleButton = function(v){
        if(!!v){
            $("#payWithPaystack").attr("disabled", "disabled");
        }else{
            $("#payWithPaystack").removeAttr("disabled");
        }

    }
    $("#payWithPaystack").click(function (e) {


        e.preventDefault();
        toggleButton("y");
        addError(undefined);
        var formInput  =  getFormInput();
       var err =  checkForm(formInput);

       if(Array.isArray(err) && err.length  > 0){

           addError(err);
           toggleButton();
           return;
       }

 /*      var data = {
            amount :  getAmountInput(),
            phoneNumber : getPhoneNumber(),
            email : getEmail(),
            campaignId : getCampaignId(),

        }*/
        //TODO check all fields are correct
        //TODO get campaign id from form


        debugger;
        showicon(IconType.loadingPay);
        $.ajax({
            type: "POST",
            url: "/d/api/campaign-donation/ref/"+formInput.campaignId ,
            data: JSON.stringify(formInput),
            contentType: 'application/json',
            success: getRefSuccess,
            dataType: "json",
            error : getRefError,
            complete : getRefComplete
        });




       // ;

    })


}())