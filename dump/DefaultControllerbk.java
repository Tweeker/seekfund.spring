package com.enov8.seekfund.seekfund.controller;

import com.enov8.seekfund.seekfund.dao.AppUserDao;
import com.enov8.seekfund.seekfund.enumumeration.CampaignStatus;
import com.enov8.seekfund.seekfund.enumumeration.GenericStatusConstant;
import com.enov8.seekfund.seekfund.exception.SeekFundException;
import com.enov8.seekfund.seekfund.model.AppUser;
import com.enov8.seekfund.seekfund.model.SocialAuthUser;
import com.enov8.seekfund.seekfund.model.attribute.SocialRegister;
import com.enov8.seekfund.seekfund.pojo.AppUserProfilePojo;
import com.enov8.seekfund.seekfund.pojo.RestResponsePojo;
import com.enov8.seekfund.seekfund.pojo.projection.CurrentCampaignPojo;
import com.enov8.seekfund.seekfund.service.AesService;
import com.enov8.seekfund.seekfund.service.AppUserCreationService;
import com.enov8.seekfund.seekfund.service.CampaignService;
import com.enov8.seekfund.seekfund.service.DatabaseService;
import com.enov8.seekfund.seekfund.util.CommonMethod;
import com.enov8.seekfund.seekfund.util.Constant;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;

@Controller
@Transactional
public class DefaultControllerbk {
    @Autowired
    DatabaseService databaseService;
    @Autowired CommonMethod commonMethod;
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AppUserDao appUserService;
    @Autowired
    AesService aesService;
    @Autowired
    AppUserCreationService appUserCreationService;
    @Autowired
    CampaignService campaignService;
    @Value("${paystack.pk_public_key}")
    private String PK_KEY ;

    @GetMapping("/connect/twitter")
    public void  twitterConnect(HttpServletRequest request,
                              HttpServletResponse response,
                              RedirectAttributes attributes) throws Exception {


        // Get twitter object from session
        Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
//Get twitter request token object from session
        RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");

        //use the token secret and token for twitter template to retrieve data
        //tring consumerKey, String consumerSecret, String accessToken, String accessTokenSec
        TwitterTemplate twitterTemplate = new TwitterTemplate(
                requestToken.getToken());

        String re = twitterTemplate.getRestTemplate()
                .getForObject("https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true", String.class);
        System.out.println("Profile Info with Email: "+ re);


        String verifier = request.getParameter("oauth_verifier");
        try {
            // Get twitter access token object by verifying request token
            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
            request.getSession().removeAttribute("requestToken");

            // Get user object from database with twitter user id
            User user = twitter.showUser(accessToken.getUserId());


            System.out.println("======");


        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("http://localhost:8288");

    }
    @GetMapping("/signin/twitter")
    public void twitterSignin(HttpServletRequest request,
      HttpServletResponse response,
      RedirectAttributes attributes) throws Exception{

// configure twitter api with consumer key and secret key
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("2QAHM4zt0Ibv7VPXf4Q3MQjVZ")
                .setOAuthConsumerSecret("JZcTLIAJaQrZOOHcybcDCYSTVAX51aW24E0m0QP1C0SoDV2bme");
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        request.getSession().setAttribute("twitter", twitter);
        try {

            // setup callback URL
            StringBuffer callbackURL = request.getRequestURL();
            int index = callbackURL.lastIndexOf("/");
            callbackURL.replace(index, callbackURL.length(), "").append("/callback");

            // get request object and save to session

            //http://127.0.0.1:8288/connect/twitter
           // RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
            System.out.println("=============="+callbackURL);
            RequestToken requestToken = twitter
                    .getOAuthRequestToken("http://127.0.0.1:8288/connect/twitter");
            request.getSession().setAttribute("requestToken", requestToken);

            // redirect to twitter authentication URL
            response.sendRedirect(requestToken.getAuthenticationURL());

        } catch (TwitterException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(DefaultControllerbk.class);

    @RequestMapping(value = "/swagger-ui.html", method = RequestMethod.GET,
            produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public Resource getFromClasspath() {
        return new ClassPathResource("META-INF/resources/swagger-ui.html");
    }

    @GetMapping("/activated/email")
    public ModelAndView  activateEmail(ModelMap model,

                                      HttpServletRequest httpServletRequest,
                                      RedirectAttributes attributes) throws Exception{

       // String username  = attributes.
        if(model.containsKey("username")){
            return new ModelAndView("/activate.email", model);
        }
        return new ModelAndView("redirect:/login", model);

    }
    @GetMapping("/campaign-donation/{campaignId}")
    public ModelAndView activateEmail(ModelMap model,
                      Principal principal,@PathVariable("campaignId") String campaignId,
                      RedirectAttributes attributes) throws Exception{

        Long lCampaignId  = Long.parseLong(
                campaignId
                .replace("SEEKFUND","")
                .replace("seekfund","")
                .replace("-",""));



        CurrentCampaignPojo currentCampaignPojo = campaignService.getCampaignById(lCampaignId);

        if(currentCampaignPojo == null){
            model.addAttribute("error","No Campaign Found with Reference " + campaignId);
        }
        if(currentCampaignPojo != null && currentCampaignPojo.getStatus() != null){

            if(!currentCampaignPojo.getStatus().getValue().equalsIgnoreCase(CampaignStatus.RUNNING.getValue())){
                model.addAttribute("error","Campaign is currently " + currentCampaignPojo.getStatus());
            }
        }


        model.addAttribute("pk_key",PK_KEY);
        model.addAttribute("campaign",currentCampaignPojo);

        return new ModelAndView("/campaign_donate", model);

    }
    @GetMapping("/activate/email/{encryptedData}")
    public RedirectView activateEmail(Model model, @PathVariable("encryptedData") String encryptedData,

                                      HttpServletRequest httpServletRequest,
                                      RedirectAttributes attributes) throws Exception{

        logger.info(encryptedData);
        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);


        AppUser appUser = appUserCreationService.activateUserEmail(encryptedData, remoteIp);

        attributes.addFlashAttribute("username", appUser.getUsername());


        //attributes.addFlashAttribute("success","true");


        return new RedirectView("/activated/email");
       // return new ModelAndView("/activate.email", model);


    }
    //@ModelAttribute("appUser")
    public RestResponsePojo getAppUser(Authentication authentication) {
        RestResponsePojo restResponsePojo = new RestResponsePojo();
        AppUserProfilePojo appUserProfilePojo = new AppUserProfilePojo();
        try {
            if (authentication != null
                    && authentication.getPrincipal() != null
                    && authentication.getPrincipal() instanceof AppUser) {

                AppUser appUser = (AppUser) authentication.getPrincipal();
                restResponsePojo.setSuccess(false);

                if (appUser != null && appUser.getId() != null) {
                    /*String hql = " select a.id as id, a.username as username , " +
                            "a.email as email, " +
                            " " +
                            "a.dateCreated as dateCreated" +
                            " " +
                            " from AppUser a where a.id = " + appUser.getId();


                    appUserProfilePojo = (AppUserProfilePojo) databaseService
                            .getUniqueRecordByHql(hql, AppUserProfilePojo.class);*/
                    AppUserProfilePojo AppUserProfilePojo = new AppUserProfilePojo();
                    appUserProfilePojo.setDateCreated(appUser.getDateCreated());
                    appUserProfilePojo.setEmail(appUser.getEmail());
                    appUserProfilePojo.setUsername(appUser.getUsername());
                    appUserProfilePojo.setId(appUser.getId());
                    restResponsePojo.setData(appUserProfilePojo);
                    restResponsePojo.setSuccess(true);


                }

            }

        } catch (Exception e) {
            logger.info(e.getMessage());
            // e.printStackTrace();
        }

        return restResponsePojo;

    }
    @GetMapping(Constant.SOCIAL_REGISTER_ROUTE)
    public String socialRegister(Model model, Authentication authentication) throws Exception {

        model.addAttribute("appUser", getAppUser(authentication));

        AppUser appUser = (AppUser) authentication.getPrincipal();
        if(appUser != null && appUser.getSocialPrincipal() != null && appUser.getSocialAuthType() != null){
           String  hql  = "select a from SocialAuthUser a where a.auth_id " +
                    "= '" + appUser.getSocialPrincipal().toString()+ "' and a.authType =  " +
                    "'" + appUser.getSocialAuthType() + "'";

            SocialAuthUser socialAuthUser =  (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);
            model.addAttribute("socialAuthUser",socialAuthUser);


        }

        model.addAttribute("socialRegister",new SocialRegister());
        return "social_auth";
    }
    @PostMapping(Constant.SOCIAL_REGISTER_ROUTE)
    public RedirectView addSocialAccount(Model model,
     RedirectAttributes attributes,
       HttpServletRequest httpServletRequest ,
     @ModelAttribute SocialRegister socialRegister , Authentication authentication) throws Exception {

        AppUser appUser = (AppUser) authentication.getPrincipal();
        if(appUser == null){ throw new Exception("Cannot find stored user");}

        if(appUser.getId() != null){ throw new Exception("User already stored");}

        if(!(appUser.getSocialPrincipal() != null && appUser.getSocialAuthType() != null)) {

            throw new Exception("Unexpected data in authentication!!.");
        }
        String  hql  = "select a from SocialAuthUser a where a.auth_id " +
                "= '" + appUser.getSocialPrincipal().toString()+ "' and a.authType =  " +
                "'" + appUser.getSocialAuthType() + "'";

        SocialAuthUser socialAuthUser =  (SocialAuthUser) databaseService.getUniqueRecordByHql(hql);

        if(socialAuthUser == null){
            attributes.addFlashAttribute("errorMessage","Cannot find social user");

            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);

        }

        if(!socialAuthUser.getStatus().getValue()
                .equalsIgnoreCase(GenericStatusConstant.PENDING.getValue())){
            attributes.addFlashAttribute("errorMessage","Social Register is not pending!.");
            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);

        }




        String remoteIp = CommonMethod.getRemoteIp(httpServletRequest);

        try{

            AppUser testEmailUser  =  appUserService.getAppUserByEmail(socialAuthUser.getEmail());
            if(testEmailUser != null){
                attributes.addFlashAttribute("errorMessage","Email "+ socialAuthUser.getEmail()+
                        " cannot be used for this process !.");
                return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);
            }
            AppUser createdUser  = appUserCreationService
                    .createSocialAuthUser(socialAuthUser,socialRegister,remoteIp);

            Collection<GrantedAuthority> authorities =
                    appUserCreationService.getAuthorites(createdUser.getRoles());




            Authentication userAuth = new UsernamePasswordAuthenticationToken(createdUser,"",
                    authorities);
            SecurityContext context =  SecurityContextHolder.getContext();
            context.setAuthentication(userAuth);
            logger.info("DOne creating social User");


        }catch (SeekFundException e){
            attributes.addFlashAttribute("errorMessage",e.getMessage());

            return  new RedirectView(Constant.SOCIAL_REGISTER_ROUTE);
        } catch (Exception e){
            e.printStackTrace();
            attributes.addFlashAttribute("errorMessage",e.getMessage());

            return  new RedirectView("/error");
        }
        return new RedirectView("/home");

    }
    @GetMapping("/accessDenied")
    public String accessDenied(HttpServletResponse httpServletResponse,Authentication authentication, Model model) {

        //System.out.println(httpServletResponse.getStatus());

        return "error/403";
    }
    //value = {"*", "/home/**", "/campaigns/**"})
    @GetMapping(value={"*", "/home/**", "/campaigns/**","/campaign/**"})
    public String index(HttpServletResponse httpServletResponse,Authentication authentication, Model model){

       //System.out.println(httpServletResponse.getStatus());
        //TODO get OAUTH details
        //
        //restResponsePojo.setData(appUserProfilePojo);


        model.addAttribute("appUser", getAppUser(authentication));
        return "index";

    }
}
